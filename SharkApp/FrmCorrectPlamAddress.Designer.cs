﻿namespace SharkApp
{
    partial class FrmCorrectPlamAddress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GridPalmCase = new System.Windows.Forms.DataGridView();
            this.label8 = new System.Windows.Forms.Label();
            this.radSelectedAddressPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblType = new System.Windows.Forms.Label();
            this.lblHidenCaseNumberID = new System.Windows.Forms.Label();
            this.txtCorrectPalmAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.radDDLPrefix = new Telerik.WinControls.UI.RadDropDownList();
            this.lblHidenSrchDefendentID = new System.Windows.Forms.Label();
            this.btnSavePalmAddress = new System.Windows.Forms.Button();
            this.txtUnitNo = new Telerik.WinControls.UI.RadTextBox();
            this.txtPostdiction = new Telerik.WinControls.UI.RadTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSreetNo = new Telerik.WinControls.UI.RadTextBox();
            this.txtMunicipality = new Telerik.WinControls.UI.RadTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSuffix = new Telerik.WinControls.UI.RadTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtstName = new Telerik.WinControls.UI.RadTextBox();
            this.gridPalmAddress = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.lblhdnPalmBeachLPCaseID = new System.Windows.Forms.Label();
            this.txtZipcode = new Telerik.WinControls.UI.RadTextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.GridPalmCase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSelectedAddressPanel)).BeginInit();
            this.radSelectedAddressPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDDLPrefix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostdiction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSreetNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuffix)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPalmAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZipcode)).BeginInit();
            this.SuspendLayout();
            // 
            // GridPalmCase
            // 
            this.GridPalmCase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridPalmCase.Location = new System.Drawing.Point(12, 59);
            this.GridPalmCase.Name = "GridPalmCase";
            this.GridPalmCase.Size = new System.Drawing.Size(461, 568);
            this.GridPalmCase.TabIndex = 0;
            this.GridPalmCase.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridPalmCase_CellContentDoubleClick);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(40, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(398, 16);
            this.label8.TabIndex = 55;
            this.label8.Text = "Double click on Palm Beach Case  to Display property addresses";
            // 
            // radSelectedAddressPanel
            // 
            this.radSelectedAddressPanel.Controls.Add(this.txtZipcode);
            this.radSelectedAddressPanel.Controls.Add(this.label3);
            this.radSelectedAddressPanel.Controls.Add(this.lblType);
            this.radSelectedAddressPanel.Controls.Add(this.lblHidenCaseNumberID);
            this.radSelectedAddressPanel.Controls.Add(this.txtCorrectPalmAddress);
            this.radSelectedAddressPanel.Controls.Add(this.label1);
            this.radSelectedAddressPanel.Controls.Add(this.label4);
            this.radSelectedAddressPanel.Controls.Add(this.radDDLPrefix);
            this.radSelectedAddressPanel.Controls.Add(this.lblHidenSrchDefendentID);
            this.radSelectedAddressPanel.Controls.Add(this.btnSavePalmAddress);
            this.radSelectedAddressPanel.Controls.Add(this.txtUnitNo);
            this.radSelectedAddressPanel.Controls.Add(this.txtPostdiction);
            this.radSelectedAddressPanel.Controls.Add(this.label5);
            this.radSelectedAddressPanel.Controls.Add(this.label7);
            this.radSelectedAddressPanel.Controls.Add(this.label10);
            this.radSelectedAddressPanel.Controls.Add(this.txtSreetNo);
            this.radSelectedAddressPanel.Controls.Add(this.txtMunicipality);
            this.radSelectedAddressPanel.Controls.Add(this.label11);
            this.radSelectedAddressPanel.Controls.Add(this.txtSuffix);
            this.radSelectedAddressPanel.Controls.Add(this.label12);
            this.radSelectedAddressPanel.Controls.Add(this.label13);
            this.radSelectedAddressPanel.Controls.Add(this.label14);
            this.radSelectedAddressPanel.Controls.Add(this.txtstName);
            this.radSelectedAddressPanel.Location = new System.Drawing.Point(479, 279);
            this.radSelectedAddressPanel.Name = "radSelectedAddressPanel";
            this.radSelectedAddressPanel.Size = new System.Drawing.Size(560, 348);
            this.radSelectedAddressPanel.TabIndex = 56;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(500, 163);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(0, 13);
            this.lblType.TabIndex = 37;
            this.lblType.Visible = false;
            // 
            // lblHidenCaseNumberID
            // 
            this.lblHidenCaseNumberID.AutoSize = true;
            this.lblHidenCaseNumberID.Location = new System.Drawing.Point(546, 131);
            this.lblHidenCaseNumberID.Name = "lblHidenCaseNumberID";
            this.lblHidenCaseNumberID.Size = new System.Drawing.Size(0, 13);
            this.lblHidenCaseNumberID.TabIndex = 36;
            // 
            // txtCorrectPalmAddress
            // 
            this.txtCorrectPalmAddress.Location = new System.Drawing.Point(166, 14);
            this.txtCorrectPalmAddress.Multiline = true;
            this.txtCorrectPalmAddress.Name = "txtCorrectPalmAddress";
            this.txtCorrectPalmAddress.Size = new System.Drawing.Size(386, 81);
            this.txtCorrectPalmAddress.TabIndex = 35;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(164, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(330, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "Copy address components and paste in respective fields below";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Selected address ";
            // 
            // radDDLPrefix
            // 
            this.radDDLPrefix.Location = new System.Drawing.Point(166, 147);
            this.radDDLPrefix.Name = "radDDLPrefix";
            this.radDDLPrefix.Size = new System.Drawing.Size(193, 20);
            this.radDDLPrefix.TabIndex = 31;
            // 
            // lblHidenSrchDefendentID
            // 
            this.lblHidenSrchDefendentID.AutoSize = true;
            this.lblHidenSrchDefendentID.Location = new System.Drawing.Point(433, 26);
            this.lblHidenSrchDefendentID.Name = "lblHidenSrchDefendentID";
            this.lblHidenSrchDefendentID.Size = new System.Drawing.Size(0, 13);
            this.lblHidenSrchDefendentID.TabIndex = 30;
            this.lblHidenSrchDefendentID.Visible = false;
            // 
            // btnSavePalmAddress
            // 
            this.btnSavePalmAddress.Location = new System.Drawing.Point(377, 296);
            this.btnSavePalmAddress.Name = "btnSavePalmAddress";
            this.btnSavePalmAddress.Size = new System.Drawing.Size(169, 38);
            this.btnSavePalmAddress.TabIndex = 29;
            this.btnSavePalmAddress.Text = "Save Address";
            this.btnSavePalmAddress.UseVisualStyleBackColor = true;
            this.btnSavePalmAddress.Click += new System.EventHandler(this.btnSavePalmAddress_Click);
            // 
            // txtUnitNo
            // 
            this.txtUnitNo.Location = new System.Drawing.Point(166, 254);
            this.txtUnitNo.Name = "txtUnitNo";
            this.txtUnitNo.Size = new System.Drawing.Size(193, 20);
            this.txtUnitNo.TabIndex = 21;
            // 
            // txtPostdiction
            // 
            this.txtPostdiction.Location = new System.Drawing.Point(166, 228);
            this.txtPostdiction.Name = "txtPostdiction";
            this.txtPostdiction.Size = new System.Drawing.Size(193, 20);
            this.txtPostdiction.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 261);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Unit Number";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 235);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Post Direction";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 121);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Street Number ";
            // 
            // txtSreetNo
            // 
            this.txtSreetNo.Location = new System.Drawing.Point(166, 121);
            this.txtSreetNo.Name = "txtSreetNo";
            this.txtSreetNo.Size = new System.Drawing.Size(193, 20);
            this.txtSreetNo.TabIndex = 16;
            // 
            // txtMunicipality
            // 
            this.txtMunicipality.Location = new System.Drawing.Point(166, 283);
            this.txtMunicipality.Name = "txtMunicipality";
            this.txtMunicipality.Size = new System.Drawing.Size(193, 20);
            this.txtMunicipality.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 285);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Municipality";
            // 
            // txtSuffix
            // 
            this.txtSuffix.Location = new System.Drawing.Point(166, 201);
            this.txtSuffix.Name = "txtSuffix";
            this.txtSuffix.Size = new System.Drawing.Size(193, 20);
            this.txtSuffix.TabIndex = 20;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(20, 146);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Prefix";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(20, 208);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Suffix";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(20, 177);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 13);
            this.label14.TabIndex = 23;
            this.label14.Text = "Street Name ";
            // 
            // txtstName
            // 
            this.txtstName.Location = new System.Drawing.Point(167, 173);
            this.txtstName.Name = "txtstName";
            this.txtstName.Size = new System.Drawing.Size(193, 20);
            this.txtstName.TabIndex = 19;
            // 
            // gridPalmAddress
            // 
            this.gridPalmAddress.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridPalmAddress.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridPalmAddress.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gridPalmAddress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridPalmAddress.Location = new System.Drawing.Point(479, 59);
            this.gridPalmAddress.Name = "gridPalmAddress";
            this.gridPalmAddress.Size = new System.Drawing.Size(560, 214);
            this.gridPalmAddress.TabIndex = 1;
            this.gridPalmAddress.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridPalmAddress_CellContentDoubleClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(526, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(430, 16);
            this.label2.TabIndex = 58;
            this.label2.Text = "Double click on Palm Beach Address  to save correct property address";
            // 
            // lblhdnPalmBeachLPCaseID
            // 
            this.lblhdnPalmBeachLPCaseID.AutoSize = true;
            this.lblhdnPalmBeachLPCaseID.Location = new System.Drawing.Point(381, 39);
            this.lblhdnPalmBeachLPCaseID.Name = "lblhdnPalmBeachLPCaseID";
            this.lblhdnPalmBeachLPCaseID.Size = new System.Drawing.Size(0, 13);
            this.lblhdnPalmBeachLPCaseID.TabIndex = 59;
            // 
            // txtZipcode
            // 
            this.txtZipcode.Location = new System.Drawing.Point(167, 314);
            this.txtZipcode.Name = "txtZipcode";
            this.txtZipcode.Size = new System.Drawing.Size(193, 20);
            this.txtZipcode.TabIndex = 38;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 314);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 39;
            this.label3.Text = "Zipcode";
            // 
            // FrmCorrectPlamAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1051, 634);
            this.Controls.Add(this.lblhdnPalmBeachLPCaseID);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.gridPalmAddress);
            this.Controls.Add(this.radSelectedAddressPanel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.GridPalmCase);
            this.Name = "FrmCorrectPlamAddress";
            this.Text = "Correct Palm Address";
            ((System.ComponentModel.ISupportInitialize)(this.GridPalmCase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSelectedAddressPanel)).EndInit();
            this.radSelectedAddressPanel.ResumeLayout(false);
            this.radSelectedAddressPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDDLPrefix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostdiction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSreetNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMunicipality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSuffix)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPalmAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZipcode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView GridPalmCase;
        private System.Windows.Forms.Label label8;
        private Telerik.WinControls.UI.RadPanel radSelectedAddressPanel;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label lblHidenCaseNumberID;
        private System.Windows.Forms.TextBox txtCorrectPalmAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.RadDropDownList radDDLPrefix;
        private System.Windows.Forms.Label lblHidenSrchDefendentID;
        private System.Windows.Forms.Button btnSavePalmAddress;
        private Telerik.WinControls.UI.RadTextBox txtUnitNo;
        private Telerik.WinControls.UI.RadTextBox txtPostdiction;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private Telerik.WinControls.UI.RadTextBox txtSreetNo;
        private Telerik.WinControls.UI.RadTextBox txtMunicipality;
        private System.Windows.Forms.Label label11;
        private Telerik.WinControls.UI.RadTextBox txtSuffix;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private Telerik.WinControls.UI.RadTextBox txtstName;
        private System.Windows.Forms.DataGridView gridPalmAddress;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblhdnPalmBeachLPCaseID;
        private Telerik.WinControls.UI.RadTextBox txtZipcode;
        private System.Windows.Forms.Label label3;
    }
}