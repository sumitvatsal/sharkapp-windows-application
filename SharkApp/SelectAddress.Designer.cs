﻿namespace SharkApp
{
    partial class SelectAddress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radSelectedAddressPanel = new Telerik.WinControls.UI.RadPanel();
            this.txtUnitNo = new Telerik.WinControls.UI.RadTextBox();
            this.txtPostDir = new Telerik.WinControls.UI.RadTextBox();
            this.lblUnitNo = new System.Windows.Forms.Label();
            this.lblPostDir = new System.Windows.Forms.Label();
            this.lblstretNumber = new System.Windows.Forms.Label();
            this.txtStNum = new Telerik.WinControls.UI.RadTextBox();
            this.txtStCity = new Telerik.WinControls.UI.RadTextBox();
            this.lblStCity = new System.Windows.Forms.Label();
            this.txtStType = new Telerik.WinControls.UI.RadTextBox();
            this.lblstdirction = new System.Windows.Forms.Label();
            this.lblstType = new System.Windows.Forms.Label();
            this.txtStdir = new Telerik.WinControls.UI.RadTextBox();
            this.lblstname = new System.Windows.Forms.Label();
            this.txtStName = new Telerik.WinControls.UI.RadTextBox();
            this.SelectedAddressSave = new System.Windows.Forms.Button();
            this.lblSelAddress = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.radSelectedAddressPanel)).BeginInit();
            this.radSelectedAddressPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostDir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStdir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStName)).BeginInit();
            this.SuspendLayout();
            // 
            // radSelectedAddressPanel
            // 
            this.radSelectedAddressPanel.Controls.Add(this.SelectedAddressSave);
            this.radSelectedAddressPanel.Controls.Add(this.txtUnitNo);
            this.radSelectedAddressPanel.Controls.Add(this.txtPostDir);
            this.radSelectedAddressPanel.Controls.Add(this.lblUnitNo);
            this.radSelectedAddressPanel.Controls.Add(this.lblPostDir);
            this.radSelectedAddressPanel.Controls.Add(this.lblstretNumber);
            this.radSelectedAddressPanel.Controls.Add(this.txtStNum);
            this.radSelectedAddressPanel.Controls.Add(this.txtStCity);
            this.radSelectedAddressPanel.Controls.Add(this.lblStCity);
            this.radSelectedAddressPanel.Controls.Add(this.txtStType);
            this.radSelectedAddressPanel.Controls.Add(this.lblstdirction);
            this.radSelectedAddressPanel.Controls.Add(this.lblstType);
            this.radSelectedAddressPanel.Controls.Add(this.txtStdir);
            this.radSelectedAddressPanel.Controls.Add(this.lblstname);
            this.radSelectedAddressPanel.Controls.Add(this.txtStName);
            this.radSelectedAddressPanel.Location = new System.Drawing.Point(12, 12);
            this.radSelectedAddressPanel.Name = "radSelectedAddressPanel";
            this.radSelectedAddressPanel.Size = new System.Drawing.Size(607, 242);
            this.radSelectedAddressPanel.TabIndex = 29;
            // 
            // txtUnitNo
            // 
            this.txtUnitNo.Location = new System.Drawing.Point(186, 153);
            this.txtUnitNo.Name = "txtUnitNo";
            this.txtUnitNo.Size = new System.Drawing.Size(203, 20);
            this.txtUnitNo.TabIndex = 21;
            // 
            // txtPostDir
            // 
            this.txtPostDir.Location = new System.Drawing.Point(186, 127);
            this.txtPostDir.Name = "txtPostDir";
            this.txtPostDir.Size = new System.Drawing.Size(203, 20);
            this.txtPostDir.TabIndex = 28;
            // 
            // lblUnitNo
            // 
            this.lblUnitNo.AutoSize = true;
            this.lblUnitNo.Location = new System.Drawing.Point(40, 160);
            this.lblUnitNo.Name = "lblUnitNo";
            this.lblUnitNo.Size = new System.Drawing.Size(73, 13);
            this.lblUnitNo.TabIndex = 27;
            this.lblUnitNo.Text = "Unit Number";
            // 
            // lblPostDir
            // 
            this.lblPostDir.AutoSize = true;
            this.lblPostDir.Location = new System.Drawing.Point(40, 134);
            this.lblPostDir.Name = "lblPostDir";
            this.lblPostDir.Size = new System.Drawing.Size(79, 13);
            this.lblPostDir.TabIndex = 26;
            this.lblPostDir.Text = "Post Direction";
            // 
            // lblstretNumber
            // 
            this.lblstretNumber.AutoSize = true;
            this.lblstretNumber.Location = new System.Drawing.Point(40, 20);
            this.lblstretNumber.Name = "lblstretNumber";
            this.lblstretNumber.Size = new System.Drawing.Size(84, 13);
            this.lblstretNumber.TabIndex = 5;
            this.lblstretNumber.Text = "Street Number ";
            // 
            // txtStNum
            // 
            this.txtStNum.Location = new System.Drawing.Point(186, 20);
            this.txtStNum.Name = "txtStNum";
            this.txtStNum.Size = new System.Drawing.Size(203, 20);
            this.txtStNum.TabIndex = 16;
            // 
            // txtStCity
            // 
            this.txtStCity.Location = new System.Drawing.Point(186, 182);
            this.txtStCity.Name = "txtStCity";
            this.txtStCity.Size = new System.Drawing.Size(203, 20);
            this.txtStCity.TabIndex = 21;
            // 
            // lblStCity
            // 
            this.lblStCity.AutoSize = true;
            this.lblStCity.Location = new System.Drawing.Point(40, 189);
            this.lblStCity.Name = "lblStCity";
            this.lblStCity.Size = new System.Drawing.Size(59, 13);
            this.lblStCity.TabIndex = 25;
            this.lblStCity.Text = "Street City";
            // 
            // txtStType
            // 
            this.txtStType.Location = new System.Drawing.Point(186, 100);
            this.txtStType.Name = "txtStType";
            this.txtStType.Size = new System.Drawing.Size(203, 20);
            this.txtStType.TabIndex = 20;
            // 
            // lblstdirction
            // 
            this.lblstdirction.AutoSize = true;
            this.lblstdirction.Location = new System.Drawing.Point(40, 45);
            this.lblstdirction.Name = "lblstdirction";
            this.lblstdirction.Size = new System.Drawing.Size(90, 13);
            this.lblstdirction.TabIndex = 22;
            this.lblstdirction.Text = "Street Direction ";
            // 
            // lblstType
            // 
            this.lblstType.AutoSize = true;
            this.lblstType.Location = new System.Drawing.Point(40, 107);
            this.lblstType.Name = "lblstType";
            this.lblstType.Size = new System.Drawing.Size(66, 13);
            this.lblstType.TabIndex = 24;
            this.lblstType.Text = "Street Type ";
            // 
            // txtStdir
            // 
            this.txtStdir.Location = new System.Drawing.Point(186, 45);
            this.txtStdir.Name = "txtStdir";
            this.txtStdir.Size = new System.Drawing.Size(203, 20);
            this.txtStdir.TabIndex = 18;
            // 
            // lblstname
            // 
            this.lblstname.AutoSize = true;
            this.lblstname.Location = new System.Drawing.Point(40, 76);
            this.lblstname.Name = "lblstname";
            this.lblstname.Size = new System.Drawing.Size(72, 13);
            this.lblstname.TabIndex = 23;
            this.lblstname.Text = "Street Name ";
            // 
            // txtStName
            // 
            this.txtStName.Location = new System.Drawing.Point(187, 72);
            this.txtStName.Name = "txtStName";
            this.txtStName.Size = new System.Drawing.Size(203, 20);
            this.txtStName.TabIndex = 19;
            // 
            // SelectedAddressSave
            // 
            this.SelectedAddressSave.Location = new System.Drawing.Point(433, 189);
            this.SelectedAddressSave.Name = "SelectedAddressSave";
            this.SelectedAddressSave.Size = new System.Drawing.Size(156, 38);
            this.SelectedAddressSave.TabIndex = 29;
            this.SelectedAddressSave.Text = "Save Address";
            this.SelectedAddressSave.UseVisualStyleBackColor = true;
            this.SelectedAddressSave.Click += new System.EventHandler(this.SelectedAddressSave_Click);
            // 
            // lblSelAddress
            // 
            this.lblSelAddress.AutoSize = true;
            this.lblSelAddress.Location = new System.Drawing.Point(52, 273);
            this.lblSelAddress.Name = "lblSelAddress";
            this.lblSelAddress.Size = new System.Drawing.Size(35, 13);
            this.lblSelAddress.TabIndex = 30;
            this.lblSelAddress.Text = "label1";
            // 
            // SelectAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 529);
            this.Controls.Add(this.lblSelAddress);
            this.Controls.Add(this.radSelectedAddressPanel);
            this.Name = "SelectAddress";
            this.Text = "SelectAddress";
            ((System.ComponentModel.ISupportInitialize)(this.radSelectedAddressPanel)).EndInit();
            this.radSelectedAddressPanel.ResumeLayout(false);
            this.radSelectedAddressPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostDir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStdir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStName)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radSelectedAddressPanel;
        private System.Windows.Forms.Button SelectedAddressSave;
        private Telerik.WinControls.UI.RadTextBox txtUnitNo;
        private Telerik.WinControls.UI.RadTextBox txtPostDir;
        private System.Windows.Forms.Label lblUnitNo;
        private System.Windows.Forms.Label lblPostDir;
        private System.Windows.Forms.Label lblstretNumber;
        private Telerik.WinControls.UI.RadTextBox txtStNum;
        private Telerik.WinControls.UI.RadTextBox txtStCity;
        private System.Windows.Forms.Label lblStCity;
        private Telerik.WinControls.UI.RadTextBox txtStType;
        private System.Windows.Forms.Label lblstdirction;
        private System.Windows.Forms.Label lblstType;
        private Telerik.WinControls.UI.RadTextBox txtStdir;
        private System.Windows.Forms.Label lblstname;
        private Telerik.WinControls.UI.RadTextBox txtStName;
        private System.Windows.Forms.Label lblSelAddress;
    }
}