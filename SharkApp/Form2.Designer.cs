﻿namespace BrowardDesk
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnBeginSunbizSearch = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBeginSunbizSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(70, 42);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 150);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // btnBeginSunbizSearch
            // 
            this.btnBeginSunbizSearch.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnBeginSunbizSearch.Location = new System.Drawing.Point(505, 134);
            this.btnBeginSunbizSearch.Name = "btnBeginSunbizSearch";
            // 
            // 
            // 
            this.btnBeginSunbizSearch.RootElement.AccessibleDescription = null;
            this.btnBeginSunbizSearch.RootElement.AccessibleName = null;
            this.btnBeginSunbizSearch.RootElement.ControlBounds = new System.Drawing.Rectangle(505, 134, 110, 24);
            this.btnBeginSunbizSearch.Size = new System.Drawing.Size(142, 33);
            this.btnBeginSunbizSearch.TabIndex = 4;
            this.btnBeginSunbizSearch.Text = "Begin Search";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBeginSunbizSearch)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private Telerik.WinControls.UI.RadButton btnBeginSunbizSearch;
    }
}