﻿using BrowardDesk.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SharkApp
{
    public class Common
    {
        public static bool IsInternetConnected()
        {
            int desc;
            return InternetGetConnectedState(out desc, 0);
        }
        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int description, int reservedValue);

        public enum CountyType
        {
            None=0,
            Broward=1,
            PalmBeach=2,
            MiamiDade=3
        }

        public enum CountySocialmedia
        {
            None = 0,
            Broward = 1,
            PalmBeach = 2,
            Individual = 3
        }

        public enum MenuItem
        {
            Home=0,
            CaseCategory=1,
            GeneralSearch=2,
            BrowardSearch=3,
            PalmBeachsearch=4,
            MiamiDadeSearch=5,
            SpokeoSearch=6,
            SunbizSearch=7,
            FSBOSearch=8,
            Socialmedia=9
        }

        public enum SpokeoSearchType
        {
            AllAddress=0,
            SingleAddress=1,
            BulkAddress=2,
            AllPerson=3,
            SinglePerson=4,
            BulkPerson=5
        }

        public static void InsertException(tblException exception)
        {
            using(BrowardclerkEntities db = new BrowardclerkEntities())
            {
                db.spInsertException(exception.ExceptionMessage, exception.ExceptionType, exception.OccurenceTime,exception.County);
            }
        }

        public static void SendExceptionEmail(List<tblException> exceptionList)
        {
            SmtpClient smtpClient = new SmtpClient("smtp.1and1.com", 25);
            MailAddress maDNR = new MailAddress("maneeshb@ajosys.com", "Technical Support");
            NetworkCredential ncInfo = new NetworkCredential("ems@ajosys.com", "Ems@ajosys0913");
            string body = string.Empty;
            MailMessage mail = new MailMessage();
            mail.From = maDNR;
            mail.Subject = "Shark Desktop Exception";
            mail.IsBodyHtml = true;

            StringBuilder sb = new StringBuilder();
            sb.Append("<html><body><table border='1'>");
            sb.Append("<thead><th>County</th><th>Exception Type</th><th>Occurence Time</th><th>Exception Message</th></thead>");

            foreach(var exception in exceptionList)
            {
                sb.Append("<tr><td>"+exception.County+"</td><td>" + exception.ExceptionType + "</td><td>" + exception.OccurenceTime + "</td><td>" + exception.ExceptionMessage + "</td></tr>");
            }

            body = sb.ToString();
            mail.Body = body;

            smtpClient.Credentials = ncInfo;
            mail.To.Add("maneeshb@ajosys.com");
            smtpClient.Send(mail);
        }
    }
}
