﻿using BrowardDesk.Models;
using SharkApp.ViewModel;
//using SharkApp.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SharkApp
{
    public partial class FrmCorrectPlamAddress : Form
    {
        VMPalmCase _vmpalmcase = new VMPalmCase();
        public ArrayList myImages = new ArrayList();
        public ArrayList TextFormate = new ArrayList();
        ArrayList _DocResultURL = new ArrayList();

        public FrmCorrectPlamAddress()
        {
            InitializeComponent();
            BindPalmLPCases();
            BindComboBoxStDirection();
        }


        public void BindPalmLPCases()
        {
            try
            {
                List<PalmBeachLpcases> _tblPalmBeach = _vmpalmcase.GetPalmSaveableLPCase().Where(x => x.IsCorrectAddress == true).ToList();
                if (_tblPalmBeach != null && _tblPalmBeach.Count > 0)
                {
                    DataTable _dt = _vmpalmcase.GetPalmCaseList(_tblPalmBeach);
                    GridPalmCase.DataSource = _dt;
                    GridPalmCase.Columns[0].Visible = false;
                    GridPalmCase.Columns[0].Width = 100;
                    GridPalmCase.Columns[1].Width = 100;
                    GridPalmCase.Columns[2].Width = 100;
                }
            }
            catch
            {

            }
        }

        private void GridPalmCase_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            try
            {
                if (e.ColumnIndex == GridPalmCase.Columns["Name"].Index && e.RowIndex >= 0)
                {
                    Cursor = Cursors.WaitCursor;                  
                    int PalmBeachLPCaseID = Convert.ToInt32(GridPalmCase.Rows[e.RowIndex].Cells[0].Value.ToString());

                    List<tblPalmAllAddress> PalmAllAddress = _vmpalmcase.GetLpPalmAddress(PalmBeachLPCaseID).ToList();
                    lblhdnPalmBeachLPCaseID.Text = PalmBeachLPCaseID.ToString();
                    if (PalmAllAddress != null)
                    {
                        GetAddress(PalmAllAddress);
                    }
                    else
                    {
                        gridPalmAddress.Columns.Add("No Record", "No Record");
                        gridPalmAddress.Rows[0].Cells[0].Value = "No Records Found.";
                        gridPalmAddress.Columns[0].Width = 200;
                    }

                    Cursor = Cursors.Arrow;
                }
            }
            catch
            {

            }
        }
        public void GetAddress(List<tblPalmAllAddress> PalmAllAddress)
        {
            try
            {

                DataTable _dt = _vmpalmcase.GetPalmAddressList(PalmAllAddress);
                gridPalmAddress.DataSource = _dt;
                gridPalmAddress.Columns[0].Visible = false;
                gridPalmAddress.Columns[0].Width = 100;
                gridPalmAddress.Columns[1].Width = 100;               
            }
            catch
            {

            }
        }
       
        private void gridPalmAddress_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == gridPalmAddress.Columns["PalmCaseAddress"].Index && e.RowIndex >= 0)
            {
                string _Text = gridPalmAddress.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtCorrectPalmAddress.Text = _Text;

            }
        }

        private void btnSavePalmAddress_Click(object sender, EventArgs e)
        {
            try{
            Cursor = Cursors.WaitCursor;
           
            int PalmBeachLPCaseID = Convert.ToInt32(lblhdnPalmBeachLPCaseID.Text.Trim());
            string StreetNumber = txtSreetNo.Text.Trim();
            string StreetDirection = radDDLPrefix.SelectedValue.ToString();
            string StreetName = txtstName.Text.Trim();
            string Sufix = txtSuffix.Text.Trim();
            string PostDirection = txtPostdiction.Text.Trim();
            string UnitNumber = txtUnitNo.Text.Trim();
            string City = txtMunicipality.Text.Trim();
            string Zipcode = txtZipcode.Text.Trim();
            _vmpalmcase.SaveCorrectAddrsLPPalmBeach(PalmBeachLPCaseID, StreetNumber, StreetDirection, StreetName, Sufix, PostDirection, UnitNumber, City, Zipcode);
            ClearFields();

            BindPalmLPCases();
            
            Cursor = Cursors.Arrow;
            }
            catch{

            }
        }

        public void ClearFields()
        {
            gridPalmAddress.DataSource = null;
            txtCorrectPalmAddress.Text = "";
            lblhdnPalmBeachLPCaseID.Text = "";
            txtSreetNo.Text = "";
            txtstName.Text = "";
            txtSuffix.Text = "";
            txtPostdiction.Text = "";
            txtUnitNo.Text = "";
            txtMunicipality.Text = "";
            txtZipcode.Text = "";
            
        }

        public void BindComboBoxStDirection()
        {
            StDirection[] list = new StDirection[]  {
                 new StDirection("None",""),
                                 new StDirection("N","N"),
                                 new StDirection("NE","NE"),
                                 new StDirection("NW","NW"),
                                 new StDirection("E","E"), 
                                  new StDirection("S","S"),
                                 new StDirection("SE","SE"), 
                                   new StDirection("SW","SW"),
                                 new StDirection("W","W"), 
                               };
            radDDLPrefix.DataSource = list;
            radDDLPrefix.DisplayMember = "StDirectionName";
            radDDLPrefix.ValueMember = "StDirectionValue";

        }
        public class StDirection
        {
            public string StDirectionName { get; set; }
            public string StDirectionValue { get; set; }
            public StDirection(string _StDirectionName, string _StDirectionValue)
            {
                StDirectionName = _StDirectionName;
                StDirectionValue = _StDirectionValue;
            }
        }
        
    }
}
