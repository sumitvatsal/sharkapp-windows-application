﻿using BrowardDesk.Models;
using SharkApp.ViewModel;
using Ghostscript.NET;
using iTextSharp.text.pdf;
using MODI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace SharkApp
{
    public partial class FrmSaveAbleAddress : Form
    {
        VMCase _vmcase = new VMCase();
        public ArrayList myImages = new ArrayList();
        public ArrayList TextFormate = new ArrayList();
        ArrayList _DocResultURL = new ArrayList();
        public FrmSaveAbleAddress(int Type)
        {
            InitializeComponent();
            BindSaveAbleAddress(Type);
            BindComboBoxStDirection();
            lblHidenCaseNumberID.Visible = false;
        }


        public void BindSaveAbleAddress(int Type)
        {
            lblSrchType.Text = Type.ToString();
            if (Type == 1)
            {
                List<SrchDefandant> NotSavedAddress = _vmcase.GetLpCasesNotSavedAddress();
                if (NotSavedAddress != null && NotSavedAddress.Count > 0)
                {
                    DataTable _dt = _vmcase.GetAddressList(NotSavedAddress);
                    GridSaveableAddress.DataSource = _dt;
                    GridSaveableAddress.Columns[0].Visible = false;
                }
            }
            else
            {
                string SearchType = "Bulk";
                List<tblCaseNumberSearch> NotSavedAddress = _vmcase.GettblCaseNumberNotSavedAddress(SearchType);
                if (NotSavedAddress != null && NotSavedAddress.Count > 0)
                {
                    DataTable _dt = _vmcase.GetCaseAddressList(NotSavedAddress);
                    GridSaveableAddress.DataSource = _dt;
                    GridSaveableAddress.Columns[0].Visible = false;
                }

            }


        }

        private void GridSaveableAddress_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            dataGridOtherProperty.DataSource = null;
            int Type = Convert.ToInt32(lblSrchType.Text);
            tblCaseNumberSearch caseDetails ;
            if (Type == 1)
            {
                int SrchDefendantID = Convert.ToInt32(GridSaveableAddress.Rows[e.RowIndex].Cells[0].Value.ToString());
                lblHidenSrchDefendentID.Text = SrchDefendantID.ToString();
                caseDetails = _vmcase.GetcaseDetails(SrchDefendantID, Type);
                GetAddressListFromOtherFolio(SrchDefendantID);
            }
            else
            {
                int CaseNumberID = Convert.ToInt32(GridSaveableAddress.Rows[e.RowIndex].Cells[0].Value.ToString());
                caseDetails = _vmcase.GetcaseDetails(CaseNumberID, Type);
                lblHidenCaseNumberID.Text = CaseNumberID.ToString();
                lblType.Text = Type.ToString();

            }
            label5.Text = "Case Number : " + caseDetails.CaseNumber;
            if (caseDetails != null)
            {
                GetAddress(caseDetails, Type);
            }
            else
            {
                gridDisplayAddress.Columns.Add("No Record", "No Record");
                //gridDisplayAddress.Rows.Add("No Record", "No Record", "No Record");
                gridDisplayAddress.Rows[0].Cells[0].Value = "No Records Found.";
                gridDisplayAddress.Columns[0].Width = 200;
            }
           
        }
        public void GetAddressListFromOtherFolio(int SrchDefendantID)
        {
            
            DataTable _dt = _vmcase.GetOtherFolioPropertyAddress(SrchDefendantID);
            dataGridOtherProperty.DataSource = _dt;
            dataGridOtherProperty.Columns[0].Visible = false;
        }

        private void SelectedAddressSave_Click(object sender, EventArgs e)
        {
            int Type = Convert.ToInt32(lblType.Text);
            Cursor = Cursors.WaitCursor;
            int? SelctedAddressId=null;
            int? CaseNumberID = null;
            if (Type == 1)
            {
                 SelctedAddressId = Convert.ToInt32(lblHidenSrchDefendentID.Text.Trim());
            }
            else
            {
                 CaseNumberID = Convert.ToInt32(lblHidenCaseNumberID.Text.Trim());
            }
          
            string StreetNumber = txtStNum.Text.Trim();
            string StreetDirection = radDdlSTDirection.SelectedValue.ToString();
            string StreetName = txtStName.Text.Trim();
            string StreetType = txtStType.Text.Trim();
            string PostDirection = txtPostDir.Text.Trim();
            string UnitNumber = txtUnitNo.Text.Trim();
            string City = txtStCity.Text.Trim();
           
            _vmcase.SaveAddressInLPList(SelctedAddressId, StreetNumber, StreetDirection, StreetName, StreetType, PostDirection, UnitNumber, City, CaseNumberID, Type);
            ClearFields();
           
            BindSaveAbleAddress(Type);
            Cursor = Cursors.Arrow;

        }


        public void BindComboBoxStDirection()
        {
            StDirection[] list = new StDirection[]  {
                 new StDirection("None",""),
                                 new StDirection("N","N"),
                                 new StDirection("NE","NE"),
                                 new StDirection("NW","NW"),
                                 new StDirection("E","E"), 
                                  new StDirection("S","S"),
                                 new StDirection("SE","SE"), 
                                   new StDirection("SW","SW"),
                                 new StDirection("W","W"), 
                               };
            radDdlSTDirection.DataSource = list;
            radDdlSTDirection.DisplayMember = "StDirectionName";
            radDdlSTDirection.ValueMember = "StDirectionValue";

        }
        public class StDirection
        {
            public string StDirectionName { get; set; }
            public string StDirectionValue { get; set; }
            public StDirection(string _StDirectionName, string _StDirectionValue)
            {
                StDirectionName = _StDirectionName;
                StDirectionValue = _StDirectionValue;
            }
        }

        public void ClearFields()
        {
            gridDisplayAddress.DataSource = null;
            txtPalmAddress.Text = "";
            lblHidenCaseNumberID.Text = "";
            lblHidenSrchDefendentID.Text = "";
            txtStNum.Text = "";
            txtStName.Text = "";
            txtStType.Text = "";
            txtPostDir.Text = "";
            txtUnitNo.Text = "";
            txtStCity.Text = "";
        }

        public void GetAddress(tblCaseNumberSearch caseDetails, int Type)
        {
            string Result = "";
          //  string sourcePath = "C:/PDfFiles/";

            _vmcase.searchPDFs = new List<SearchedPdf>();
            if (Type == 1)
            {
                int SrchDefendantID = Convert.ToInt32(caseDetails.SrchDefendantID);
            var _SearchPdfCheck = _vmcase.GetPDFResultRecord(SrchDefendantID);
            if (_SearchPdfCheck != null && _SearchPdfCheck.Count > 0)
            {
                foreach (var item in _SearchPdfCheck)
                {
                    if (item.tblNewAllAddress != null && item.tblNewAllAddress.Count > 0)
                    {
                        _vmcase.searchPDFs = _SearchPdfCheck;
                    }
                    else
                    {
                        Result = SaveAndReadPdfDoc(caseDetails, Result,Type);
                        var _SearchPdf = _vmcase.GetPDFResultRecord(SrchDefendantID);
                        _vmcase.searchPDFs = _SearchPdf;
                    }
                }
            }
            else
            {
                Result = SaveAndReadPdfDoc(caseDetails, Result,Type);
                var _SearchPdf = _vmcase.GetPDFResultRecord(SrchDefendantID);
                _vmcase.searchPDFs = _SearchPdf;
            }
            }
            else
            {
                int CaseNumberID = Convert.ToInt32(caseDetails.CaseNumberID);
               List<AllPageAddress> _AllPageAddress = _vmcase.GetPDFResultblSrchcase(CaseNumberID);
               if (_AllPageAddress != null && _AllPageAddress.Count > 0)
                {
                    _vmcase.AllPageAddress = _AllPageAddress;

                }
                else
                {
                      Result = SaveAndReadPdfDoc(caseDetails, Result,Type);
                       _AllPageAddress = _vmcase.GetPDFResultblSrchcase(CaseNumberID);
                     _vmcase.AllPageAddress = _AllPageAddress;
                }
            }
            BindAddressListTOPopup(_vmcase,Type);

            //SelectAddress frmSelectAddress=new SelectAddress("ff");

            //frmSelectAddress.Show();

        }


        public void BindAddressListTOPopup(VMCase _vmcase, int Type)
        {

            DataTable _dt = new DataTable();
            _dt.Columns.Add("IndexNumber", typeof(string));
            _dt.Columns.Add("Address", typeof(String));
            if (Type == 1)
            {
                int cnt = 1;

                foreach (var item in _vmcase.searchPDFs)
                {
                    if (item.tblNewAllAddress != null && item.tblNewAllAddress != null && item.tblNewAllAddress.Count > 0)
                    {
                        foreach (var tbladdress in item.tblNewAllAddress)
                        {
                            if (tbladdress.Defendantaddress != null && tbladdress.Defendantaddress != "")
                            {
                                string TextAdd = tbladdress.Defendantaddress;
                                DataRow dr = _dt.NewRow();
                                dr["IndexNumber"] = "Address- " + cnt;
                                dr["Address"] = TextAdd;
                                _dt.Rows.Add(dr);
                                cnt++;
                            }
                        }
                    }
                }
            }
            else
            {    
                int cnt=1;
                string TextAdd = "";
                foreach (var tbladdress in _vmcase.AllPageAddress)
                {   
                    for(int CntRow=1; CntRow<=5;CntRow++ ){
                    switch(CntRow){
                        case 1:
                             TextAdd = tbladdress.OnDefendant;
                            if(TextAdd!="" && TextAdd!= null){
                                DataRow dr = _dt.NewRow();
                        dr["IndexNumber"] = "Address- " + cnt;
                        dr["Address"] = TextAdd;
                        _dt.Rows.Add(dr);
                        cnt++;
                            }                     
                            break;
                        case 2:
                             TextAdd = tbladdress.ToDefendant;
                            if(TextAdd!="" && TextAdd!= null){
                                 DataRow dr = _dt.NewRow();
                        dr["IndexNumber"] = "Address- " + cnt;
                        dr["Address"] = TextAdd;
                        _dt.Rows.Add(dr);
                        cnt++;
                            }                      
                            break;
                        case 3:
                             TextAdd = tbladdress.ToWit;
                            if(TextAdd!="" && TextAdd!= null){
                                 DataRow dr = _dt.NewRow();
                        dr["IndexNumber"] = "Address- " + cnt;
                        dr["Address"] = TextAdd;
                        _dt.Rows.Add(dr);
                        cnt++;
                            }                     
                            break;
                        case 4:
                             TextAdd = tbladdress.LocatedAt;
                            if(TextAdd!="" && TextAdd!= null){
                                 DataRow dr = _dt.NewRow();
                        dr["IndexNumber"] = "Address- " + cnt;
                        dr["Address"] = TextAdd;
                        _dt.Rows.Add(dr);
                        cnt++;
                            }                       
                            break;
                        case 5:
                             TextAdd = tbladdress.CopyFurnished;
                            if(TextAdd!="" && TextAdd!= null){
                                 DataRow dr = _dt.NewRow();
                        dr["IndexNumber"] = "Address- " + cnt;
                        dr["Address"] = TextAdd;
                        _dt.Rows.Add(dr);
                        cnt++;
                            }                      
                          break;
                    }
                }
                }
            }
            gridDisplayAddress.DataSource = _dt;
            gridDisplayAddress.Columns[0].Width = 100;
            gridDisplayAddress.Columns[1].Width = 100;
           
        }

        private string SaveAndReadPdfDoc(tblCaseNumberSearch caseDetails, string Result, int Type)
        {
            string sourcePath = "C:/PDfFiles/";

            string CaseNumber = caseDetails.CaseNumber.Trim();
            int SrchDefendantID = Convert.ToInt32(caseDetails.SrchDefendantID);

            DateTime _dt = Convert.ToDateTime(caseDetails.CaseSearchDate);

            string srcdt = _dt.ToString("dd/MM/yyyy hh:mm");
            string[] SrchedDate = srcdt.Split('/');
            var Foldername = CaseNumber + "_" + SrchedDate[0] + "_" + SrchedDate[1] + "_" + SrchedDate[2].Split(' ')[0] + "_" + SrchedDate[2].Split(' ')[1].Split(':')[0] + "_" + SrchedDate[2].Split(' ')[1].Split(':')[1] + "";

            if (System.IO.Directory.Exists(sourcePath))
            {
                string[] _Dir = System.IO.Directory.GetDirectories(sourcePath);

                var FilesPath = sourcePath + Foldername;
                bool _result = _Dir.Contains(FilesPath.Trim());
                if (_result == true)
                {
                    string dirName = new DirectoryInfo(FilesPath).Name;
                    if (dirName == Foldername)
                    {
                        string[] files11 = System.IO.Directory.GetFiles(FilesPath);
                        string[] filteredFiles = files11.Where(w => w.Contains(CaseNumber)).ToArray();

                        //string directoryPath = Server.MapPath("~/PdfToImage/" + Foldername);
                        string directoryPath = Path.GetDirectoryName(Application.ExecutablePath) + "\\PdfToImage\\" + Foldername;
                        if (!System.IO.Directory.Exists(directoryPath))
                        {
                            Directory.CreateDirectory(directoryPath);
                        }

                        foreach (var _file in filteredFiles)
                        {
                            if (System.IO.Path.GetExtension(_file) == ".PDF")
                            {
                                var fileName = System.IO.Path.GetFileName(_file);
                                string FileCaseNo = fileName.Split('_')[0];
                                string SplitCaseNumber = FileCaseNo.Split('-')[0];
                                if (SplitCaseNumber.Trim() == CaseNumber)
                                {
                                    var PdfFilePath = "C:/PDfFiles/" + Foldername + "/" + fileName;
                                    //var PdfFilePath = Path.GetDirectoryName(Application.ExecutablePath) + "\\PdfToImage\\" + Foldername + "\\" + fileName;
                                    Result = "Success";
                                    myImages.Clear();
                                    ConvertPdftoImage(PdfFilePath, Foldername);
                                    string tt = "";
                                    SearchedPdf _searchedPDF = new SearchedPdf();
                                    _searchedPDF = SavePDFResultDoc(caseDetails, Foldername);

                                    if (_searchedPDF.pdfAddresslist.Count() > 0)
                                    {
                                        _vmcase.searchPDFs.Add(_searchedPDF);
                                    }

                                }
                            }
                            else
                            {
                                Result = "No eSummons Find In This Case";
                            }
                        }
                        _vmcase.SaveAddressResult(_vmcase, SrchDefendantID,Type);
                    }
                }
            }
            return Result;
        }

        private void ConvertPdftoImage(string filePath, string Foldername)
        {

            var _pdfReader = new PdfReader(filePath); //other filestream etc
            string workFile;
            StringBuilder text = new StringBuilder();
            using (PdfReader reader = new PdfReader(filePath))
            {


                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    string _Imagename = LoadImage(filePath, i, Foldername);
                    myImages.Add(_Imagename);
                }
            }
        }
        public string LoadImage(string InputPDFFile, int PageNumber, string Foldername)
        {

            string outImageName = System.IO.Path.GetFileNameWithoutExtension(InputPDFFile);
            outImageName = outImageName + "_" + PageNumber.ToString() + "_.png";

            //var imagename = Server.MapPath(@"~/PdfToImage/" + Foldername + "/" + outImageName);
            var imagename = Path.GetDirectoryName(Application.ExecutablePath) + "\\PdfToImage\\" + Foldername + "\\" + outImageName;
            
            if (System.IO.File.Exists(imagename))
            {
                System.IO.File.Delete(imagename);
            }

            GhostscriptPngDevice dev = new GhostscriptPngDevice(GhostscriptPngDeviceType.Png256);
            dev.GraphicsAlphaBits = GhostscriptImageDeviceAlphaBits.V_4;
            dev.TextAlphaBits = GhostscriptImageDeviceAlphaBits.V_4;
            dev.ResolutionXY = new GhostscriptImageDeviceResolution(290, 290);
            dev.InputFiles.Add(InputPDFFile);
            dev.Pdf.FirstPage = PageNumber;
            dev.Pdf.LastPage = PageNumber;
            dev.CustomSwitches.Add("-dDOINTERPOLATE");
            //dev.OutputPath = Server.MapPath(@"~/PdfToImage/" + Foldername + "/" + outImageName);
            dev.OutputPath = Path.GetDirectoryName(Application.ExecutablePath) + "\\PdfToImage\\" + Foldername + "\\" + outImageName;
            dev.Process();
            return outImageName;
        }

        private SearchedPdf SavePDFResultDoc(tblCaseNumberSearch caseDetails, string Foldername)
        {
            try
            {
                string _OcrText = "";
                SearchedPdf _searchedPDF = new SearchedPdf();
                _searchedPDF.pdfAddresslist = new List<PDFAddress>();
                foreach (var _Imagename in myImages)
                {
                    //string fImagename = System.IO.Path.Combine(Server.MapPath("~/PdfToImage/" + Foldername + "/" + _Imagename));
                    string fImagename = Path.GetDirectoryName(Application.ExecutablePath) + "\\PdfToImage\\" + Foldername + "\\" + _Imagename;
                    string extractText = this.ExtractTextFromImage(fImagename);
                    _OcrText = extractText.Replace(Environment.NewLine, "<br />");
                    TextFormate.Add(_OcrText);

                    string[] numbers = Regex.Split(extractText, @"\D+");

                    int cntDefendant = 0, cntLocated = 0, cntTowit = 0, cntondefendant = 0, cntCopyFurnishedTo = 0, cntprincipal = 0, cntSERVER = 0, cntaddressis = 0;

                    string _PdfName = _Imagename.ToString();
                    int CaseNumberID = Convert.ToInt32(caseDetails.CaseNumberID);
                    _searchedPDF.Pdfname = _PdfName.Split('_')[0];
                    _searchedPDF.SerchedDate = Convert.ToDateTime(caseDetails.CaseSearchDate);
                    _searchedPDF.CaseNumber = caseDetails.CaseNumber;
                    _searchedPDF.CaseNumberID = CaseNumberID;
                    foreach (string value1 in numbers)
                    {
                        Regex rex = new Regex(@"\b[0-9]{5}(?:-[0-9]{4})?\b");

                        if (rex.IsMatch(value1) == true)
                        {
                            PDFAddress _Pdfadd = new PDFAddress();

                            var filtrtr = value1.ToString();
                            var txtfilter = extractText.Split(new string[] { filtrtr }, StringSplitOptions.None);

                            //---------------- Start Get Any Type Address-----------//

                            string mystring = txtfilter[0].ToString();
                            string newAddress = mystring.Substring(Math.Max(0, mystring.Length - 70));
                            _Pdfadd.AddressType = "New Address:";
                            _Pdfadd.Pdfaddress = newAddress + " " + value1;
                            _searchedPDF.pdfAddresslist.Add(_Pdfadd);

                            //----------------End Get Any Type Address-----------//
                        }

                    }

                }

                return _searchedPDF;
            }
            catch
            {
                return null;
            }
        }

        private string ExtractTextFromImage(string filePath)
        {
            MODI.Document modiDocument = new MODI.Document();
            modiDocument.Create(filePath);
            modiDocument.OCR(MiLANGUAGES.miLANG_ENGLISH);
            MODI.Image modiImage = (modiDocument.Images[0] as MODI.Image);
            string extractedText = modiImage.Layout.Text;
            modiDocument.Close();
            return extractedText;
        }

        private void gridDisplayAddress_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string _Text = gridDisplayAddress.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtPalmAddress.Text = _Text;

        }

        private void GridSaveableAddress_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == GridSaveableAddress.Columns["CaseNumber"].Index && e.RowIndex >= 0)
            {
                Cursor = Cursors.WaitCursor;
                int Type = Convert.ToInt32(lblSrchType.Text);
                tblCaseNumberSearch caseDetails;
                if (Type == 1)
                {
                    int SrchDefendantID = Convert.ToInt32(GridSaveableAddress.Rows[e.RowIndex].Cells[0].Value.ToString());
                    lblHidenSrchDefendentID.Text = SrchDefendantID.ToString();
                    caseDetails = _vmcase.GetcaseDetails(SrchDefendantID, Type);
                }
                else
                {
                    int CaseNumberID = Convert.ToInt32(GridSaveableAddress.Rows[e.RowIndex].Cells[0].Value.ToString());
                    caseDetails = _vmcase.GetcaseDetails(CaseNumberID, Type);

                }

                label5.Text = "Case Number : " + caseDetails.CaseNumber;

                if (caseDetails != null)
                {

                    GetAddress(caseDetails, Type);
                }
                else
                {
                    gridDisplayAddress.Columns.Add("No Record", "No Record");
                    gridDisplayAddress.Rows[0].Cells[0].Value = "No Records Found.";
                    gridDisplayAddress.Columns[0].Width = 200;
                }
               
                Cursor = Cursors.Arrow;
            }
        }

        private void gridDisplayAddress_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == gridDisplayAddress.Columns["IndexNumber"].Index && e.RowIndex >= 0)
            {                                       
                string _Text = gridDisplayAddress.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtPalmAddress.Text = _Text;

            }
        }

        private void dataGridOtherProperty_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridOtherProperty.Columns["FolioId"].Index && e.RowIndex >= 0)
            {
                int OTHAddressID = Convert.ToInt32(dataGridOtherProperty.Rows[e.RowIndex].Cells[0].Value.ToString());
                string _Text = dataGridOtherProperty.Rows[e.RowIndex].Cells[2].Value.ToString();
                int SrchDefendentID = Convert.ToInt32(lblHidenSrchDefendentID.Text.ToString());
                //Save Correct Address In Main Lp List
                _vmcase.SaveLpcaseAddressByFolio(SrchDefendentID, OTHAddressID, _Text);
                dataGridOtherProperty.DataSource = null;
                gridDisplayAddress.DataSource = null;
                int Type = Convert.ToInt32(lblType.Text);
                BindSaveAbleAddress(Type);
            }
        }



    }
}
