﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using iTextSharp;

namespace SharkApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            base.Dispose(disposing);
        }
       

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem9 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem10 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem11 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem12 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem13 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem14 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem15 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem16 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem17 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem18 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem19 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem20 = new Telerik.WinControls.UI.RadListDataItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            Telerik.WinControls.UI.RadListDataItem radListDataItem21 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem22 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem23 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem24 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            this.lblTab = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDateTo = new System.Windows.Forms.Label();
            this.lbldatefrom = new System.Windows.Forms.Label();
            this.lblBusinessName = new System.Windows.Forms.Label();
            this.lblCourtYpe = new System.Windows.Forms.Label();
            this.lblBusinessNameTitle = new System.Windows.Forms.Label();
            this.radDropDownList1 = new Telerik.WinControls.UI.RadDropDownList();
            this.BusinessNamePanel = new Telerik.WinControls.UI.RadPanel();
            this.btnBeginSearch = new Telerik.WinControls.UI.RadButton();
            this.txtdateTo = new Telerik.WinControls.UI.RadDateTimePicker();
            this.txtDateFrom = new Telerik.WinControls.UI.RadDateTimePicker();
            this.txtBusinessName = new Telerik.WinControls.UI.RadTextBox();
            this.radDropDownList2 = new Telerik.WinControls.UI.RadDropDownList();
            this.PropertysearchPanel = new Telerik.WinControls.UI.RadPanel();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnsrchByLPAddress = new System.Windows.Forms.Button();
            this.dgvwsunbizgrid = new System.Windows.Forms.DataGridView();
            this.label11 = new System.Windows.Forms.Label();
            this.lblpropertyAddress = new System.Windows.Forms.Label();
            this.radCompleteAddressPanel = new Telerik.WinControls.UI.RadPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtUnitNumber = new Telerik.WinControls.UI.RadTextBox();
            this.txtPostDirection = new Telerik.WinControls.UI.RadTextBox();
            this.txtStreetNumber = new Telerik.WinControls.UI.RadTextBox();
            this.txtStreeCity = new Telerik.WinControls.UI.RadTextBox();
            this.txtStreetType = new Telerik.WinControls.UI.RadTextBox();
            this.txtStreetDirection = new Telerik.WinControls.UI.RadTextBox();
            this.txtStreetName = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.radForBulkCase = new Telerik.WinControls.UI.RadRadioButton();
            this.radforLPcase = new Telerik.WinControls.UI.RadRadioButton();
            this.txtPropertyAddresss = new Telerik.WinControls.UI.RadTextBox();
            this.PropertSearch = new Telerik.WinControls.UI.RadButton();
            this.radbtnFolioNumber = new Telerik.WinControls.UI.RadRadioButton();
            this.radbtnCompleteAddress = new Telerik.WinControls.UI.RadRadioButton();
            this.radbtnIndividualsearchBroward = new Telerik.WinControls.UI.RadRadioButton();
            this.GetLpCaseForBroward = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.PartyPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblLastName = new System.Windows.Forms.Label();
            this.lblMiddleName = new System.Windows.Forms.Label();
            this.txtLastName = new Telerik.WinControls.UI.RadTextBox();
            this.txtMiddleName = new Telerik.WinControls.UI.RadTextBox();
            this.PartySearch = new Telerik.WinControls.UI.RadButton();
            this.txtPartyDateTo = new Telerik.WinControls.UI.RadDateTimePicker();
            this.txtPartyDateFrom = new Telerik.WinControls.UI.RadDateTimePicker();
            this.txtFirstName = new Telerik.WinControls.UI.RadTextBox();
            this.radDropDownParty = new Telerik.WinControls.UI.RadDropDownList();
            this.lblPartNameTitle = new System.Windows.Forms.Label();
            this.lblPartDateTo = new System.Windows.Forms.Label();
            this.lblPartCourtType = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblPartDateFrom = new System.Windows.Forms.Label();
            this.dailySearchPanel = new Telerik.WinControls.UI.RadPanel();
            this.linkBtnSpokioDetails = new System.Windows.Forms.LinkLabel();
            this.ClearCacheDefandent = new System.Windows.Forms.LinkLabel();
            this.label34 = new System.Windows.Forms.Label();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.GetDetailsFromBCPA = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label26 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.btnSaveAddress = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.GettAddress = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DDSearchDay = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.DefendantSearch = new Telerik.WinControls.UI.RadButton();
            this.lblError1 = new Telerik.WinControls.UI.RadLabel();
            this.lblError = new Telerik.WinControls.UI.RadLabel();
            this.DDDocumentType = new Telerik.WinControls.UI.RadDropDownList();
            this.lblDocType = new Telerik.WinControls.UI.RadLabel();
            this.radEndDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radBeginDate = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblEnddate = new Telerik.WinControls.UI.RadLabel();
            this.lblBeginDate = new Telerik.WinControls.UI.RadLabel();
            this.lblcasenumber = new System.Windows.Forms.Label();
            this.lblcaseNumberTitle = new System.Windows.Forms.Label();
            this.CasePanel = new Telerik.WinControls.UI.RadPanel();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.GetDetailsForBulk = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.btnAssignAddressForBulk = new System.Windows.Forms.Button();
            this.radPanelBulk = new Telerik.WinControls.UI.RadPanel();
            this.btnMannualBulk = new System.Windows.Forms.Button();
            this.txtBulkCase = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.btnCaseSearch = new Telerik.WinControls.UI.RadButton();
            this.txtCaseNumber = new Telerik.WinControls.UI.RadTextBox();
            this.radRadioBulk = new Telerik.WinControls.UI.RadRadioButton();
            this.radRadioSingle = new Telerik.WinControls.UI.RadRadioButton();
            this.radTextBox3 = new Telerik.WinControls.UI.RadTextBox();
            this.radDateTimePicker1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.CitationPanel = new Telerik.WinControls.UI.RadPanel();
            this.lblCitationTitle = new System.Windows.Forms.Label();
            this.lblCitationNumber = new System.Windows.Forms.Label();
            this.btnCitationSearch = new Telerik.WinControls.UI.RadButton();
            this.txtCitationNumber = new Telerik.WinControls.UI.RadTextBox();
            this.Home = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.BussinessName = new Telerik.WinControls.UI.RadMenuItem();
            this.PartyName = new Telerik.WinControls.UI.RadMenuItem();
            this.CaseNumber = new Telerik.WinControls.UI.RadMenuItem();
            this.CitationNumber = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuGeneral = new Telerik.WinControls.UI.RadMenuItem();
            this.radmenuBroward = new Telerik.WinControls.UI.RadMenuItem();
            this.lblselectCategory = new Telerik.WinControls.UI.RadLabel();
            this.PalmBeachPanel = new Telerik.WinControls.UI.RadPanel();
            this.lnkPalmSpokeoDetails = new System.Windows.Forms.LinkLabel();
            this.lblpalmpending = new Telerik.WinControls.UI.RadLabel();
            this.linkRefreshPalmCases = new System.Windows.Forms.LinkLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.btnPCBPalmDetails = new Telerik.WinControls.UI.RadButton();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.PalmSelectCorrectAddreess = new Telerik.WinControls.UI.RadButton();
            this.link90Days = new System.Windows.Forms.LinkLabel();
            this.link30Days = new System.Windows.Forms.LinkLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.link7Days = new System.Windows.Forms.LinkLabel();
            this.label37 = new System.Windows.Forms.Label();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radDDlDisplayRecords = new Telerik.WinControls.UI.RadDropDownList();
            this.radDDlTotalRecord = new Telerik.WinControls.UI.RadDropDownList();
            this.PalmBeachsrch = new Telerik.WinControls.UI.RadButton();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.txtPalmDocType = new System.Windows.Forms.TextBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.label35 = new System.Windows.Forms.Label();
            this.radPalmDateFrom = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radPalmDateTo = new Telerik.WinControls.UI.RadDateTimePicker();
            this.lblInternetConnection = new System.Windows.Forms.Label();
            this.radmenuPalmBeach = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuMiamiDade = new Telerik.WinControls.UI.RadMenuItem();
            this.pnlGeneral = new Telerik.WinControls.UI.RadPanel();
            this.pnlBulk = new Telerik.WinControls.UI.RadPanel();
            this.btnSearchAllCasesAddress = new Telerik.WinControls.UI.RadButton();
            this.lblPendingBulkCases = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.radBulkCases = new Telerik.WinControls.UI.RadRadioButton();
            this.radLPcases = new Telerik.WinControls.UI.RadRadioButton();
            this.lblPendingLPcases = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.pnlsearchByFolio = new Telerik.WinControls.UI.RadPanel();
            this.lblFolioNumber = new System.Windows.Forms.Label();
            this.txtFolioNumber = new Telerik.WinControls.UI.RadTextBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radBtnScheduledSearch = new Telerik.WinControls.UI.RadRadioButton();
            this.gbSelectCounty = new Telerik.WinControls.UI.RadGroupBox();
            this.cmbCountyList = new Telerik.WinControls.UI.RadDropDownList();
            this.pnlSearchByAddress = new Telerik.WinControls.UI.RadPanel();
            this.txtUnitNumber1 = new Telerik.WinControls.UI.RadTextBox();
            this.txtPostDirection1 = new Telerik.WinControls.UI.RadTextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.txtStreetNumber1 = new Telerik.WinControls.UI.RadTextBox();
            this.txtStreeCity1 = new Telerik.WinControls.UI.RadTextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txtStreetType1 = new Telerik.WinControls.UI.RadTextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.txtStreetDirection1 = new Telerik.WinControls.UI.RadTextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.txtStreetName1 = new Telerik.WinControls.UI.RadTextBox();
            this.btnManualPropSearch = new Telerik.WinControls.UI.RadButton();
            this.pnlsearchspokeosingleaddress = new Telerik.WinControls.UI.RadPanel();
            this.lblspokeosingleaddress = new System.Windows.Forms.Label();
            this.txtspokeosingleaddress = new Telerik.WinControls.UI.RadTextBox();
            this.object_73c9a747_22cf_4f3a_a660_2813c219558a = new Telerik.WinControls.RootRadElement();
            this.SelectCategoryMenu = new Telerik.WinControls.UI.RadMenu();
            this.radMenuSpokeoSearch = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSunbizSearch = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuFSBOSearch = new Telerik.WinControls.UI.RadMenuItem();
            this.radmenuSocialmediaSearch = new Telerik.WinControls.UI.RadMenuItem();
            this.pnlSpokeoSearch = new Telerik.WinControls.UI.RadPanel();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.cmbSpokeoCountyList = new Telerik.WinControls.UI.RadDropDownList();
            this.btnBeginSpokeoSearch = new Telerik.WinControls.UI.RadButton();
            this.radGrpSearchByPerson = new Telerik.WinControls.UI.RadGroupBox();
            this.radAllPerson = new Telerik.WinControls.UI.RadRadioButton();
            this.radSearchByBulkPerson = new Telerik.WinControls.UI.RadRadioButton();
            this.radSearchBySinglePerson = new Telerik.WinControls.UI.RadRadioButton();
            this.radGrpSearchByAddress = new Telerik.WinControls.UI.RadGroupBox();
            this.radAllAddress = new Telerik.WinControls.UI.RadRadioButton();
            this.radSearchByOneAddress = new Telerik.WinControls.UI.RadRadioButton();
            this.radSearchByBulkAddress = new Telerik.WinControls.UI.RadRadioButton();
            this.lblSpokeoSearch = new System.Windows.Forms.Label();
            this.lblFSBOSearch = new System.Windows.Forms.Label();
            this.lblSocialMedia = new System.Windows.Forms.Label();
            this.pnlSunbizsearch = new Telerik.WinControls.UI.RadPanel();
            this.radSunbizGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.cmbSunbizCountyList = new Telerik.WinControls.UI.RadDropDownList();
            this.lblSunbizSearch = new System.Windows.Forms.Label();
            this.radGrpSunbizSearchNew = new Telerik.WinControls.UI.RadGroupBox();
            this.radSearchByZipcode = new Telerik.WinControls.UI.RadCheckBox();
            this.radSearchBySunbizCounty = new Telerik.WinControls.UI.RadCheckBox();
            this.radSocialGroupBox = new Telerik.WinControls.UI.RadGroupBox();
            this.cmbSocialmediaCountyList = new Telerik.WinControls.UI.RadDropDownList();
            this.radGrpSunbizSearch = new Telerik.WinControls.UI.RadGroupBox();
            this.radSunbizName = new Telerik.WinControls.UI.RadRadioButton();
            this.radSearchBySunbizAddress = new Telerik.WinControls.UI.RadRadioButton();
            this.radGrpFSBOSearchCategory = new Telerik.WinControls.UI.RadGroupBox();
            this.cmbFSBOSearchCategory = new Telerik.WinControls.UI.RadDropDownList();
            this.pnlFSBOsearch = new Telerik.WinControls.UI.RadPanel();
            this.pnlSocialmedia = new Telerik.WinControls.UI.RadPanel();
            this.radGrpFSBOSearch = new Telerik.WinControls.UI.RadGroupBox();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.CompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Officer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Zipcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupbox1 = new System.Windows.Forms.GroupBox();
            this.txtboxEmailID = new System.Windows.Forms.TextBox();
            this.txtboxPasswrd = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblpassword = new System.Windows.Forms.Label();
            this.lblTwitterPassword = new System.Windows.Forms.Label();
            this.lblTwitterEmail = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnBeginSunbizSearch = new System.Windows.Forms.Button();
            this.btnSocialmediasearch = new System.Windows.Forms.Button();
            this.btnRefreshSocialmediasearch = new System.Windows.Forms.Button();
            this.btnfsbosearch = new System.Windows.Forms.Button();
            this.btnfsboRefresh = new System.Windows.Forms.Button();
            this.groupFSBO = new System.Windows.Forms.GroupBox();
            this.cmbFSBOSearchCategopry = new System.Windows.Forms.ComboBox();
            this.radFSBOSearchGroupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbFSBOSearchCountyList1 = new System.Windows.Forms.ComboBox();
            this.grpbsingleaddress = new System.Windows.Forms.GroupBox();
            this.richsingleaddress = new System.Windows.Forms.RichTextBox();
            this.lblsingleaddress = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.dgvwbulkAddress = new System.Windows.Forms.DataGridView();
            this.BulkAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpbEntries = new System.Windows.Forms.GroupBox();
            this.cmbEntries = new System.Windows.Forms.ComboBox();
            this.cmbsearchdays = new System.Windows.Forms.ComboBox();
            this.grpbSearchdays = new System.Windows.Forms.GroupBox();
            this.grpbdatefromto = new System.Windows.Forms.GroupBox();
            this.dtmfsbodateto = new System.Windows.Forms.DateTimePicker();
            this.dtmfsbodatefrom = new System.Windows.Forms.DateTimePicker();
            this.lblfsbodateto = new System.Windows.Forms.Label();
            this.lblfsbodatefrom = new System.Windows.Forms.Label();
            this.btnResum = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessNamePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBeginSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBusinessName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertysearchPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwsunbizgrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCompleteAddressPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostDirection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreeCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetDirection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radForBulkCase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radforLPcase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPropertyAddresss)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbtnFolioNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbtnCompleteAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbtnIndividualsearchBroward)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartyPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddleName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartySearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownParty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dailySearchPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DDSearchDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefendantSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblError1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DDDocumentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDocType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEnddate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBeginDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CasePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelBulk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCaseSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaseNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioBulk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioSingle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CitationPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCitationSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCitationNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblselectCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PalmBeachPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblpalmpending)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPCBPalmDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PalmSelectCorrectAddreess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDDlDisplayRecords)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDDlTotalRecord)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PalmBeachsrch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPalmDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPalmDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGeneral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBulk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearchAllCasesAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBulkCases)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLPcases)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlsearchByFolio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFolioNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnScheduledSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbSelectCounty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCountyList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSearchByAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitNumber1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostDirection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetNumber1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreeCity1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetType1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetDirection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetName1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnManualPropSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlsearchspokeosingleaddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtspokeosingleaddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectCategoryMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSpokeoSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSpokeoCountyList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBeginSpokeoSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGrpSearchByPerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radAllPerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSearchByBulkPerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSearchBySinglePerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGrpSearchByAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radAllAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSearchByOneAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSearchByBulkAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSunbizsearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSunbizGroupBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSunbizCountyList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGrpSunbizSearchNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSearchByZipcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSearchBySunbizCounty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSocialGroupBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSocialmediaCountyList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGrpSunbizSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSunbizName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSearchBySunbizAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGrpFSBOSearchCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFSBOSearchCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlFSBOsearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSocialmedia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGrpFSBOSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupbox1.SuspendLayout();
            this.groupFSBO.SuspendLayout();
            this.radFSBOSearchGroupBox1.SuspendLayout();
            this.grpbsingleaddress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwbulkAddress)).BeginInit();
            this.grpbEntries.SuspendLayout();
            this.grpbSearchdays.SuspendLayout();
            this.grpbdatefromto.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTab
            // 
            this.lblTab.AutoSize = true;
            this.lblTab.Location = new System.Drawing.Point(33, 23);
            this.lblTab.Name = "lblTab";
            this.lblTab.Size = new System.Drawing.Size(59, 13);
            this.lblTab.TabIndex = 1;
            this.lblTab.Text = "Select Tab";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 0;
            // 
            // lblDateTo
            // 
            this.lblDateTo.AutoSize = true;
            this.lblDateTo.Location = new System.Drawing.Point(35, 160);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Size = new System.Drawing.Size(45, 13);
            this.lblDateTo.TabIndex = 9;
            this.lblDateTo.Text = "Date To";
            // 
            // lbldatefrom
            // 
            this.lbldatefrom.AutoSize = true;
            this.lbldatefrom.Location = new System.Drawing.Point(35, 130);
            this.lbldatefrom.Name = "lbldatefrom";
            this.lbldatefrom.Size = new System.Drawing.Size(60, 13);
            this.lbldatefrom.TabIndex = 6;
            this.lbldatefrom.Text = "Date From";
            // 
            // lblBusinessName
            // 
            this.lblBusinessName.AutoSize = true;
            this.lblBusinessName.Location = new System.Drawing.Point(35, 81);
            this.lblBusinessName.Name = "lblBusinessName";
            this.lblBusinessName.Size = new System.Drawing.Size(84, 13);
            this.lblBusinessName.TabIndex = 5;
            this.lblBusinessName.Text = "Business Name";
            // 
            // lblCourtYpe
            // 
            this.lblCourtYpe.AutoSize = true;
            this.lblCourtYpe.Location = new System.Drawing.Point(35, 47);
            this.lblCourtYpe.Name = "lblCourtYpe";
            this.lblCourtYpe.Size = new System.Drawing.Size(61, 13);
            this.lblCourtYpe.TabIndex = 1;
            this.lblCourtYpe.Text = "Court Type";
            // 
            // lblBusinessNameTitle
            // 
            this.lblBusinessNameTitle.AutoSize = true;
            this.lblBusinessNameTitle.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblBusinessNameTitle.Location = new System.Drawing.Point(11, 16);
            this.lblBusinessNameTitle.Name = "lblBusinessNameTitle";
            this.lblBusinessNameTitle.Size = new System.Drawing.Size(157, 19);
            this.lblBusinessNameTitle.TabIndex = 0;
            this.lblBusinessNameTitle.Text = "Business Name Search";
            // 
            // radDropDownList1
            // 
            this.radDropDownList1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            radListDataItem9.Text = "Business Name";
            radListDataItem9.TextWrap = true;
            radListDataItem10.Text = "party Name";
            radListDataItem10.TextWrap = true;
            radListDataItem11.Text = "Case Number";
            radListDataItem11.TextWrap = true;
            radListDataItem12.Text = "Citation Number";
            radListDataItem12.TextWrap = true;
            this.radDropDownList1.Items.Add(radListDataItem9);
            this.radDropDownList1.Items.Add(radListDataItem10);
            this.radDropDownList1.Items.Add(radListDataItem11);
            this.radDropDownList1.Items.Add(radListDataItem12);
            this.radDropDownList1.Location = new System.Drawing.Point(304, 43);
            this.radDropDownList1.Name = "radDropDownList1";
            // 
            // 
            // 
            this.radDropDownList1.RootElement.AccessibleDescription = null;
            this.radDropDownList1.RootElement.AccessibleName = null;
            this.radDropDownList1.RootElement.ControlBounds = new System.Drawing.Rectangle(304, 43, 125, 20);
            this.radDropDownList1.RootElement.StretchVertically = true;
            this.radDropDownList1.Size = new System.Drawing.Size(203, 20);
            this.radDropDownList1.TabIndex = 0;
            this.radDropDownList1.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.radDropDownList1_SelectedIndexChanged);
            // 
            // BusinessNamePanel
            // 
            this.BusinessNamePanel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.BusinessNamePanel.Controls.Add(this.btnBeginSearch);
            this.BusinessNamePanel.Controls.Add(this.txtdateTo);
            this.BusinessNamePanel.Controls.Add(this.txtDateFrom);
            this.BusinessNamePanel.Controls.Add(this.txtBusinessName);
            this.BusinessNamePanel.Controls.Add(this.radDropDownList2);
            this.BusinessNamePanel.Controls.Add(this.lblBusinessNameTitle);
            this.BusinessNamePanel.Controls.Add(this.lblDateTo);
            this.BusinessNamePanel.Controls.Add(this.lblCourtYpe);
            this.BusinessNamePanel.Controls.Add(this.lblBusinessName);
            this.BusinessNamePanel.Controls.Add(this.lbldatefrom);
            this.BusinessNamePanel.Location = new System.Drawing.Point(177, 18);
            this.BusinessNamePanel.Name = "BusinessNamePanel";
            // 
            // 
            // 
            this.BusinessNamePanel.RootElement.AccessibleDescription = null;
            this.BusinessNamePanel.RootElement.AccessibleName = null;
            this.BusinessNamePanel.RootElement.ControlBounds = new System.Drawing.Rectangle(177, 18, 200, 100);
            this.BusinessNamePanel.Size = new System.Drawing.Size(608, 46);
            this.BusinessNamePanel.TabIndex = 3;
            // 
            // btnBeginSearch
            // 
            this.btnBeginSearch.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnBeginSearch.Location = new System.Drawing.Point(187, 202);
            this.btnBeginSearch.Name = "btnBeginSearch";
            // 
            // 
            // 
            this.btnBeginSearch.RootElement.AccessibleDescription = null;
            this.btnBeginSearch.RootElement.AccessibleName = null;
            this.btnBeginSearch.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 202, 110, 24);
            this.btnBeginSearch.Size = new System.Drawing.Size(142, 35);
            this.btnBeginSearch.TabIndex = 14;
            this.btnBeginSearch.Text = "Begin Search";
            this.btnBeginSearch.Click += new System.EventHandler(this.btnBeginSearch_Click_1);
            // 
            // txtdateTo
            // 
            this.txtdateTo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtdateTo.Location = new System.Drawing.Point(187, 160);
            this.txtdateTo.Name = "txtdateTo";
            // 
            // 
            // 
            this.txtdateTo.RootElement.AccessibleDescription = null;
            this.txtdateTo.RootElement.AccessibleName = null;
            this.txtdateTo.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 160, 164, 20);
            this.txtdateTo.RootElement.StretchVertically = true;
            this.txtdateTo.Size = new System.Drawing.Size(203, 20);
            this.txtdateTo.TabIndex = 13;
            this.txtdateTo.TabStop = false;
            this.txtdateTo.Text = "Wednesday, August 10, 2016";
            this.txtdateTo.Value = new System.DateTime(2016, 8, 10, 16, 6, 4, 785);
            // 
            // txtDateFrom
            // 
            this.txtDateFrom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtDateFrom.Location = new System.Drawing.Point(187, 122);
            this.txtDateFrom.Name = "txtDateFrom";
            // 
            // 
            // 
            this.txtDateFrom.RootElement.AccessibleDescription = null;
            this.txtDateFrom.RootElement.AccessibleName = null;
            this.txtDateFrom.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 122, 164, 20);
            this.txtDateFrom.RootElement.StretchVertically = true;
            this.txtDateFrom.Size = new System.Drawing.Size(203, 20);
            this.txtDateFrom.TabIndex = 12;
            this.txtDateFrom.TabStop = false;
            this.txtDateFrom.Text = "Wednesday, August 10, 2016";
            this.txtDateFrom.Value = new System.DateTime(2016, 8, 10, 16, 1, 0, 0);
            // 
            // txtBusinessName
            // 
            this.txtBusinessName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtBusinessName.Location = new System.Drawing.Point(187, 73);
            this.txtBusinessName.Name = "txtBusinessName";
            // 
            // 
            // 
            this.txtBusinessName.RootElement.AccessibleDescription = null;
            this.txtBusinessName.RootElement.AccessibleName = null;
            this.txtBusinessName.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 73, 100, 20);
            this.txtBusinessName.RootElement.StretchVertically = true;
            this.txtBusinessName.Size = new System.Drawing.Size(203, 20);
            this.txtBusinessName.TabIndex = 11;
            // 
            // radDropDownList2
            // 
            this.radDropDownList2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radDropDownList2.Location = new System.Drawing.Point(187, 40);
            this.radDropDownList2.Name = "radDropDownList2";
            // 
            // 
            // 
            this.radDropDownList2.RootElement.AccessibleDescription = null;
            this.radDropDownList2.RootElement.AccessibleName = null;
            this.radDropDownList2.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 40, 125, 20);
            this.radDropDownList2.RootElement.StretchVertically = true;
            this.radDropDownList2.Size = new System.Drawing.Size(203, 20);
            this.radDropDownList2.TabIndex = 10;
            // 
            // PropertysearchPanel
            // 
            this.PropertysearchPanel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.PropertysearchPanel.Controls.Add(this.label25);
            this.PropertysearchPanel.Controls.Add(this.label24);
            this.PropertysearchPanel.Controls.Add(this.label17);
            this.PropertysearchPanel.Controls.Add(this.label15);
            this.PropertysearchPanel.Controls.Add(this.btnsrchByLPAddress);
            this.PropertysearchPanel.Controls.Add(this.dgvwsunbizgrid);
            this.PropertysearchPanel.Controls.Add(this.label11);
            this.PropertysearchPanel.Controls.Add(this.lblpropertyAddress);
            this.PropertysearchPanel.Controls.Add(this.radCompleteAddressPanel);
            this.PropertysearchPanel.Controls.Add(this.label4);
            this.PropertysearchPanel.Controls.Add(this.radForBulkCase);
            this.PropertysearchPanel.Controls.Add(this.radforLPcase);
            this.PropertysearchPanel.Controls.Add(this.txtPropertyAddresss);
            this.PropertysearchPanel.Controls.Add(this.PropertSearch);
            this.PropertysearchPanel.Location = new System.Drawing.Point(25, 10);
            this.PropertysearchPanel.Name = "PropertysearchPanel";
            // 
            // 
            // 
            this.PropertysearchPanel.RootElement.AccessibleDescription = null;
            this.PropertysearchPanel.RootElement.AccessibleName = null;
            this.PropertysearchPanel.RootElement.ControlBounds = new System.Drawing.Rectangle(25, 10, 200, 100);
            this.PropertysearchPanel.Size = new System.Drawing.Size(607, 105);
            this.PropertysearchPanel.TabIndex = 10;
            this.PropertysearchPanel.Visible = false;
            this.PropertysearchPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.PropertysearchPanel_Paint);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(35, 356);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(44, 15);
            this.label25.TabIndex = 38;
            this.label25.Text = "label25";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(35, 327);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(105, 15);
            this.label24.TabIndex = 37;
            this.label24.Text = "Choose Search For";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(35, 377);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 15);
            this.label17.TabIndex = 34;
            this.label17.Text = "label17";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(35, 404);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(265, 15);
            this.label15.TabIndex = 33;
            this.label15.Text = "Automatic Search on BCPA web site for all  Cases";
            // 
            // btnsrchByLPAddress
            // 
            this.btnsrchByLPAddress.Location = new System.Drawing.Point(341, 392);
            this.btnsrchByLPAddress.Name = "btnsrchByLPAddress";
            this.btnsrchByLPAddress.Size = new System.Drawing.Size(171, 28);
            this.btnsrchByLPAddress.TabIndex = 32;
            this.btnsrchByLPAddress.Text = "Search all Cases Addresses";
            this.btnsrchByLPAddress.UseVisualStyleBackColor = true;
            this.btnsrchByLPAddress.Click += new System.EventHandler(this.btnsrchByLPAddress_Click);
            // 
            // dgvwsunbizgrid
            // 
            this.dgvwsunbizgrid.Location = new System.Drawing.Point(0, 0);
            this.dgvwsunbizgrid.Name = "dgvwsunbizgrid";
            this.dgvwsunbizgrid.Size = new System.Drawing.Size(240, 150);
            this.dgvwsunbizgrid.TabIndex = 39;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(45, 40);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 15);
            this.label11.TabIndex = 31;
            this.label11.Text = "Search By";
            // 
            // lblpropertyAddress
            // 
            this.lblpropertyAddress.AutoSize = true;
            this.lblpropertyAddress.Location = new System.Drawing.Point(58, 131);
            this.lblpropertyAddress.Name = "lblpropertyAddress";
            this.lblpropertyAddress.Size = new System.Drawing.Size(110, 15);
            this.lblpropertyAddress.TabIndex = 30;
            this.lblpropertyAddress.Text = "Enter Folio Number";
            // 
            // radCompleteAddressPanel
            // 
            this.radCompleteAddressPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radCompleteAddressPanel.Controls.Add(this.label10);
            this.radCompleteAddressPanel.Controls.Add(this.label3);
            this.radCompleteAddressPanel.Controls.Add(this.label5);
            this.radCompleteAddressPanel.Controls.Add(this.label9);
            this.radCompleteAddressPanel.Controls.Add(this.label6);
            this.radCompleteAddressPanel.Controls.Add(this.label8);
            this.radCompleteAddressPanel.Controls.Add(this.label7);
            this.radCompleteAddressPanel.Controls.Add(this.txtUnitNumber);
            this.radCompleteAddressPanel.Controls.Add(this.txtPostDirection);
            this.radCompleteAddressPanel.Controls.Add(this.txtStreetNumber);
            this.radCompleteAddressPanel.Controls.Add(this.txtStreeCity);
            this.radCompleteAddressPanel.Controls.Add(this.txtStreetType);
            this.radCompleteAddressPanel.Controls.Add(this.txtStreetDirection);
            this.radCompleteAddressPanel.Controls.Add(this.txtStreetName);
            this.radCompleteAddressPanel.Location = new System.Drawing.Point(17, 57);
            this.radCompleteAddressPanel.Name = "radCompleteAddressPanel";
            // 
            // 
            // 
            this.radCompleteAddressPanel.RootElement.AccessibleDescription = null;
            this.radCompleteAddressPanel.RootElement.AccessibleName = null;
            this.radCompleteAddressPanel.RootElement.ControlBounds = new System.Drawing.Rectangle(17, 57, 200, 100);
            this.radCompleteAddressPanel.Size = new System.Drawing.Size(473, 208);
            this.radCompleteAddressPanel.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(40, 160);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 15);
            this.label10.TabIndex = 27;
            this.label10.Text = "Unit Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 15);
            this.label3.TabIndex = 26;
            this.label3.Text = "Post Direction";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 15);
            this.label5.TabIndex = 5;
            this.label5.Text = "Street Number ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(40, 189);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 15);
            this.label9.TabIndex = 25;
            this.label9.Text = "Street City";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(40, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 15);
            this.label6.TabIndex = 22;
            this.label6.Text = "Street Direction ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(40, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 15);
            this.label8.TabIndex = 24;
            this.label8.Text = "Street Type ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(40, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 15);
            this.label7.TabIndex = 23;
            this.label7.Text = "Street Name ";
            // 
            // txtUnitNumber
            // 
            this.txtUnitNumber.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtUnitNumber.Location = new System.Drawing.Point(186, 153);
            this.txtUnitNumber.Name = "txtUnitNumber";
            // 
            // 
            // 
            this.txtUnitNumber.RootElement.AccessibleDescription = null;
            this.txtUnitNumber.RootElement.AccessibleName = null;
            this.txtUnitNumber.RootElement.ControlBounds = new System.Drawing.Rectangle(186, 153, 100, 20);
            this.txtUnitNumber.RootElement.StretchVertically = true;
            this.txtUnitNumber.Size = new System.Drawing.Size(203, 20);
            this.txtUnitNumber.TabIndex = 21;
            // 
            // txtPostDirection
            // 
            this.txtPostDirection.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtPostDirection.Location = new System.Drawing.Point(186, 127);
            this.txtPostDirection.Name = "txtPostDirection";
            // 
            // 
            // 
            this.txtPostDirection.RootElement.AccessibleDescription = null;
            this.txtPostDirection.RootElement.AccessibleName = null;
            this.txtPostDirection.RootElement.ControlBounds = new System.Drawing.Rectangle(186, 127, 100, 20);
            this.txtPostDirection.RootElement.StretchVertically = true;
            this.txtPostDirection.Size = new System.Drawing.Size(203, 20);
            this.txtPostDirection.TabIndex = 28;
            // 
            // txtStreetNumber
            // 
            this.txtStreetNumber.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStreetNumber.Location = new System.Drawing.Point(186, 20);
            this.txtStreetNumber.Name = "txtStreetNumber";
            // 
            // 
            // 
            this.txtStreetNumber.RootElement.AccessibleDescription = null;
            this.txtStreetNumber.RootElement.AccessibleName = null;
            this.txtStreetNumber.RootElement.ControlBounds = new System.Drawing.Rectangle(186, 20, 100, 20);
            this.txtStreetNumber.RootElement.StretchVertically = true;
            this.txtStreetNumber.Size = new System.Drawing.Size(203, 20);
            this.txtStreetNumber.TabIndex = 16;
            // 
            // txtStreeCity
            // 
            this.txtStreeCity.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStreeCity.Location = new System.Drawing.Point(186, 182);
            this.txtStreeCity.Name = "txtStreeCity";
            // 
            // 
            // 
            this.txtStreeCity.RootElement.AccessibleDescription = null;
            this.txtStreeCity.RootElement.AccessibleName = null;
            this.txtStreeCity.RootElement.ControlBounds = new System.Drawing.Rectangle(186, 182, 100, 20);
            this.txtStreeCity.RootElement.StretchVertically = true;
            this.txtStreeCity.Size = new System.Drawing.Size(203, 20);
            this.txtStreeCity.TabIndex = 21;
            // 
            // txtStreetType
            // 
            this.txtStreetType.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStreetType.Location = new System.Drawing.Point(186, 100);
            this.txtStreetType.Name = "txtStreetType";
            // 
            // 
            // 
            this.txtStreetType.RootElement.AccessibleDescription = null;
            this.txtStreetType.RootElement.AccessibleName = null;
            this.txtStreetType.RootElement.ControlBounds = new System.Drawing.Rectangle(186, 100, 100, 20);
            this.txtStreetType.RootElement.StretchVertically = true;
            this.txtStreetType.Size = new System.Drawing.Size(203, 20);
            this.txtStreetType.TabIndex = 20;
            // 
            // txtStreetDirection
            // 
            this.txtStreetDirection.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStreetDirection.Location = new System.Drawing.Point(186, 45);
            this.txtStreetDirection.Name = "txtStreetDirection";
            // 
            // 
            // 
            this.txtStreetDirection.RootElement.AccessibleDescription = null;
            this.txtStreetDirection.RootElement.AccessibleName = null;
            this.txtStreetDirection.RootElement.ControlBounds = new System.Drawing.Rectangle(186, 45, 100, 20);
            this.txtStreetDirection.RootElement.StretchVertically = true;
            this.txtStreetDirection.Size = new System.Drawing.Size(203, 20);
            this.txtStreetDirection.TabIndex = 18;
            // 
            // txtStreetName
            // 
            this.txtStreetName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStreetName.Location = new System.Drawing.Point(187, 72);
            this.txtStreetName.Name = "txtStreetName";
            // 
            // 
            // 
            this.txtStreetName.RootElement.AccessibleDescription = null;
            this.txtStreetName.RootElement.AccessibleName = null;
            this.txtStreetName.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 72, 100, 20);
            this.txtStreetName.RootElement.StretchVertically = true;
            this.txtStreetName.Size = new System.Drawing.Size(203, 20);
            this.txtStreetName.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(13, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(291, 19);
            this.label4.TabIndex = 0;
            this.label4.Text = "Property Search Panel from BCPA website";
            // 
            // radForBulkCase
            // 
            this.radForBulkCase.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radForBulkCase.Location = new System.Drawing.Point(404, 322);
            this.radForBulkCase.Name = "radForBulkCase";
            // 
            // 
            // 
            this.radForBulkCase.RootElement.AccessibleDescription = null;
            this.radForBulkCase.RootElement.AccessibleName = null;
            this.radForBulkCase.RootElement.ControlBounds = new System.Drawing.Rectangle(404, 322, 110, 18);
            this.radForBulkCase.RootElement.StretchHorizontally = true;
            this.radForBulkCase.RootElement.StretchVertically = true;
            this.radForBulkCase.Size = new System.Drawing.Size(73, 18);
            this.radForBulkCase.TabIndex = 36;
            this.radForBulkCase.Text = "Bulk Cases";
            // 
            // radforLPcase
            // 
            this.radforLPcase.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radforLPcase.Location = new System.Drawing.Point(248, 322);
            this.radforLPcase.Name = "radforLPcase";
            // 
            // 
            // 
            this.radforLPcase.RootElement.AccessibleDescription = null;
            this.radforLPcase.RootElement.AccessibleName = null;
            this.radforLPcase.RootElement.ControlBounds = new System.Drawing.Rectangle(248, 322, 110, 18);
            this.radforLPcase.RootElement.StretchHorizontally = true;
            this.radforLPcase.RootElement.StretchVertically = true;
            this.radforLPcase.Size = new System.Drawing.Size(63, 18);
            this.radforLPcase.TabIndex = 35;
            this.radforLPcase.Text = "LP Cases";
            // 
            // txtPropertyAddresss
            // 
            this.txtPropertyAddresss.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtPropertyAddresss.Location = new System.Drawing.Point(194, 124);
            this.txtPropertyAddresss.Name = "txtPropertyAddresss";
            // 
            // 
            // 
            this.txtPropertyAddresss.RootElement.AccessibleDescription = null;
            this.txtPropertyAddresss.RootElement.AccessibleName = null;
            this.txtPropertyAddresss.RootElement.ControlBounds = new System.Drawing.Rectangle(194, 124, 100, 20);
            this.txtPropertyAddresss.RootElement.StretchVertically = true;
            this.txtPropertyAddresss.Size = new System.Drawing.Size(203, 20);
            this.txtPropertyAddresss.TabIndex = 29;
            // 
            // PropertSearch
            // 
            this.PropertSearch.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.PropertSearch.Location = new System.Drawing.Point(201, 277);
            this.PropertSearch.Name = "PropertSearch";
            // 
            // 
            // 
            this.PropertSearch.RootElement.AccessibleDescription = null;
            this.PropertSearch.RootElement.AccessibleName = null;
            this.PropertSearch.RootElement.ControlBounds = new System.Drawing.Rectangle(201, 277, 110, 24);
            this.PropertSearch.Size = new System.Drawing.Size(175, 35);
            this.PropertSearch.TabIndex = 17;
            this.PropertSearch.Text = "Begin Manual Property Search";
            this.PropertSearch.Click += new System.EventHandler(this.PropertSearch_Click_1);
            // 
            // radbtnFolioNumber
            // 
            this.radbtnFolioNumber.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radbtnFolioNumber.Location = new System.Drawing.Point(135, 27);
            this.radbtnFolioNumber.Name = "radbtnFolioNumber";
            // 
            // 
            // 
            this.radbtnFolioNumber.RootElement.AccessibleDescription = null;
            this.radbtnFolioNumber.RootElement.AccessibleName = null;
            this.radbtnFolioNumber.RootElement.ControlBounds = new System.Drawing.Rectangle(135, 27, 110, 18);
            this.radbtnFolioNumber.RootElement.StretchHorizontally = true;
            this.radbtnFolioNumber.RootElement.StretchVertically = true;
            this.radbtnFolioNumber.Size = new System.Drawing.Size(89, 18);
            this.radbtnFolioNumber.TabIndex = 27;
            this.radbtnFolioNumber.Text = "Folio Number";
            this.radbtnFolioNumber.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radbtnIndividualAddress_ToggleStateChanged);
            // 
            // radbtnCompleteAddress
            // 
            this.radbtnCompleteAddress.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radbtnCompleteAddress.Location = new System.Drawing.Point(238, 27);
            this.radbtnCompleteAddress.Name = "radbtnCompleteAddress";
            // 
            // 
            // 
            this.radbtnCompleteAddress.RootElement.AccessibleDescription = null;
            this.radbtnCompleteAddress.RootElement.AccessibleName = null;
            this.radbtnCompleteAddress.RootElement.ControlBounds = new System.Drawing.Rectangle(238, 27, 110, 18);
            this.radbtnCompleteAddress.RootElement.StretchHorizontally = true;
            this.radbtnCompleteAddress.RootElement.StretchVertically = true;
            this.radbtnCompleteAddress.Size = new System.Drawing.Size(112, 18);
            this.radbtnCompleteAddress.TabIndex = 26;
            this.radbtnCompleteAddress.Text = "Complete Address";
            this.radbtnCompleteAddress.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radbtnCompleteAddress_ToggleStateChanged);
            // 
            // radbtnIndividualsearchBroward
            // 
            this.radbtnIndividualsearchBroward.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radbtnIndividualsearchBroward.Location = new System.Drawing.Point(238, 27);
            this.radbtnIndividualsearchBroward.Name = "radbtnIndividualsearchBroward";
            // 
            // 
            // 
            this.radbtnIndividualsearchBroward.RootElement.AccessibleDescription = null;
            this.radbtnIndividualsearchBroward.RootElement.AccessibleName = null;
            this.radbtnIndividualsearchBroward.RootElement.ControlBounds = new System.Drawing.Rectangle(238, 27, 110, 18);
            this.radbtnIndividualsearchBroward.RootElement.StretchHorizontally = true;
            this.radbtnIndividualsearchBroward.RootElement.StretchVertically = true;
            this.radbtnIndividualsearchBroward.Size = new System.Drawing.Size(112, 18);
            this.radbtnIndividualsearchBroward.TabIndex = 26;
            this.radbtnIndividualsearchBroward.Text = "Individual Search";
            this.radbtnIndividualsearchBroward.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radbtnIndividualsearchBroward_ToggleStateChanged);
            // 
            // GetLpCaseForBroward
            // 
            this.GetLpCaseForBroward.Location = new System.Drawing.Point(427, 299);
            this.GetLpCaseForBroward.Name = "GetLpCaseForBroward";
            this.GetLpCaseForBroward.Size = new System.Drawing.Size(191, 35);
            this.GetLpCaseForBroward.TabIndex = 40;
            this.GetLpCaseForBroward.Text = "1. Download PDFs";
            this.GetLpCaseForBroward.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.GetLpCaseForBroward.UseVisualStyleBackColor = true;
            this.GetLpCaseForBroward.Visible = false;
            this.GetLpCaseForBroward.Click += new System.EventHandler(this.GetLpCaseForBroward_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(37, 318);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 43;
            this.label14.Text = "label14";
            this.label14.Visible = false;
            // 
            // PartyPanel
            // 
            this.PartyPanel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.PartyPanel.Controls.Add(this.lblLastName);
            this.PartyPanel.Controls.Add(this.lblMiddleName);
            this.PartyPanel.Controls.Add(this.txtLastName);
            this.PartyPanel.Controls.Add(this.txtMiddleName);
            this.PartyPanel.Controls.Add(this.PartySearch);
            this.PartyPanel.Controls.Add(this.txtPartyDateTo);
            this.PartyPanel.Controls.Add(this.txtPartyDateFrom);
            this.PartyPanel.Controls.Add(this.txtFirstName);
            this.PartyPanel.Controls.Add(this.radDropDownParty);
            this.PartyPanel.Controls.Add(this.lblPartNameTitle);
            this.PartyPanel.Controls.Add(this.lblPartDateTo);
            this.PartyPanel.Controls.Add(this.lblPartCourtType);
            this.PartyPanel.Controls.Add(this.lblFirstName);
            this.PartyPanel.Controls.Add(this.lblPartDateFrom);
            this.PartyPanel.Location = new System.Drawing.Point(160, 34);
            this.PartyPanel.Name = "PartyPanel";
            // 
            // 
            // 
            this.PartyPanel.RootElement.AccessibleDescription = null;
            this.PartyPanel.RootElement.AccessibleName = null;
            this.PartyPanel.RootElement.ControlBounds = new System.Drawing.Rectangle(160, 34, 200, 100);
            this.PartyPanel.Size = new System.Drawing.Size(614, 56);
            this.PartyPanel.TabIndex = 4;
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(34, 115);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(59, 13);
            this.lblLastName.TabIndex = 18;
            this.lblLastName.Text = "Last Name";
            // 
            // lblMiddleName
            // 
            this.lblMiddleName.AutoSize = true;
            this.lblMiddleName.Location = new System.Drawing.Point(34, 156);
            this.lblMiddleName.Name = "lblMiddleName";
            this.lblMiddleName.Size = new System.Drawing.Size(75, 13);
            this.lblMiddleName.TabIndex = 17;
            this.lblMiddleName.Text = "Middle Name";
            // 
            // txtLastName
            // 
            this.txtLastName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtLastName.Location = new System.Drawing.Point(187, 108);
            this.txtLastName.Name = "txtLastName";
            // 
            // 
            // 
            this.txtLastName.RootElement.AccessibleDescription = null;
            this.txtLastName.RootElement.AccessibleName = null;
            this.txtLastName.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 108, 100, 20);
            this.txtLastName.RootElement.StretchVertically = true;
            this.txtLastName.Size = new System.Drawing.Size(203, 20);
            this.txtLastName.TabIndex = 16;
            // 
            // txtMiddleName
            // 
            this.txtMiddleName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtMiddleName.Location = new System.Drawing.Point(187, 149);
            this.txtMiddleName.Name = "txtMiddleName";
            // 
            // 
            // 
            this.txtMiddleName.RootElement.AccessibleDescription = null;
            this.txtMiddleName.RootElement.AccessibleName = null;
            this.txtMiddleName.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 149, 100, 20);
            this.txtMiddleName.RootElement.StretchVertically = true;
            this.txtMiddleName.Size = new System.Drawing.Size(203, 20);
            this.txtMiddleName.TabIndex = 15;
            // 
            // PartySearch
            // 
            this.PartySearch.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.PartySearch.Location = new System.Drawing.Point(187, 280);
            this.PartySearch.Name = "PartySearch";
            // 
            // 
            // 
            this.PartySearch.RootElement.AccessibleDescription = null;
            this.PartySearch.RootElement.AccessibleName = null;
            this.PartySearch.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 280, 110, 24);
            this.PartySearch.Size = new System.Drawing.Size(142, 35);
            this.PartySearch.TabIndex = 14;
            this.PartySearch.Text = "Begin Search";
            this.PartySearch.Click += new System.EventHandler(this.PartySearch_Click);
            // 
            // txtPartyDateTo
            // 
            this.txtPartyDateTo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtPartyDateTo.Location = new System.Drawing.Point(187, 236);
            this.txtPartyDateTo.Name = "txtPartyDateTo";
            // 
            // 
            // 
            this.txtPartyDateTo.RootElement.AccessibleDescription = null;
            this.txtPartyDateTo.RootElement.AccessibleName = null;
            this.txtPartyDateTo.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 236, 164, 20);
            this.txtPartyDateTo.RootElement.StretchVertically = true;
            this.txtPartyDateTo.Size = new System.Drawing.Size(203, 20);
            this.txtPartyDateTo.TabIndex = 13;
            this.txtPartyDateTo.TabStop = false;
            this.txtPartyDateTo.Text = "Wednesday, August 10, 2016";
            this.txtPartyDateTo.Value = new System.DateTime(2016, 8, 10, 16, 6, 4, 785);
            // 
            // txtPartyDateFrom
            // 
            this.txtPartyDateFrom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtPartyDateFrom.Location = new System.Drawing.Point(187, 187);
            this.txtPartyDateFrom.Name = "txtPartyDateFrom";
            // 
            // 
            // 
            this.txtPartyDateFrom.RootElement.AccessibleDescription = null;
            this.txtPartyDateFrom.RootElement.AccessibleName = null;
            this.txtPartyDateFrom.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 187, 164, 20);
            this.txtPartyDateFrom.RootElement.StretchVertically = true;
            this.txtPartyDateFrom.Size = new System.Drawing.Size(203, 20);
            this.txtPartyDateFrom.TabIndex = 12;
            this.txtPartyDateFrom.TabStop = false;
            this.txtPartyDateFrom.Text = "Wednesday, August 10, 2016";
            this.txtPartyDateFrom.Value = new System.DateTime(2016, 8, 10, 16, 1, 31, 0);
            // 
            // txtFirstName
            // 
            this.txtFirstName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtFirstName.Location = new System.Drawing.Point(187, 73);
            this.txtFirstName.Name = "txtFirstName";
            // 
            // 
            // 
            this.txtFirstName.RootElement.AccessibleDescription = null;
            this.txtFirstName.RootElement.AccessibleName = null;
            this.txtFirstName.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 73, 100, 20);
            this.txtFirstName.RootElement.StretchVertically = true;
            this.txtFirstName.Size = new System.Drawing.Size(203, 20);
            this.txtFirstName.TabIndex = 11;
            // 
            // radDropDownParty
            // 
            this.radDropDownParty.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radDropDownParty.Location = new System.Drawing.Point(187, 40);
            this.radDropDownParty.Name = "radDropDownParty";
            // 
            // 
            // 
            this.radDropDownParty.RootElement.AccessibleDescription = null;
            this.radDropDownParty.RootElement.AccessibleName = null;
            this.radDropDownParty.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 40, 125, 20);
            this.radDropDownParty.RootElement.StretchVertically = true;
            this.radDropDownParty.Size = new System.Drawing.Size(203, 20);
            this.radDropDownParty.TabIndex = 10;
            // 
            // lblPartNameTitle
            // 
            this.lblPartNameTitle.AutoSize = true;
            this.lblPartNameTitle.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblPartNameTitle.Location = new System.Drawing.Point(11, 9);
            this.lblPartNameTitle.Name = "lblPartNameTitle";
            this.lblPartNameTitle.Size = new System.Drawing.Size(138, 19);
            this.lblPartNameTitle.TabIndex = 0;
            this.lblPartNameTitle.Text = "Party Name Search";
            // 
            // lblPartDateTo
            // 
            this.lblPartDateTo.AutoSize = true;
            this.lblPartDateTo.Location = new System.Drawing.Point(35, 243);
            this.lblPartDateTo.Name = "lblPartDateTo";
            this.lblPartDateTo.Size = new System.Drawing.Size(45, 13);
            this.lblPartDateTo.TabIndex = 9;
            this.lblPartDateTo.Text = "Date To";
            // 
            // lblPartCourtType
            // 
            this.lblPartCourtType.AutoSize = true;
            this.lblPartCourtType.Location = new System.Drawing.Point(35, 47);
            this.lblPartCourtType.Name = "lblPartCourtType";
            this.lblPartCourtType.Size = new System.Drawing.Size(61, 13);
            this.lblPartCourtType.TabIndex = 1;
            this.lblPartCourtType.Text = "Court Type";
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(34, 73);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(61, 13);
            this.lblFirstName.TabIndex = 5;
            this.lblFirstName.Text = "First Name";
            // 
            // lblPartDateFrom
            // 
            this.lblPartDateFrom.AutoSize = true;
            this.lblPartDateFrom.Location = new System.Drawing.Point(35, 194);
            this.lblPartDateFrom.Name = "lblPartDateFrom";
            this.lblPartDateFrom.Size = new System.Drawing.Size(60, 13);
            this.lblPartDateFrom.TabIndex = 6;
            this.lblPartDateFrom.Text = "Date From";
            // 
            // dailySearchPanel
            // 
            this.dailySearchPanel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dailySearchPanel.Controls.Add(this.linkBtnSpokioDetails);
            this.dailySearchPanel.Controls.Add(this.ClearCacheDefandent);
            this.dailySearchPanel.Controls.Add(this.label34);
            this.dailySearchPanel.Controls.Add(this.linkLabel2);
            this.dailySearchPanel.Controls.Add(this.label30);
            this.dailySearchPanel.Controls.Add(this.label29);
            this.dailySearchPanel.Controls.Add(this.label28);
            this.dailySearchPanel.Controls.Add(this.GetDetailsFromBCPA);
            this.dailySearchPanel.Controls.Add(this.label27);
            this.dailySearchPanel.Controls.Add(this.linkLabel1);
            this.dailySearchPanel.Controls.Add(this.label26);
            this.dailySearchPanel.Controls.Add(this.label19);
            this.dailySearchPanel.Controls.Add(this.btnSaveAddress);
            this.dailySearchPanel.Controls.Add(this.label18);
            this.dailySearchPanel.Controls.Add(this.GetLpCaseForBroward);
            this.dailySearchPanel.Controls.Add(this.GettAddress);
            this.dailySearchPanel.Controls.Add(this.label16);
            this.dailySearchPanel.Controls.Add(this.label14);
            this.dailySearchPanel.Controls.Add(this.label13);
            this.dailySearchPanel.Controls.Add(this.label12);
            this.dailySearchPanel.Controls.Add(this.label1);
            this.dailySearchPanel.Controls.Add(this.DDSearchDay);
            this.dailySearchPanel.Controls.Add(this.radLabel1);
            this.dailySearchPanel.Controls.Add(this.DefendantSearch);
            this.dailySearchPanel.Controls.Add(this.lblError1);
            this.dailySearchPanel.Controls.Add(this.lblError);
            this.dailySearchPanel.Controls.Add(this.DDDocumentType);
            this.dailySearchPanel.Controls.Add(this.lblDocType);
            this.dailySearchPanel.Controls.Add(this.radEndDate);
            this.dailySearchPanel.Controls.Add(this.radBeginDate);
            this.dailySearchPanel.Controls.Add(this.lblEnddate);
            this.dailySearchPanel.Controls.Add(this.lblBeginDate);
            this.dailySearchPanel.Location = new System.Drawing.Point(575, 78);
            this.dailySearchPanel.Name = "dailySearchPanel";
            // 
            // 
            // 
            this.dailySearchPanel.RootElement.AccessibleDescription = null;
            this.dailySearchPanel.RootElement.AccessibleName = null;
            this.dailySearchPanel.RootElement.ControlBounds = new System.Drawing.Rectangle(575, 78, 200, 100);
            this.dailySearchPanel.Size = new System.Drawing.Size(112, 100);
            this.dailySearchPanel.TabIndex = 9;
            this.dailySearchPanel.Visible = false;
            // 
            // linkBtnSpokioDetails
            // 
            this.linkBtnSpokioDetails.AutoSize = true;
            this.linkBtnSpokioDetails.Location = new System.Drawing.Point(540, 227);
            this.linkBtnSpokioDetails.Name = "linkBtnSpokioDetails";
            this.linkBtnSpokioDetails.Size = new System.Drawing.Size(110, 13);
            this.linkBtnSpokioDetails.TabIndex = 60;
            this.linkBtnSpokioDetails.TabStop = true;
            this.linkBtnSpokioDetails.Text = "User Spokeo Details";
            this.linkBtnSpokioDetails.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkBtnSpokioDetails_LinkClicked);
            // 
            // ClearCacheDefandent
            // 
            this.ClearCacheDefandent.AutoSize = true;
            this.ClearCacheDefandent.Location = new System.Drawing.Point(424, 225);
            this.ClearCacheDefandent.Name = "ClearCacheDefandent";
            this.ClearCacheDefandent.Size = new System.Drawing.Size(67, 13);
            this.ClearCacheDefandent.TabIndex = 59;
            this.ClearCacheDefandent.TabStop = true;
            this.ClearCacheDefandent.Text = "Clear Cache";
            this.ClearCacheDefandent.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ClearCacheDefandent_LinkClicked);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(257, 244);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(44, 13);
            this.label34.TabIndex = 57;
            this.label34.Text = "label34";
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(424, 330);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(69, 13);
            this.linkLabel2.TabIndex = 56;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "2. Click here";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(37, 330);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(208, 13);
            this.label30.TabIndex = 55;
            this.label30.Text = "Step-2  Go to web for property analysis";
            this.label30.Click += new System.EventHandler(this.label30_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(37, 304);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(211, 13);
            this.label29.TabIndex = 54;
            this.label29.Text = "Step-1  Get details for pending Lp cases";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(37, 408);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(44, 13);
            this.label28.TabIndex = 53;
            this.label28.Text = "label28";
            this.label28.Visible = false;
            // 
            // GetDetailsFromBCPA
            // 
            this.GetDetailsFromBCPA.Location = new System.Drawing.Point(427, 290);
            this.GetDetailsFromBCPA.Name = "GetDetailsFromBCPA";
            this.GetDetailsFromBCPA.Size = new System.Drawing.Size(191, 34);
            this.GetDetailsFromBCPA.TabIndex = 52;
            this.GetDetailsFromBCPA.Text = "1. Get Details From BCPA";
            this.GetDetailsFromBCPA.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.GetDetailsFromBCPA.UseVisualStyleBackColor = true;
            this.GetDetailsFromBCPA.Click += new System.EventHandler(this.GetDetailsFromBCPA_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(39, 266);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(570, 13);
            this.label27.TabIndex = 51;
            this.label27.Text = "--------------------------------------  Find Address from Browardclerk.org ------" +
    "------------------------------------------------";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(257, 225);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(95, 13);
            this.linkLabel1.TabIndex = 50;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Refresh Numbers";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked_1);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(37, 247);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(44, 13);
            this.label26.TabIndex = 49;
            this.label26.Text = "label26";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(37, 382);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(310, 13);
            this.label19.TabIndex = 47;
            this.label19.Text = "Step-3  Choose correct Property address  Assign Addresses\'";
            this.label19.Visible = false;
            // 
            // btnSaveAddress
            // 
            this.btnSaveAddress.Location = new System.Drawing.Point(427, 383);
            this.btnSaveAddress.Name = "btnSaveAddress";
            this.btnSaveAddress.Size = new System.Drawing.Size(191, 34);
            this.btnSaveAddress.TabIndex = 46;
            this.btnSaveAddress.Text = "3. Assign addresses to properties";
            this.btnSaveAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveAddress.UseVisualStyleBackColor = true;
            this.btnSaveAddress.Visible = false;
            this.btnSaveAddress.Click += new System.EventHandler(this.btnSaveAddress_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(37, 286);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(20, 13);
            this.label18.TabIndex = 45;
            this.label18.Text = "lbl";
            this.label18.Visible = false;
            // 
            // GettAddress
            // 
            this.GettAddress.Location = new System.Drawing.Point(427, 341);
            this.GettAddress.Name = "GettAddress";
            this.GettAddress.Size = new System.Drawing.Size(191, 36);
            this.GettAddress.TabIndex = 1;
            this.GettAddress.Text = "2. Read addresses from PDFs";
            this.GettAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.GettAddress.UseVisualStyleBackColor = true;
            this.GettAddress.Visible = false;
            this.GettAddress.Click += new System.EventHandler(this.GettAddress_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(37, 347);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(20, 13);
            this.label16.TabIndex = 44;
            this.label16.Text = "lbl";
            this.label16.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(37, 303);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 13);
            this.label13.TabIndex = 42;
            this.label13.Text = "lbl";
            this.label13.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(37, 225);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 41;
            this.label12.Text = "label12";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(11, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(378, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Daily Defendant Search (Auto search starts at 9:30 AM.)";
            // 
            // DDSearchDay
            // 
            this.DDSearchDay.BackColor = System.Drawing.SystemColors.ControlLightLight;
            radListDataItem13.Text = "Business Name";
            radListDataItem13.TextWrap = true;
            radListDataItem14.Text = "party Name";
            radListDataItem14.TextWrap = true;
            radListDataItem15.Text = "Case Number";
            radListDataItem15.TextWrap = true;
            radListDataItem16.Text = "Citation Number";
            radListDataItem16.TextWrap = true;
            this.DDSearchDay.Items.Add(radListDataItem13);
            this.DDSearchDay.Items.Add(radListDataItem14);
            this.DDSearchDay.Items.Add(radListDataItem15);
            this.DDSearchDay.Items.Add(radListDataItem16);
            this.DDSearchDay.Location = new System.Drawing.Point(156, 38);
            this.DDSearchDay.Name = "DDSearchDay";
            // 
            // 
            // 
            this.DDSearchDay.RootElement.AccessibleDescription = null;
            this.DDSearchDay.RootElement.AccessibleName = null;
            this.DDSearchDay.RootElement.ControlBounds = new System.Drawing.Rectangle(156, 38, 125, 20);
            this.DDSearchDay.RootElement.StretchVertically = true;
            this.DDSearchDay.Size = new System.Drawing.Size(208, 20);
            this.DDSearchDay.TabIndex = 58;
            this.DDSearchDay.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.DDSearchDay_SelectedIndexChanged);
            // 
            // radLabel1
            // 
            this.radLabel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.radLabel1.Location = new System.Drawing.Point(46, 36);
            this.radLabel1.Name = "radLabel1";
            // 
            // 
            // 
            this.radLabel1.RootElement.AccessibleDescription = null;
            this.radLabel1.RootElement.AccessibleName = null;
            this.radLabel1.RootElement.ControlBounds = new System.Drawing.Rectangle(46, 36, 100, 18);
            this.radLabel1.Size = new System.Drawing.Size(89, 19);
            this.radLabel1.TabIndex = 39;
            this.radLabel1.Text = "Search by Day";
            // 
            // DefendantSearch
            // 
            this.DefendantSearch.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.DefendantSearch.Location = new System.Drawing.Point(156, 178);
            this.DefendantSearch.Name = "DefendantSearch";
            // 
            // 
            // 
            this.DefendantSearch.RootElement.AccessibleDescription = null;
            this.DefendantSearch.RootElement.AccessibleName = null;
            this.DefendantSearch.RootElement.ControlBounds = new System.Drawing.Rectangle(156, 178, 110, 24);
            this.DefendantSearch.Size = new System.Drawing.Size(142, 35);
            this.DefendantSearch.TabIndex = 17;
            this.DefendantSearch.Text = "Begin Manual Search";
            this.DefendantSearch.Click += new System.EventHandler(this.DefendantSearch_Click);
            // 
            // lblError1
            // 
            this.lblError1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblError1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblError1.ForeColor = System.Drawing.Color.Red;
            this.lblError1.Location = new System.Drawing.Point(370, 70);
            this.lblError1.Name = "lblError1";
            // 
            // 
            // 
            this.lblError1.RootElement.AccessibleDescription = null;
            this.lblError1.RootElement.AccessibleName = null;
            this.lblError1.RootElement.ControlBounds = new System.Drawing.Rectangle(370, 70, 100, 18);
            this.lblError1.Size = new System.Drawing.Size(13, 21);
            this.lblError1.TabIndex = 38;
            this.lblError1.Text = "*";
            // 
            // lblError
            // 
            this.lblError.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblError.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblError.Location = new System.Drawing.Point(107, 221);
            this.lblError.Name = "lblError";
            // 
            // 
            // 
            this.lblError.RootElement.AccessibleDescription = null;
            this.lblError.RootElement.AccessibleName = null;
            this.lblError.RootElement.ControlBounds = new System.Drawing.Rectangle(107, 221, 100, 18);
            this.lblError.Size = new System.Drawing.Size(2, 2);
            this.lblError.TabIndex = 37;
            // 
            // DDDocumentType
            // 
            this.DDDocumentType.BackColor = System.Drawing.SystemColors.ControlLightLight;
            radListDataItem17.Text = "Business Name";
            radListDataItem17.TextWrap = true;
            radListDataItem18.Text = "party Name";
            radListDataItem18.TextWrap = true;
            radListDataItem19.Text = "Case Number";
            radListDataItem19.TextWrap = true;
            radListDataItem20.Text = "Citation Number";
            radListDataItem20.TextWrap = true;
            this.DDDocumentType.Items.Add(radListDataItem17);
            this.DDDocumentType.Items.Add(radListDataItem18);
            this.DDDocumentType.Items.Add(radListDataItem19);
            this.DDDocumentType.Items.Add(radListDataItem20);
            this.DDDocumentType.Location = new System.Drawing.Point(156, 71);
            this.DDDocumentType.Name = "DDDocumentType";
            // 
            // 
            // 
            this.DDDocumentType.RootElement.AccessibleDescription = null;
            this.DDDocumentType.RootElement.AccessibleName = null;
            this.DDDocumentType.RootElement.ControlBounds = new System.Drawing.Rectangle(156, 71, 125, 20);
            this.DDDocumentType.RootElement.StretchVertically = true;
            this.DDDocumentType.Size = new System.Drawing.Size(208, 20);
            this.DDDocumentType.TabIndex = 36;
            this.DDDocumentType.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.DDDocumentType_SelectedIndexChanged);
            // 
            // lblDocType
            // 
            this.lblDocType.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblDocType.Controls.Add(this.PropertysearchPanel);
            this.lblDocType.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblDocType.Location = new System.Drawing.Point(46, 73);
            this.lblDocType.Name = "lblDocType";
            // 
            // 
            // 
            this.lblDocType.RootElement.AccessibleDescription = null;
            this.lblDocType.RootElement.AccessibleName = null;
            this.lblDocType.RootElement.ControlBounds = new System.Drawing.Rectangle(46, 73, 100, 18);
            this.lblDocType.Size = new System.Drawing.Size(93, 19);
            this.lblDocType.TabIndex = 35;
            this.lblDocType.Text = "Document Type";
            // 
            // radEndDate
            // 
            this.radEndDate.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.radEndDate.Location = new System.Drawing.Point(156, 142);
            this.radEndDate.Name = "radEndDate";
            // 
            // 
            // 
            this.radEndDate.RootElement.AccessibleDescription = null;
            this.radEndDate.RootElement.AccessibleName = null;
            this.radEndDate.RootElement.ControlBounds = new System.Drawing.Rectangle(156, 142, 164, 20);
            this.radEndDate.RootElement.StretchVertically = true;
            this.radEndDate.Size = new System.Drawing.Size(205, 20);
            this.radEndDate.TabIndex = 34;
            this.radEndDate.TabStop = false;
            this.radEndDate.Text = "28-Dec-16";
            this.radEndDate.Value = new System.DateTime(2016, 12, 28, 0, 0, 0, 0);
            // 
            // radBeginDate
            // 
            this.radBeginDate.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radBeginDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.radBeginDate.Location = new System.Drawing.Point(156, 105);
            this.radBeginDate.Name = "radBeginDate";
            // 
            // 
            // 
            this.radBeginDate.RootElement.AccessibleDescription = null;
            this.radBeginDate.RootElement.AccessibleName = null;
            this.radBeginDate.RootElement.ControlBounds = new System.Drawing.Rectangle(156, 105, 164, 20);
            this.radBeginDate.RootElement.StretchVertically = true;
            this.radBeginDate.Size = new System.Drawing.Size(208, 20);
            this.radBeginDate.TabIndex = 33;
            this.radBeginDate.TabStop = false;
            this.radBeginDate.Text = "21-Dec-16";
            this.radBeginDate.Value = new System.DateTime(2016, 12, 21, 0, 0, 0, 0);
            // 
            // lblEnddate
            // 
            this.lblEnddate.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblEnddate.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblEnddate.Location = new System.Drawing.Point(46, 142);
            this.lblEnddate.Name = "lblEnddate";
            // 
            // 
            // 
            this.lblEnddate.RootElement.AccessibleDescription = null;
            this.lblEnddate.RootElement.AccessibleName = null;
            this.lblEnddate.RootElement.ControlBounds = new System.Drawing.Rectangle(46, 142, 100, 18);
            this.lblEnddate.Size = new System.Drawing.Size(56, 19);
            this.lblEnddate.TabIndex = 30;
            this.lblEnddate.Text = "End Date";
            // 
            // lblBeginDate
            // 
            this.lblBeginDate.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblBeginDate.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblBeginDate.Location = new System.Drawing.Point(46, 106);
            this.lblBeginDate.Name = "lblBeginDate";
            // 
            // 
            // 
            this.lblBeginDate.RootElement.AccessibleDescription = null;
            this.lblBeginDate.RootElement.AccessibleName = null;
            this.lblBeginDate.RootElement.ControlBounds = new System.Drawing.Rectangle(46, 106, 100, 18);
            this.lblBeginDate.Size = new System.Drawing.Size(65, 19);
            this.lblBeginDate.TabIndex = 28;
            this.lblBeginDate.Text = "Begin date";
            // 
            // lblcasenumber
            // 
            this.lblcasenumber.AutoSize = true;
            this.lblcasenumber.Location = new System.Drawing.Point(65, 116);
            this.lblcasenumber.Name = "lblcasenumber";
            this.lblcasenumber.Size = new System.Drawing.Size(71, 13);
            this.lblcasenumber.TabIndex = 5;
            this.lblcasenumber.Text = "Case Number";
            // 
            // lblcaseNumberTitle
            // 
            this.lblcaseNumberTitle.AutoSize = true;
            this.lblcaseNumberTitle.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.lblcaseNumberTitle.Location = new System.Drawing.Point(11, 9);
            this.lblcaseNumberTitle.Name = "lblcaseNumberTitle";
            this.lblcaseNumberTitle.Size = new System.Drawing.Size(148, 19);
            this.lblcaseNumberTitle.TabIndex = 0;
            this.lblcaseNumberTitle.Text = "Case Number Search";
            // 
            // CasePanel
            // 
            this.CasePanel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.CasePanel.Controls.Add(this.label33);
            this.CasePanel.Controls.Add(this.label32);
            this.CasePanel.Controls.Add(this.label31);
            this.CasePanel.Controls.Add(this.GetDetailsForBulk);
            this.CasePanel.Controls.Add(this.label23);
            this.CasePanel.Controls.Add(this.label22);
            this.CasePanel.Controls.Add(this.btnAssignAddressForBulk);
            this.CasePanel.Controls.Add(this.lblcasenumber);
            this.CasePanel.Controls.Add(this.radPanelBulk);
            this.CasePanel.Controls.Add(this.label20);
            this.CasePanel.Controls.Add(this.lblcaseNumberTitle);
            this.CasePanel.Controls.Add(this.btnCaseSearch);
            this.CasePanel.Controls.Add(this.txtCaseNumber);
            this.CasePanel.Controls.Add(this.radRadioBulk);
            this.CasePanel.Controls.Add(this.radRadioSingle);
            this.CasePanel.Location = new System.Drawing.Point(200, 70);
            this.CasePanel.Name = "CasePanel";
            // 
            // 
            // 
            this.CasePanel.RootElement.AccessibleDescription = null;
            this.CasePanel.RootElement.AccessibleName = null;
            this.CasePanel.RootElement.ControlBounds = new System.Drawing.Rectangle(200, 70, 200, 100);
            this.CasePanel.Size = new System.Drawing.Size(608, 46);
            this.CasePanel.TabIndex = 5;
            this.CasePanel.Visible = false;
            this.CasePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.CasePanel_Paint);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(38, 393);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(258, 13);
            this.label33.TabIndex = 53;
            this.label33.Text = "Step-2  Get details from BCPA for pending bulk cases";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(43, 281);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(475, 13);
            this.label32.TabIndex = 52;
            this.label32.Text = "--------------------------------------------------------- Find BCPA details  ----" +
    "----------------------------------------------------------------";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(38, 371);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 13);
            this.label31.TabIndex = 51;
            this.label31.Text = "label31";
            // 
            // GetDetailsForBulk
            // 
            this.GetDetailsForBulk.Location = new System.Drawing.Point(446, 380);
            this.GetDetailsForBulk.Name = "GetDetailsForBulk";
            this.GetDetailsForBulk.Size = new System.Drawing.Size(204, 38);
            this.GetDetailsForBulk.TabIndex = 50;
            this.GetDetailsForBulk.Text = "Get Details From BCPA";
            this.GetDetailsForBulk.UseVisualStyleBackColor = true;
            this.GetDetailsForBulk.Click += new System.EventHandler(this.GetDetailsForBulk_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(38, 307);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 13);
            this.label23.TabIndex = 49;
            this.label23.Text = "label23";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(38, 345);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(383, 13);
            this.label22.TabIndex = 48;
            this.label22.Text = "Step-1  Choose correct Property address by \'Assign address to properties\'";
            // 
            // btnAssignAddressForBulk
            // 
            this.btnAssignAddressForBulk.Location = new System.Drawing.Point(446, 334);
            this.btnAssignAddressForBulk.Name = "btnAssignAddressForBulk";
            this.btnAssignAddressForBulk.Size = new System.Drawing.Size(204, 38);
            this.btnAssignAddressForBulk.TabIndex = 24;
            this.btnAssignAddressForBulk.Text = "Assign address to properties";
            this.btnAssignAddressForBulk.UseVisualStyleBackColor = true;
            this.btnAssignAddressForBulk.Click += new System.EventHandler(this.btnAssignAddressForBulk_Click);
            // 
            // radPanelBulk
            // 
            this.radPanelBulk.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radPanelBulk.Controls.Add(this.btnMannualBulk);
            this.radPanelBulk.Controls.Add(this.txtBulkCase);
            this.radPanelBulk.Controls.Add(this.label21);
            this.radPanelBulk.Location = new System.Drawing.Point(19, 64);
            this.radPanelBulk.Name = "radPanelBulk";
            // 
            // 
            // 
            this.radPanelBulk.RootElement.AccessibleDescription = null;
            this.radPanelBulk.RootElement.AccessibleName = null;
            this.radPanelBulk.RootElement.ControlBounds = new System.Drawing.Rectangle(19, 64, 200, 100);
            this.radPanelBulk.Size = new System.Drawing.Size(518, 198);
            this.radPanelBulk.TabIndex = 23;
            // 
            // btnMannualBulk
            // 
            this.btnMannualBulk.Location = new System.Drawing.Point(148, 155);
            this.btnMannualBulk.Name = "btnMannualBulk";
            this.btnMannualBulk.Size = new System.Drawing.Size(145, 30);
            this.btnMannualBulk.TabIndex = 2;
            this.btnMannualBulk.Text = "Mannual Bulk Search";
            this.btnMannualBulk.UseVisualStyleBackColor = true;
            this.btnMannualBulk.Click += new System.EventHandler(this.btnMannualBulk_Click);
            // 
            // txtBulkCase
            // 
            this.txtBulkCase.Location = new System.Drawing.Point(148, 22);
            this.txtBulkCase.Multiline = true;
            this.txtBulkCase.Name = "txtBulkCase";
            this.txtBulkCase.Size = new System.Drawing.Size(343, 108);
            this.txtBulkCase.TabIndex = 1;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(18, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(101, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Paste Case Number";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(65, 40);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(62, 13);
            this.label20.TabIndex = 22;
            this.label20.Text = "Search By :";
            // 
            // btnCaseSearch
            // 
            this.btnCaseSearch.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCaseSearch.Location = new System.Drawing.Point(197, 184);
            this.btnCaseSearch.Name = "btnCaseSearch";
            // 
            // 
            // 
            this.btnCaseSearch.RootElement.AccessibleDescription = null;
            this.btnCaseSearch.RootElement.AccessibleName = null;
            this.btnCaseSearch.RootElement.ControlBounds = new System.Drawing.Rectangle(197, 184, 110, 24);
            this.btnCaseSearch.Size = new System.Drawing.Size(142, 35);
            this.btnCaseSearch.TabIndex = 18;
            this.btnCaseSearch.Text = "Begin Search";
            this.btnCaseSearch.Click += new System.EventHandler(this.btncasesearch_click);
            // 
            // txtCaseNumber
            // 
            this.txtCaseNumber.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtCaseNumber.Location = new System.Drawing.Point(197, 116);
            this.txtCaseNumber.Name = "txtCaseNumber";
            // 
            // 
            // 
            this.txtCaseNumber.RootElement.AccessibleDescription = null;
            this.txtCaseNumber.RootElement.AccessibleName = null;
            this.txtCaseNumber.RootElement.ControlBounds = new System.Drawing.Rectangle(197, 116, 100, 20);
            this.txtCaseNumber.RootElement.StretchVertically = true;
            this.txtCaseNumber.Size = new System.Drawing.Size(203, 20);
            this.txtCaseNumber.TabIndex = 16;
            // 
            // radRadioBulk
            // 
            this.radRadioBulk.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radRadioBulk.Location = new System.Drawing.Point(327, 40);
            this.radRadioBulk.Name = "radRadioBulk";
            // 
            // 
            // 
            this.radRadioBulk.RootElement.AccessibleDescription = null;
            this.radRadioBulk.RootElement.AccessibleName = null;
            this.radRadioBulk.RootElement.ControlBounds = new System.Drawing.Rectangle(327, 40, 110, 18);
            this.radRadioBulk.RootElement.StretchHorizontally = true;
            this.radRadioBulk.RootElement.StretchVertically = true;
            this.radRadioBulk.Size = new System.Drawing.Size(73, 18);
            this.radRadioBulk.TabIndex = 21;
            this.radRadioBulk.Text = "Bulk Cases";
            this.radRadioBulk.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radRadioBulk_ToggleStateChanged);
            // 
            // radRadioSingle
            // 
            this.radRadioSingle.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radRadioSingle.Location = new System.Drawing.Point(203, 40);
            this.radRadioSingle.Name = "radRadioSingle";
            // 
            // 
            // 
            this.radRadioSingle.RootElement.AccessibleDescription = null;
            this.radRadioSingle.RootElement.AccessibleName = null;
            this.radRadioSingle.RootElement.ControlBounds = new System.Drawing.Rectangle(203, 40, 110, 18);
            this.radRadioSingle.RootElement.StretchHorizontally = true;
            this.radRadioSingle.RootElement.StretchVertically = true;
            this.radRadioSingle.Size = new System.Drawing.Size(77, 18);
            this.radRadioSingle.TabIndex = 20;
            this.radRadioSingle.Text = "Single Case";
            this.radRadioSingle.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radRadioSingle_ToggleStateChanged);
            // 
            // radTextBox3
            // 
            this.radTextBox3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radTextBox3.Location = new System.Drawing.Point(187, 73);
            this.radTextBox3.Name = "radTextBox3";
            // 
            // 
            // 
            this.radTextBox3.RootElement.AccessibleDescription = null;
            this.radTextBox3.RootElement.AccessibleName = null;
            this.radTextBox3.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 73, 100, 20);
            this.radTextBox3.RootElement.StretchVertically = true;
            this.radTextBox3.Size = new System.Drawing.Size(203, 20);
            this.radTextBox3.TabIndex = 11;
            // 
            // radDateTimePicker1
            // 
            this.radDateTimePicker1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radDateTimePicker1.Location = new System.Drawing.Point(187, 236);
            this.radDateTimePicker1.Name = "radDateTimePicker1";
            // 
            // 
            // 
            this.radDateTimePicker1.RootElement.AccessibleDescription = null;
            this.radDateTimePicker1.RootElement.AccessibleName = null;
            this.radDateTimePicker1.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 236, 164, 20);
            this.radDateTimePicker1.RootElement.StretchVertically = true;
            this.radDateTimePicker1.Size = new System.Drawing.Size(203, 20);
            this.radDateTimePicker1.TabIndex = 13;
            this.radDateTimePicker1.TabStop = false;
            this.radDateTimePicker1.Text = "Wednesday, August 10, 2016";
            this.radDateTimePicker1.Value = new System.DateTime(2016, 8, 10, 16, 6, 4, 785);
            // 
            // CitationPanel
            // 
            this.CitationPanel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.CitationPanel.Controls.Add(this.lblCitationTitle);
            this.CitationPanel.Controls.Add(this.lblCitationNumber);
            this.CitationPanel.Controls.Add(this.btnCitationSearch);
            this.CitationPanel.Controls.Add(this.txtCitationNumber);
            this.CitationPanel.Location = new System.Drawing.Point(206, 130);
            this.CitationPanel.Name = "CitationPanel";
            // 
            // 
            // 
            this.CitationPanel.RootElement.AccessibleDescription = null;
            this.CitationPanel.RootElement.AccessibleName = null;
            this.CitationPanel.RootElement.ControlBounds = new System.Drawing.Rectangle(206, 130, 200, 100);
            this.CitationPanel.Size = new System.Drawing.Size(528, 33);
            this.CitationPanel.TabIndex = 6;
            this.CitationPanel.Visible = false;
            // 
            // lblCitationTitle
            // 
            this.lblCitationTitle.AutoSize = true;
            this.lblCitationTitle.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCitationTitle.Location = new System.Drawing.Point(11, 9);
            this.lblCitationTitle.Name = "lblCitationTitle";
            this.lblCitationTitle.Size = new System.Drawing.Size(155, 17);
            this.lblCitationTitle.TabIndex = 0;
            this.lblCitationTitle.Text = "Citation Number Search";
            // 
            // lblCitationNumber
            // 
            this.lblCitationNumber.AutoSize = true;
            this.lblCitationNumber.Location = new System.Drawing.Point(37, 66);
            this.lblCitationNumber.Name = "lblCitationNumber";
            this.lblCitationNumber.Size = new System.Drawing.Size(82, 13);
            this.lblCitationNumber.TabIndex = 5;
            this.lblCitationNumber.Text = "Citation Number";
            // 
            // btnCitationSearch
            // 
            this.btnCitationSearch.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCitationSearch.Location = new System.Drawing.Point(190, 126);
            this.btnCitationSearch.Name = "btnCitationSearch";
            // 
            // 
            // 
            this.btnCitationSearch.RootElement.AccessibleDescription = null;
            this.btnCitationSearch.RootElement.AccessibleName = null;
            this.btnCitationSearch.RootElement.ControlBounds = new System.Drawing.Rectangle(190, 126, 110, 24);
            this.btnCitationSearch.Size = new System.Drawing.Size(142, 35);
            this.btnCitationSearch.TabIndex = 18;
            this.btnCitationSearch.Text = "Begin Search";
            this.btnCitationSearch.Click += new System.EventHandler(this.btnCitationSearch_Click);
            // 
            // txtCitationNumber
            // 
            this.txtCitationNumber.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtCitationNumber.Location = new System.Drawing.Point(190, 66);
            this.txtCitationNumber.Name = "txtCitationNumber";
            // 
            // 
            // 
            this.txtCitationNumber.RootElement.AccessibleDescription = null;
            this.txtCitationNumber.RootElement.AccessibleName = null;
            this.txtCitationNumber.RootElement.ControlBounds = new System.Drawing.Rectangle(190, 66, 100, 20);
            this.txtCitationNumber.RootElement.StretchVertically = true;
            this.txtCitationNumber.Size = new System.Drawing.Size(203, 20);
            this.txtCitationNumber.TabIndex = 16;
            // 
            // Home
            // 
            this.Home.AccessibleDescription = "Home";
            this.Home.AccessibleName = "Home";
            this.Home.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.Home.AngleTransform = 90F;
            this.Home.AutoSize = true;
            this.Home.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.Home.BackColor = System.Drawing.SystemColors.HighlightText;
            this.Home.CanFocus = true;
            this.Home.CheckOnClick = true;
            this.Home.ClipDrawing = true;
            this.Home.DisplayStyle = Telerik.WinControls.DisplayStyle.ImageAndText;
            this.Home.FlipText = false;
            this.Home.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.Home.IsChecked = true;
            this.Home.Margin = new System.Windows.Forms.Padding(0);
            this.Home.MaxSize = new System.Drawing.Size(0, 0);
            this.Home.Name = "Home";
            this.Home.Padding = new System.Windows.Forms.Padding(8, 1, 8, 1);
            this.Home.RightToLeft = false;
            this.Home.ShouldPaint = false;
            this.Home.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
            this.Home.StretchHorizontally = true;
            this.Home.StretchVertically = true;
            this.Home.Text = "Home";
            this.Home.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.Home.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.Home.TextOrientation = System.Windows.Forms.Orientation.Vertical;
            this.Home.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.Home.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.Home.UseCompatibleTextRendering = true;
            this.Home.UseDefaultDisabledPaint = false;
            this.Home.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.Home.Click += new System.EventHandler(this.Home_Click);
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.AccessibleDescription = "Case Category";
            this.radMenuItem1.AccessibleName = "Case Category";
            this.radMenuItem1.AngleTransform = 90F;
            this.radMenuItem1.AutoSize = true;
            this.radMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("radMenuItem1.Image")));
            this.radMenuItem1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.BussinessName,
            this.PartyName,
            this.CaseNumber,
            this.CitationNumber});
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.ShouldPaint = false;
            this.radMenuItem1.ShowArrow = false;
            this.radMenuItem1.Text = "Case Category";
            this.radMenuItem1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.radMenuItem1.TextOrientation = System.Windows.Forms.Orientation.Vertical;
            this.radMenuItem1.Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            this.radMenuItem1.Click += new System.EventHandler(this.radMenuItem1_Click);
            // 
            // BussinessName
            // 
            this.BussinessName.AccessibleDescription = "Bussiness Name";
            this.BussinessName.AccessibleName = "Bussiness Name";
            this.BussinessName.Name = "BussinessName";
            this.BussinessName.Text = "Bussiness Name";
            this.BussinessName.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.BussinessName.Click += new System.EventHandler(this.radMenuItem4_Click);
            // 
            // PartyName
            // 
            this.PartyName.AccessibleDescription = "Party Name";
            this.PartyName.AccessibleName = "Party Name";
            this.PartyName.Name = "PartyName";
            this.PartyName.Text = "Party Name";
            this.PartyName.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.PartyName.Click += new System.EventHandler(this.PartyName_Click);
            // 
            // CaseNumber
            // 
            this.CaseNumber.AccessibleDescription = "Case Number";
            this.CaseNumber.AccessibleName = "Case Number";
            this.CaseNumber.Name = "CaseNumber";
            this.CaseNumber.Text = "Case Number";
            this.CaseNumber.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.CaseNumber.Click += new System.EventHandler(this.CaseNumber_Click);
            // 
            // CitationNumber
            // 
            this.CitationNumber.AccessibleDescription = "Citation Number";
            this.CitationNumber.AccessibleName = "Citation Number";
            this.CitationNumber.Name = "CitationNumber";
            this.CitationNumber.Text = "Citation Number";
            this.CitationNumber.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.CitationNumber.Click += new System.EventHandler(this.CitationNumber_Click);
            // 
            // radMenuGeneral
            // 
            this.radMenuGeneral.AccessibleDescription = "Saerch By Address";
            this.radMenuGeneral.AccessibleName = "Saerch By Address";
            this.radMenuGeneral.AngleTransform = 90F;
            this.radMenuGeneral.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radMenuGeneral.Name = "radMenuGeneral";
            this.radMenuGeneral.ShouldPaint = false;
            this.radMenuGeneral.Text = "General Search";
            this.radMenuGeneral.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.radMenuGeneral.TextOrientation = System.Windows.Forms.Orientation.Vertical;
            this.radMenuGeneral.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radMenuGeneral.Click += new System.EventHandler(this.radMenuGeneral_Click);
            Image img2 = ((System.Drawing.Image)(resources.GetObject("radMenuGeneral.Image")));
              this.radMenuGeneral.Image = RotateImageIfReq(img2);
            // 
            // radmenuBroward
            // 
            this.radmenuBroward.AccessibleDescription = "Broward Search";
            this.radmenuBroward.AccessibleName = "Broward Search";
            this.radmenuBroward.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.radmenuBroward.AngleTransform = 90F;
            this.radmenuBroward.AutoSize = true;
            this.radmenuBroward.FlipText = false;
            this.radmenuBroward.Image = ((System.Drawing.Image)(resources.GetObject("radmenuBroward.Image")));
            this.radmenuBroward.Image = RotateImageIfReq(this.radmenuBroward.Image);
            this.radmenuBroward.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radmenuBroward.IsSharedImage = false;
            this.radmenuBroward.Name = "radmenuBroward";
            this.radmenuBroward.Padding = new System.Windows.Forms.Padding(20, 1, 8, 1);
            this.radmenuBroward.ShouldPaint = false;
            this.radmenuBroward.ShowArrow = false;
            this.radmenuBroward.Text = "Broward Search";
            this.radmenuBroward.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.radmenuBroward.TextOrientation = System.Windows.Forms.Orientation.Vertical;
            this.radmenuBroward.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radmenuBroward.Click += new System.EventHandler(this.DailySearchForLP_Click);
            // 
            // lblselectCategory
            // 
            this.lblselectCategory.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblselectCategory.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblselectCategory.Location = new System.Drawing.Point(174, 44);
            this.lblselectCategory.Name = "lblselectCategory";
            // 
            // 
            // 
            this.lblselectCategory.RootElement.AccessibleDescription = null;
            this.lblselectCategory.RootElement.AccessibleName = null;
            this.lblselectCategory.RootElement.ControlBounds = new System.Drawing.Rectangle(174, 44, 100, 18);
            this.lblselectCategory.Size = new System.Drawing.Size(59, 19);
            this.lblselectCategory.TabIndex = 12;
            this.lblselectCategory.Text = "Search By";
            // 
            // PalmBeachPanel
            // 
            this.PalmBeachPanel.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.PalmBeachPanel.Controls.Add(this.lnkPalmSpokeoDetails);
            this.PalmBeachPanel.Controls.Add(this.lblpalmpending);
            this.PalmBeachPanel.Controls.Add(this.linkRefreshPalmCases);
            this.PalmBeachPanel.Controls.Add(this.radLabel9);
            this.PalmBeachPanel.Controls.Add(this.btnPCBPalmDetails);
            this.PalmBeachPanel.Controls.Add(this.radLabel8);
            this.PalmBeachPanel.Controls.Add(this.PalmSelectCorrectAddreess);
            this.PalmBeachPanel.Controls.Add(this.link90Days);
            this.PalmBeachPanel.Controls.Add(this.link30Days);
            this.PalmBeachPanel.Controls.Add(this.radLabel5);
            this.PalmBeachPanel.Controls.Add(this.link7Days);
            this.PalmBeachPanel.Controls.Add(this.label37);
            this.PalmBeachPanel.Controls.Add(this.radLabel7);
            this.PalmBeachPanel.Controls.Add(this.radLabel6);
            this.PalmBeachPanel.Controls.Add(this.radDDlDisplayRecords);
            this.PalmBeachPanel.Controls.Add(this.radDDlTotalRecord);
            this.PalmBeachPanel.Controls.Add(this.PalmBeachsrch);
            this.PalmBeachPanel.Controls.Add(this.radLabel4);
            this.PalmBeachPanel.Controls.Add(this.txtPalmDocType);
            this.PalmBeachPanel.Controls.Add(this.radLabel2);
            this.PalmBeachPanel.Controls.Add(this.radLabel3);
            this.PalmBeachPanel.Controls.Add(this.label35);
            this.PalmBeachPanel.Controls.Add(this.radPalmDateFrom);
            this.PalmBeachPanel.Controls.Add(this.radPalmDateTo);
            this.PalmBeachPanel.Location = new System.Drawing.Point(258, 12);
            this.PalmBeachPanel.Name = "PalmBeachPanel";
            // 
            // 
            // 
            this.PalmBeachPanel.RootElement.AccessibleDescription = null;
            this.PalmBeachPanel.RootElement.AccessibleName = null;
            this.PalmBeachPanel.RootElement.ControlBounds = new System.Drawing.Rectangle(258, 12, 200, 100);
            this.PalmBeachPanel.Size = new System.Drawing.Size(649, 70);
            this.PalmBeachPanel.TabIndex = 13;
            // 
            // lnkPalmSpokeoDetails
            // 
            this.lnkPalmSpokeoDetails.AutoSize = true;
            this.lnkPalmSpokeoDetails.Location = new System.Drawing.Point(421, 252);
            this.lnkPalmSpokeoDetails.Name = "lnkPalmSpokeoDetails";
            this.lnkPalmSpokeoDetails.Size = new System.Drawing.Size(110, 13);
            this.lnkPalmSpokeoDetails.TabIndex = 75;
            this.lnkPalmSpokeoDetails.TabStop = true;
            this.lnkPalmSpokeoDetails.Text = "User Spokeo Details";
            this.lnkPalmSpokeoDetails.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkPalmSpokeoDetails_LinkClicked);
            // 
            // lblpalmpending
            // 
            this.lblpalmpending.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblpalmpending.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.lblpalmpending.Location = new System.Drawing.Point(307, 329);
            this.lblpalmpending.Name = "lblpalmpending";
            // 
            // 
            // 
            this.lblpalmpending.RootElement.AccessibleDescription = null;
            this.lblpalmpending.RootElement.AccessibleName = null;
            this.lblpalmpending.RootElement.ControlBounds = new System.Drawing.Rectangle(307, 329, 100, 18);
            this.lblpalmpending.Size = new System.Drawing.Size(2, 2);
            this.lblpalmpending.TabIndex = 73;
            // 
            // linkRefreshPalmCases
            // 
            this.linkRefreshPalmCases.AutoSize = true;
            this.linkRefreshPalmCases.Location = new System.Drawing.Point(304, 252);
            this.linkRefreshPalmCases.Name = "linkRefreshPalmCases";
            this.linkRefreshPalmCases.Size = new System.Drawing.Size(78, 13);
            this.linkRefreshPalmCases.TabIndex = 74;
            this.linkRefreshPalmCases.TabStop = true;
            this.linkRefreshPalmCases.Text = "Refresh Cases";
            this.linkRefreshPalmCases.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkRefreshPalmCases_LinkClicked);
            // 
            // radLabel9
            // 
            this.radLabel9.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel9.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.radLabel9.Location = new System.Drawing.Point(34, 323);
            this.radLabel9.Name = "radLabel9";
            // 
            // 
            // 
            this.radLabel9.RootElement.AccessibleDescription = null;
            this.radLabel9.RootElement.AccessibleName = null;
            this.radLabel9.RootElement.ControlBounds = new System.Drawing.Rectangle(34, 323, 100, 18);
            this.radLabel9.Size = new System.Drawing.Size(252, 19);
            this.radLabel9.TabIndex = 72;
            this.radLabel9.Text = "Step-1:   Get PCB Details For Pending Palm :  ";
            // 
            // btnPCBPalmDetails
            // 
            this.btnPCBPalmDetails.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPCBPalmDetails.Location = new System.Drawing.Point(418, 318);
            this.btnPCBPalmDetails.Name = "btnPCBPalmDetails";
            // 
            // 
            // 
            this.btnPCBPalmDetails.RootElement.AccessibleDescription = null;
            this.btnPCBPalmDetails.RootElement.AccessibleName = null;
            this.btnPCBPalmDetails.RootElement.ControlBounds = new System.Drawing.Rectangle(418, 318, 110, 24);
            this.btnPCBPalmDetails.Size = new System.Drawing.Size(177, 35);
            this.btnPCBPalmDetails.TabIndex = 71;
            this.btnPCBPalmDetails.Text = "Get Palm Cases Details From PCB";
            this.btnPCBPalmDetails.Click += new System.EventHandler(this.btnPCBPalmDetails_Click);
            // 
            // radLabel8
            // 
            this.radLabel8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel8.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.radLabel8.Location = new System.Drawing.Point(32, 284);
            this.radLabel8.Name = "radLabel8";
            // 
            // 
            // 
            this.radLabel8.RootElement.AccessibleDescription = null;
            this.radLabel8.RootElement.AccessibleName = null;
            this.radLabel8.RootElement.ControlBounds = new System.Drawing.Rectangle(32, 284, 100, 18);
            this.radLabel8.Size = new System.Drawing.Size(248, 19);
            this.radLabel8.TabIndex = 70;
            this.radLabel8.Text = "Step-1:   Select correct palm beach address  ";
            // 
            // PalmSelectCorrectAddreess
            // 
            this.PalmSelectCorrectAddreess.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.PalmSelectCorrectAddreess.Location = new System.Drawing.Point(418, 274);
            this.PalmSelectCorrectAddreess.Name = "PalmSelectCorrectAddreess";
            // 
            // 
            // 
            this.PalmSelectCorrectAddreess.RootElement.AccessibleDescription = null;
            this.PalmSelectCorrectAddreess.RootElement.AccessibleName = null;
            this.PalmSelectCorrectAddreess.RootElement.ControlBounds = new System.Drawing.Rectangle(418, 274, 110, 24);
            this.PalmSelectCorrectAddreess.Size = new System.Drawing.Size(177, 35);
            this.PalmSelectCorrectAddreess.TabIndex = 69;
            this.PalmSelectCorrectAddreess.Text = "Select Correct Address";
            this.PalmSelectCorrectAddreess.Click += new System.EventHandler(this.PalmSelectCorrectAddreess_Click);
            // 
            // link90Days
            // 
            this.link90Days.AutoSize = true;
            this.link90Days.Location = new System.Drawing.Point(296, 143);
            this.link90Days.Name = "link90Days";
            this.link90Days.Size = new System.Drawing.Size(45, 13);
            this.link90Days.TabIndex = 68;
            this.link90Days.TabStop = true;
            this.link90Days.Text = "90 days";
            this.link90Days.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link90Days_LinkClicked);
            // 
            // link30Days
            // 
            this.link30Days.AutoSize = true;
            this.link30Days.Location = new System.Drawing.Point(235, 144);
            this.link30Days.Name = "link30Days";
            this.link30Days.Size = new System.Drawing.Size(45, 13);
            this.link30Days.TabIndex = 67;
            this.link30Days.TabStop = true;
            this.link30Days.Text = "30 days";
            this.link30Days.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link30Days_LinkClicked);
            // 
            // radLabel5
            // 
            this.radLabel5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.radLabel5.Location = new System.Drawing.Point(32, 137);
            this.radLabel5.Name = "radLabel5";
            // 
            // 
            // 
            this.radLabel5.RootElement.AccessibleDescription = null;
            this.radLabel5.RootElement.AccessibleName = null;
            this.radLabel5.RootElement.ControlBounds = new System.Drawing.Rectangle(32, 137, 100, 18);
            this.radLabel5.Size = new System.Drawing.Size(89, 19);
            this.radLabel5.TabIndex = 66;
            this.radLabel5.Text = "Search by Day";
            // 
            // link7Days
            // 
            this.link7Days.AutoSize = true;
            this.link7Days.Location = new System.Drawing.Point(176, 144);
            this.link7Days.Name = "link7Days";
            this.link7Days.Size = new System.Drawing.Size(39, 13);
            this.link7Days.TabIndex = 65;
            this.link7Days.TabStop = true;
            this.link7Days.Text = "7 days";
            this.link7Days.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.link7Days_LinkClicked);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.label37.Location = new System.Drawing.Point(441, 69);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(91, 15);
            this.label37.TabIndex = 64;
            this.label37.Text = "Search Options";
            // 
            // radLabel7
            // 
            this.radLabel7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel7.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.radLabel7.Location = new System.Drawing.Point(391, 137);
            this.radLabel7.Name = "radLabel7";
            // 
            // 
            // 
            this.radLabel7.RootElement.AccessibleDescription = null;
            this.radLabel7.RootElement.AccessibleName = null;
            this.radLabel7.RootElement.ControlBounds = new System.Drawing.Rectangle(391, 137, 100, 18);
            this.radLabel7.Size = new System.Drawing.Size(46, 19);
            this.radLabel7.TabIndex = 63;
            this.radLabel7.Text = "Display";
            // 
            // radLabel6
            // 
            this.radLabel6.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel6.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.radLabel6.Location = new System.Drawing.Point(528, 136);
            this.radLabel6.Name = "radLabel6";
            // 
            // 
            // 
            this.radLabel6.RootElement.AccessibleDescription = null;
            this.radLabel6.RootElement.AccessibleName = null;
            this.radLabel6.RootElement.ControlBounds = new System.Drawing.Rectangle(528, 136, 100, 18);
            this.radLabel6.Size = new System.Drawing.Size(102, 19);
            this.radLabel6.TabIndex = 62;
            this.radLabel6.Text = "Records Per Page";
            // 
            // radDDlDisplayRecords
            // 
            this.radDDlDisplayRecords.BackColor = System.Drawing.SystemColors.ControlLightLight;
            radListDataItem21.Text = "Business Name";
            radListDataItem21.TextWrap = true;
            radListDataItem22.Text = "party Name";
            radListDataItem22.TextWrap = true;
            radListDataItem23.Text = "Case Number";
            radListDataItem23.TextWrap = true;
            radListDataItem24.Text = "Citation Number";
            radListDataItem24.TextWrap = true;
            this.radDDlDisplayRecords.Items.Add(radListDataItem21);
            this.radDDlDisplayRecords.Items.Add(radListDataItem22);
            this.radDDlDisplayRecords.Items.Add(radListDataItem23);
            this.radDDlDisplayRecords.Items.Add(radListDataItem24);
            this.radDDlDisplayRecords.Location = new System.Drawing.Point(445, 136);
            this.radDDlDisplayRecords.Name = "radDDlDisplayRecords";
            // 
            // 
            // 
            this.radDDlDisplayRecords.RootElement.AccessibleDescription = null;
            this.radDDlDisplayRecords.RootElement.AccessibleName = null;
            this.radDDlDisplayRecords.RootElement.ControlBounds = new System.Drawing.Rectangle(445, 136, 125, 20);
            this.radDDlDisplayRecords.RootElement.StretchVertically = true;
            this.radDDlDisplayRecords.Size = new System.Drawing.Size(77, 20);
            this.radDDlDisplayRecords.TabIndex = 61;
            // 
            // radDDlTotalRecord
            // 
            this.radDDlTotalRecord.BackColor = System.Drawing.SystemColors.ControlLightLight;
            radListDataItem1.Text = "Business Name";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "party Name";
            radListDataItem2.TextWrap = true;
            radListDataItem3.Text = "Case Number";
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "Citation Number";
            radListDataItem4.TextWrap = true;
            this.radDDlTotalRecord.Items.Add(radListDataItem1);
            this.radDDlTotalRecord.Items.Add(radListDataItem2);
            this.radDDlTotalRecord.Items.Add(radListDataItem3);
            this.radDDlTotalRecord.Items.Add(radListDataItem4);
            this.radDDlTotalRecord.Location = new System.Drawing.Point(418, 104);
            this.radDDlTotalRecord.Name = "radDDlTotalRecord";
            // 
            // 
            // 
            this.radDDlTotalRecord.RootElement.AccessibleDescription = null;
            this.radDDlTotalRecord.RootElement.AccessibleName = null;
            this.radDDlTotalRecord.RootElement.ControlBounds = new System.Drawing.Rectangle(418, 104, 125, 20);
            this.radDDlTotalRecord.RootElement.StretchVertically = true;
            this.radDDlTotalRecord.Size = new System.Drawing.Size(208, 20);
            this.radDDlTotalRecord.TabIndex = 59;
            // 
            // PalmBeachsrch
            // 
            this.PalmBeachsrch.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.PalmBeachsrch.Location = new System.Drawing.Point(164, 175);
            this.PalmBeachsrch.Name = "PalmBeachsrch";
            // 
            // 
            // 
            this.PalmBeachsrch.RootElement.AccessibleDescription = null;
            this.PalmBeachsrch.RootElement.AccessibleName = null;
            this.PalmBeachsrch.RootElement.ControlBounds = new System.Drawing.Rectangle(164, 175, 110, 24);
            this.PalmBeachsrch.Size = new System.Drawing.Size(177, 35);
            this.PalmBeachsrch.TabIndex = 45;
            this.PalmBeachsrch.Text = "Begin Manual Search";
            this.PalmBeachsrch.Click += new System.EventHandler(this.PalmBeachsrch_Click);
            // 
            // radLabel4
            // 
            this.radLabel4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel4.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.radLabel4.Location = new System.Drawing.Point(32, 48);
            this.radLabel4.Name = "radLabel4";
            // 
            // 
            // 
            this.radLabel4.RootElement.AccessibleDescription = null;
            this.radLabel4.RootElement.AccessibleName = null;
            this.radLabel4.RootElement.ControlBounds = new System.Drawing.Rectangle(32, 48, 100, 18);
            this.radLabel4.Size = new System.Drawing.Size(93, 19);
            this.radLabel4.TabIndex = 44;
            this.radLabel4.Text = "Document Type";
            // 
            // txtPalmDocType
            // 
            this.txtPalmDocType.Location = new System.Drawing.Point(164, 45);
            this.txtPalmDocType.Name = "txtPalmDocType";
            this.txtPalmDocType.Size = new System.Drawing.Size(208, 20);
            this.txtPalmDocType.TabIndex = 43;
            // 
            // radLabel2
            // 
            this.radLabel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.radLabel2.Location = new System.Drawing.Point(32, 72);
            this.radLabel2.Name = "radLabel2";
            // 
            // 
            // 
            this.radLabel2.RootElement.AccessibleDescription = null;
            this.radLabel2.RootElement.AccessibleName = null;
            this.radLabel2.RootElement.ControlBounds = new System.Drawing.Rectangle(32, 72, 100, 18);
            this.radLabel2.Size = new System.Drawing.Size(102, 19);
            this.radLabel2.TabIndex = 42;
            this.radLabel2.Text = "Date Range From";
            // 
            // radLabel3
            // 
            this.radLabel3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.radLabel3.Location = new System.Drawing.Point(32, 101);
            this.radLabel3.Name = "radLabel3";
            // 
            // 
            // 
            this.radLabel3.RootElement.AccessibleDescription = null;
            this.radLabel3.RootElement.AccessibleName = null;
            this.radLabel3.RootElement.ControlBounds = new System.Drawing.Rectangle(32, 101, 100, 18);
            this.radLabel3.Size = new System.Drawing.Size(87, 19);
            this.radLabel3.TabIndex = 41;
            this.radLabel3.Text = "Date Range To";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(19, 10);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(136, 19);
            this.label35.TabIndex = 36;
            this.label35.Text = "Palm Beach Search";
            // 
            // radPalmDateFrom
            // 
            this.radPalmDateFrom.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radPalmDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.radPalmDateFrom.Location = new System.Drawing.Point(164, 71);
            this.radPalmDateFrom.Name = "radPalmDateFrom";
            // 
            // 
            // 
            this.radPalmDateFrom.RootElement.AccessibleDescription = null;
            this.radPalmDateFrom.RootElement.AccessibleName = null;
            this.radPalmDateFrom.RootElement.ControlBounds = new System.Drawing.Rectangle(164, 71, 164, 20);
            this.radPalmDateFrom.RootElement.StretchVertically = true;
            this.radPalmDateFrom.Size = new System.Drawing.Size(208, 20);
            this.radPalmDateFrom.TabIndex = 35;
            this.radPalmDateFrom.TabStop = false;
            this.radPalmDateFrom.Text = "04-Jan-17";
            this.radPalmDateFrom.Value = new System.DateTime(2017, 1, 4, 0, 0, 0, 0);
            // 
            // radPalmDateTo
            // 
            this.radPalmDateTo.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radPalmDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.radPalmDateTo.Location = new System.Drawing.Point(164, 102);
            this.radPalmDateTo.Name = "radPalmDateTo";
            // 
            // 
            // 
            this.radPalmDateTo.RootElement.AccessibleDescription = null;
            this.radPalmDateTo.RootElement.AccessibleName = null;
            this.radPalmDateTo.RootElement.ControlBounds = new System.Drawing.Rectangle(164, 102, 164, 20);
            this.radPalmDateTo.RootElement.StretchVertically = true;
            this.radPalmDateTo.Size = new System.Drawing.Size(208, 20);
            this.radPalmDateTo.TabIndex = 34;
            this.radPalmDateTo.TabStop = false;
            this.radPalmDateTo.Text = "04-Jan-17";
            this.radPalmDateTo.Value = new System.DateTime(2017, 1, 4, 0, 0, 0, 0);
            // 
            // lblInternetConnection
            // 
            this.lblInternetConnection.AutoSize = true;
            this.lblInternetConnection.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblInternetConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInternetConnection.ForeColor = System.Drawing.Color.Red;
            this.lblInternetConnection.Location = new System.Drawing.Point(323, 521);
            this.lblInternetConnection.Name = "lblInternetConnection";
            this.lblInternetConnection.Size = new System.Drawing.Size(245, 20);
            this.lblInternetConnection.TabIndex = 61;
            this.lblInternetConnection.Text = "Internet connection is unavailable";
            // 
            // radmenuPalmBeach
            // 
            this.radmenuPalmBeach.AccessibleDescription = "Palm Beach Search";
            this.radmenuPalmBeach.AccessibleName = "Palm Beach Search";
            this.radmenuPalmBeach.AngleTransform = 90F;
            this.radmenuPalmBeach.Image = ((System.Drawing.Image)(resources.GetObject("radmenuPalmBeach.Image")));
            this.radmenuPalmBeach.Name = "radmenuPalmBeach";
            this.radmenuPalmBeach.Padding = new System.Windows.Forms.Padding(20, 1, 8, 1);
            this.radmenuPalmBeach.RightToLeft = false;
            this.radmenuPalmBeach.Text = "Palm Beach Search";
            this.radmenuPalmBeach.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radmenuPalmBeach.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.radmenuPalmBeach.TextOrientation = System.Windows.Forms.Orientation.Vertical;
            this.radmenuPalmBeach.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radmenuPalmBeach.Click += new System.EventHandler(this.radmenuPalmBeach_Click);
            this.radmenuPalmBeach.Image = RotateImageIfReq(this.radmenuPalmBeach.Image);
            // 
            // radMenuMiamiDade
            // 
            this.radMenuMiamiDade.AccessibleDescription = "Miami Dade Search";
            this.radMenuMiamiDade.AccessibleName = "Miami Dade Search";
            this.radMenuMiamiDade.AngleTransform = 90F;
            this.radMenuMiamiDade.Image = ((System.Drawing.Image)(resources.GetObject("radMenuMiamiDade.Image")));
            this.radMenuMiamiDade.Image = RotateImageIfReq(this.radMenuMiamiDade.Image);
            this.radMenuMiamiDade.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radMenuMiamiDade.Name = "radMenuMiamiDade";
            this.radMenuMiamiDade.Text = "Miami Dade Search";
            this.radMenuMiamiDade.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.radMenuMiamiDade.TextOrientation = System.Windows.Forms.Orientation.Vertical;
            this.radMenuMiamiDade.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radMenuMiamiDade.Click += new System.EventHandler(this.radMenuMiamiDade_Click);
            // 
            // pnlGeneral
            // 
            this.pnlGeneral.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pnlGeneral.Controls.Add(this.pnlBulk);
            this.pnlGeneral.Controls.Add(this.pnlsearchByFolio);
            this.pnlGeneral.Controls.Add(this.radGroupBox1);
            this.pnlGeneral.Controls.Add(this.dailySearchPanel);
            this.pnlGeneral.Controls.Add(this.gbSelectCounty);
            this.pnlGeneral.Controls.Add(this.pnlSearchByAddress);
            this.pnlGeneral.Controls.Add(this.btnManualPropSearch);
            this.pnlGeneral.Location = new System.Drawing.Point(121, 209);
            this.pnlGeneral.Name = "pnlGeneral";
            // 
            // 
            // 
            this.pnlGeneral.RootElement.AccessibleDescription = null;
            this.pnlGeneral.RootElement.AccessibleName = null;
            this.pnlGeneral.RootElement.ControlBounds = new System.Drawing.Rectangle(121, 209, 200, 100);
            this.pnlGeneral.Size = new System.Drawing.Size(715, 165);
            this.pnlGeneral.TabIndex = 62;
            this.pnlGeneral.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlGeneral_Paint_1);
            // 
            // pnlBulk
            // 
            this.pnlBulk.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlBulk.Controls.Add(this.btnSearchAllCasesAddress);
            this.pnlBulk.Controls.Add(this.lblPendingBulkCases);
            this.pnlBulk.Controls.Add(this.label38);
            this.pnlBulk.Controls.Add(this.radBulkCases);
            this.pnlBulk.Controls.Add(this.radLPcases);
            this.pnlBulk.Controls.Add(this.lblPendingLPcases);
            this.pnlBulk.Controls.Add(this.label40);
            this.pnlBulk.Location = new System.Drawing.Point(14, 368);
            this.pnlBulk.Name = "pnlBulk";
            // 
            // 
            // 
            this.pnlBulk.RootElement.AccessibleDescription = null;
            this.pnlBulk.RootElement.AccessibleName = null;
            this.pnlBulk.RootElement.ControlBounds = new System.Drawing.Rectangle(14, 368, 200, 100);
            this.pnlBulk.Size = new System.Drawing.Size(687, 106);
            this.pnlBulk.TabIndex = 33;
            // 
            // btnSearchAllCasesAddress
            // 
            this.btnSearchAllCasesAddress.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSearchAllCasesAddress.Location = new System.Drawing.Point(420, 41);
            this.btnSearchAllCasesAddress.Name = "btnSearchAllCasesAddress";
            // 
            // 
            // 
            this.btnSearchAllCasesAddress.RootElement.AccessibleDescription = null;
            this.btnSearchAllCasesAddress.RootElement.AccessibleName = null;
            this.btnSearchAllCasesAddress.RootElement.ControlBounds = new System.Drawing.Rectangle(420, 41, 110, 24);
            this.btnSearchAllCasesAddress.Size = new System.Drawing.Size(174, 34);
            this.btnSearchAllCasesAddress.TabIndex = 47;
            this.btnSearchAllCasesAddress.Text = "Search all Cases Addresses";
            this.btnSearchAllCasesAddress.Click += new System.EventHandler(this.btnsrchByLPAddress_Click);
            // 
            // lblPendingBulkCases
            // 
            this.lblPendingBulkCases.AutoSize = true;
            this.lblPendingBulkCases.Location = new System.Drawing.Point(105, 41);
            this.lblPendingBulkCases.Name = "lblPendingBulkCases";
            this.lblPendingBulkCases.Size = new System.Drawing.Size(0, 13);
            this.lblPendingBulkCases.TabIndex = 46;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(105, 12);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(103, 13);
            this.label38.TabIndex = 45;
            this.label38.Text = "Choose Search For";
            // 
            // radBulkCases
            // 
            this.radBulkCases.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radBulkCases.Location = new System.Drawing.Point(474, 7);
            this.radBulkCases.Name = "radBulkCases";
            // 
            // 
            // 
            this.radBulkCases.RootElement.AccessibleDescription = null;
            this.radBulkCases.RootElement.AccessibleName = null;
            this.radBulkCases.RootElement.ControlBounds = new System.Drawing.Rectangle(474, 7, 110, 18);
            this.radBulkCases.RootElement.StretchHorizontally = true;
            this.radBulkCases.RootElement.StretchVertically = true;
            this.radBulkCases.Size = new System.Drawing.Size(73, 18);
            this.radBulkCases.TabIndex = 44;
            this.radBulkCases.Text = "Bulk Cases";
            // 
            // radLPcases
            // 
            this.radLPcases.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radLPcases.Location = new System.Drawing.Point(318, 7);
            this.radLPcases.Name = "radLPcases";
            // 
            // 
            // 
            this.radLPcases.RootElement.AccessibleDescription = null;
            this.radLPcases.RootElement.AccessibleName = null;
            this.radLPcases.RootElement.ControlBounds = new System.Drawing.Rectangle(318, 7, 110, 18);
            this.radLPcases.RootElement.StretchHorizontally = true;
            this.radLPcases.RootElement.StretchVertically = true;
            this.radLPcases.Size = new System.Drawing.Size(63, 18);
            this.radLPcases.TabIndex = 43;
            this.radLPcases.Text = "LP Cases";
            // 
            // lblPendingLPcases
            // 
            this.lblPendingLPcases.AutoSize = true;
            this.lblPendingLPcases.Location = new System.Drawing.Point(105, 62);
            this.lblPendingLPcases.Name = "lblPendingLPcases";
            this.lblPendingLPcases.Size = new System.Drawing.Size(0, 13);
            this.lblPendingLPcases.TabIndex = 42;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(105, 113);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(256, 13);
            this.label40.TabIndex = 41;
            this.label40.Text = "Automatic Search on BCPA web site for all  Cases";
            // 
            // pnlsearchByFolio
            // 
            this.pnlsearchByFolio.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlsearchByFolio.Controls.Add(this.lblFolioNumber);
            this.pnlsearchByFolio.Controls.Add(this.txtFolioNumber);
            this.pnlsearchByFolio.Location = new System.Drawing.Point(137, 78);
            this.pnlsearchByFolio.Name = "pnlsearchByFolio";
            // 
            // 
            // 
            this.pnlsearchByFolio.RootElement.AccessibleDescription = null;
            this.pnlsearchByFolio.RootElement.AccessibleName = null;
            this.pnlsearchByFolio.RootElement.ControlBounds = new System.Drawing.Rectangle(137, 78, 200, 100);
            this.pnlsearchByFolio.Size = new System.Drawing.Size(422, 64);
            this.pnlsearchByFolio.TabIndex = 31;
            this.pnlsearchByFolio.Paint += new System.Windows.Forms.PaintEventHandler(this.radPanel2_Paint);
            // 
            // lblFolioNumber
            // 
            this.lblFolioNumber.AutoSize = true;
            this.lblFolioNumber.Location = new System.Drawing.Point(13, 26);
            this.lblFolioNumber.Name = "lblFolioNumber";
            this.lblFolioNumber.Size = new System.Drawing.Size(107, 13);
            this.lblFolioNumber.TabIndex = 30;
            this.lblFolioNumber.Text = "Enter Folio Number";
            // 
            // txtFolioNumber
            // 
            this.txtFolioNumber.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtFolioNumber.Location = new System.Drawing.Point(190, 25);
            this.txtFolioNumber.Name = "txtFolioNumber";
            // 
            // 
            // 
            this.txtFolioNumber.RootElement.AccessibleDescription = null;
            this.txtFolioNumber.RootElement.AccessibleName = null;
            this.txtFolioNumber.RootElement.ControlBounds = new System.Drawing.Rectangle(190, 25, 100, 20);
            this.txtFolioNumber.RootElement.StretchVertically = true;
            this.txtFolioNumber.Size = new System.Drawing.Size(216, 20);
            this.txtFolioNumber.TabIndex = 29;
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGroupBox1.Controls.Add(this.radBtnScheduledSearch);
            this.radGroupBox1.Controls.Add(this.radbtnFolioNumber);
            this.radGroupBox1.Controls.Add(this.radbtnCompleteAddress);
            this.radGroupBox1.HeaderText = "Search By";
            this.radGroupBox1.Location = new System.Drawing.Point(299, 13);
            this.radGroupBox1.Name = "radGroupBox1";
            // 
            // 
            // 
            this.radGroupBox1.RootElement.AccessibleDescription = null;
            this.radGroupBox1.RootElement.AccessibleName = null;
            this.radGroupBox1.RootElement.ControlBounds = new System.Drawing.Rectangle(299, 13, 200, 100);
            this.radGroupBox1.Size = new System.Drawing.Size(361, 56);
            this.radGroupBox1.TabIndex = 12;
            this.radGroupBox1.Text = "Search By";
            // 
            // radBtnScheduledSearch
            // 
            this.radBtnScheduledSearch.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radBtnScheduledSearch.Location = new System.Drawing.Point(13, 27);
            this.radBtnScheduledSearch.Name = "radBtnScheduledSearch";
            // 
            // 
            // 
            this.radBtnScheduledSearch.RootElement.AccessibleDescription = null;
            this.radBtnScheduledSearch.RootElement.AccessibleName = null;
            this.radBtnScheduledSearch.RootElement.ControlBounds = new System.Drawing.Rectangle(13, 27, 110, 18);
            this.radBtnScheduledSearch.RootElement.StretchHorizontally = true;
            this.radBtnScheduledSearch.RootElement.StretchVertically = true;
            this.radBtnScheduledSearch.Size = new System.Drawing.Size(109, 18);
            this.radBtnScheduledSearch.TabIndex = 28;
            this.radBtnScheduledSearch.TabStop = true;
            this.radBtnScheduledSearch.Text = "Scheduled Search";
            this.radBtnScheduledSearch.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radBtnScheduledSearch.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radBtnScheduledSearch_ToggleStateChanged);
            // 
            // gbSelectCounty
            // 
            this.gbSelectCounty.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.gbSelectCounty.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.gbSelectCounty.Controls.Add(this.cmbCountyList);
            this.gbSelectCounty.HeaderText = "Select County";
            this.gbSelectCounty.Location = new System.Drawing.Point(54, 13);
            this.gbSelectCounty.Name = "gbSelectCounty";
            // 
            // 
            // 
            this.gbSelectCounty.RootElement.AccessibleDescription = null;
            this.gbSelectCounty.RootElement.AccessibleName = null;
            this.gbSelectCounty.RootElement.ControlBounds = new System.Drawing.Rectangle(54, 13, 200, 100);
            this.gbSelectCounty.Size = new System.Drawing.Size(206, 56);
            this.gbSelectCounty.TabIndex = 11;
            this.gbSelectCounty.Text = "Select County";
            this.gbSelectCounty.Click += new System.EventHandler(this.gbSelectCounty_Click);
            // 
            // cmbCountyList
            // 
            this.cmbCountyList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cmbCountyList.Location = new System.Drawing.Point(13, 27);
            this.cmbCountyList.Name = "cmbCountyList";
            // 
            // 
            // 
            this.cmbCountyList.RootElement.AccessibleDescription = null;
            this.cmbCountyList.RootElement.AccessibleName = null;
            this.cmbCountyList.RootElement.ControlBounds = new System.Drawing.Rectangle(13, 27, 125, 20);
            this.cmbCountyList.RootElement.StretchVertically = true;
            this.cmbCountyList.Size = new System.Drawing.Size(179, 20);
            this.cmbCountyList.TabIndex = 10;
            this.cmbCountyList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cmbCountyList_SelectedIndexChanged);
            // 
            // pnlSearchByAddress
            // 
            this.pnlSearchByAddress.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlSearchByAddress.Controls.Add(this.txtUnitNumber1);
            this.pnlSearchByAddress.Controls.Add(this.txtPostDirection1);
            this.pnlSearchByAddress.Controls.Add(this.label43);
            this.pnlSearchByAddress.Controls.Add(this.label44);
            this.pnlSearchByAddress.Controls.Add(this.label45);
            this.pnlSearchByAddress.Controls.Add(this.txtStreetNumber1);
            this.pnlSearchByAddress.Controls.Add(this.txtStreeCity1);
            this.pnlSearchByAddress.Controls.Add(this.label46);
            this.pnlSearchByAddress.Controls.Add(this.txtStreetType1);
            this.pnlSearchByAddress.Controls.Add(this.label47);
            this.pnlSearchByAddress.Controls.Add(this.label48);
            this.pnlSearchByAddress.Controls.Add(this.txtStreetDirection1);
            this.pnlSearchByAddress.Controls.Add(this.label49);
            this.pnlSearchByAddress.Controls.Add(this.txtStreetName1);
            this.pnlSearchByAddress.Location = new System.Drawing.Point(73, 113);
            this.pnlSearchByAddress.Name = "pnlSearchByAddress";
            // 
            // 
            // 
            this.pnlSearchByAddress.RootElement.AccessibleDescription = null;
            this.pnlSearchByAddress.RootElement.AccessibleName = null;
            this.pnlSearchByAddress.RootElement.ControlBounds = new System.Drawing.Rectangle(73, 113, 200, 100);
            this.pnlSearchByAddress.Size = new System.Drawing.Size(473, 208);
            this.pnlSearchByAddress.TabIndex = 28;
            // 
            // txtUnitNumber1
            // 
            this.txtUnitNumber1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtUnitNumber1.Location = new System.Drawing.Point(186, 153);
            this.txtUnitNumber1.Name = "txtUnitNumber1";
            // 
            // 
            // 
            this.txtUnitNumber1.RootElement.AccessibleDescription = null;
            this.txtUnitNumber1.RootElement.AccessibleName = null;
            this.txtUnitNumber1.RootElement.ControlBounds = new System.Drawing.Rectangle(186, 153, 100, 20);
            this.txtUnitNumber1.RootElement.StretchVertically = true;
            this.txtUnitNumber1.Size = new System.Drawing.Size(203, 20);
            this.txtUnitNumber1.TabIndex = 21;
            // 
            // txtPostDirection1
            // 
            this.txtPostDirection1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtPostDirection1.Location = new System.Drawing.Point(186, 127);
            this.txtPostDirection1.Name = "txtPostDirection1";
            // 
            // 
            // 
            this.txtPostDirection1.RootElement.AccessibleDescription = null;
            this.txtPostDirection1.RootElement.AccessibleName = null;
            this.txtPostDirection1.RootElement.ControlBounds = new System.Drawing.Rectangle(186, 127, 100, 20);
            this.txtPostDirection1.RootElement.StretchVertically = true;
            this.txtPostDirection1.Size = new System.Drawing.Size(203, 20);
            this.txtPostDirection1.TabIndex = 28;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(40, 160);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(73, 13);
            this.label43.TabIndex = 27;
            this.label43.Text = "Unit Number";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(40, 134);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(79, 13);
            this.label44.TabIndex = 26;
            this.label44.Text = "Post Direction";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(40, 20);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(84, 13);
            this.label45.TabIndex = 5;
            this.label45.Text = "Street Number ";
            // 
            // txtStreetNumber1
            // 
            this.txtStreetNumber1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStreetNumber1.Location = new System.Drawing.Point(186, 20);
            this.txtStreetNumber1.Name = "txtStreetNumber1";
            // 
            // 
            // 
            this.txtStreetNumber1.RootElement.AccessibleDescription = null;
            this.txtStreetNumber1.RootElement.AccessibleName = null;
            this.txtStreetNumber1.RootElement.ControlBounds = new System.Drawing.Rectangle(186, 20, 100, 20);
            this.txtStreetNumber1.RootElement.StretchVertically = true;
            this.txtStreetNumber1.Size = new System.Drawing.Size(203, 20);
            this.txtStreetNumber1.TabIndex = 16;
            // 
            // txtStreeCity1
            // 
            this.txtStreeCity1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStreeCity1.Location = new System.Drawing.Point(186, 182);
            this.txtStreeCity1.Name = "txtStreeCity1";
            // 
            // 
            // 
            this.txtStreeCity1.RootElement.AccessibleDescription = null;
            this.txtStreeCity1.RootElement.AccessibleName = null;
            this.txtStreeCity1.RootElement.ControlBounds = new System.Drawing.Rectangle(186, 182, 100, 20);
            this.txtStreeCity1.RootElement.StretchVertically = true;
            this.txtStreeCity1.Size = new System.Drawing.Size(203, 20);
            this.txtStreeCity1.TabIndex = 21;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(40, 189);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(59, 13);
            this.label46.TabIndex = 25;
            this.label46.Text = "Street City";
            // 
            // txtStreetType1
            // 
            this.txtStreetType1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStreetType1.Location = new System.Drawing.Point(186, 100);
            this.txtStreetType1.Name = "txtStreetType1";
            // 
            // 
            // 
            this.txtStreetType1.RootElement.AccessibleDescription = null;
            this.txtStreetType1.RootElement.AccessibleName = null;
            this.txtStreetType1.RootElement.ControlBounds = new System.Drawing.Rectangle(186, 100, 100, 20);
            this.txtStreetType1.RootElement.StretchVertically = true;
            this.txtStreetType1.Size = new System.Drawing.Size(203, 20);
            this.txtStreetType1.TabIndex = 20;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(40, 45);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(90, 13);
            this.label47.TabIndex = 22;
            this.label47.Text = "Street Direction ";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(40, 107);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(65, 13);
            this.label48.TabIndex = 24;
            this.label48.Text = "Street Type ";
            // 
            // txtStreetDirection1
            // 
            this.txtStreetDirection1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStreetDirection1.Location = new System.Drawing.Point(186, 45);
            this.txtStreetDirection1.Name = "txtStreetDirection1";
            // 
            // 
            // 
            this.txtStreetDirection1.RootElement.AccessibleDescription = null;
            this.txtStreetDirection1.RootElement.AccessibleName = null;
            this.txtStreetDirection1.RootElement.ControlBounds = new System.Drawing.Rectangle(186, 45, 100, 20);
            this.txtStreetDirection1.RootElement.StretchVertically = true;
            this.txtStreetDirection1.Size = new System.Drawing.Size(203, 20);
            this.txtStreetDirection1.TabIndex = 18;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(40, 76);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(72, 13);
            this.label49.TabIndex = 23;
            this.label49.Text = "Street Name ";
            // 
            // txtStreetName1
            // 
            this.txtStreetName1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStreetName1.Location = new System.Drawing.Point(187, 72);
            this.txtStreetName1.Name = "txtStreetName1";
            // 
            // 
            // 
            this.txtStreetName1.RootElement.AccessibleDescription = null;
            this.txtStreetName1.RootElement.AccessibleName = null;
            this.txtStreetName1.RootElement.ControlBounds = new System.Drawing.Rectangle(187, 72, 100, 20);
            this.txtStreetName1.RootElement.StretchVertically = true;
            this.txtStreetName1.Size = new System.Drawing.Size(203, 20);
            this.txtStreetName1.TabIndex = 19;
            // 
            // btnManualPropSearch
            // 
            this.btnManualPropSearch.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnManualPropSearch.Location = new System.Drawing.Point(246, 327);
            this.btnManualPropSearch.Name = "btnManualPropSearch";
            // 
            // 
            // 
            this.btnManualPropSearch.RootElement.AccessibleDescription = null;
            this.btnManualPropSearch.RootElement.AccessibleName = null;
            this.btnManualPropSearch.RootElement.ControlBounds = new System.Drawing.Rectangle(246, 327, 110, 24);
            this.btnManualPropSearch.Size = new System.Drawing.Size(175, 35);
            this.btnManualPropSearch.TabIndex = 39;
            this.btnManualPropSearch.Text = "Begin Manual Property Search";
            this.btnManualPropSearch.Click += new System.EventHandler(this.PropertSearch_Click_1);
            // 
            // pnlsearchspokeosingleaddress
            // 
            this.pnlsearchspokeosingleaddress.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pnlsearchspokeosingleaddress.Controls.Add(this.lblspokeosingleaddress);
            this.pnlsearchspokeosingleaddress.Controls.Add(this.txtspokeosingleaddress);
            this.pnlsearchspokeosingleaddress.Location = new System.Drawing.Point(137, 78);
            this.pnlsearchspokeosingleaddress.Name = "pnlsearchspokeosingleaddress";
            // 
            // 
            // 
            this.pnlsearchspokeosingleaddress.RootElement.AccessibleDescription = null;
            this.pnlsearchspokeosingleaddress.RootElement.AccessibleName = null;
            this.pnlsearchspokeosingleaddress.RootElement.ControlBounds = new System.Drawing.Rectangle(137, 78, 200, 100);
            this.pnlsearchspokeosingleaddress.Size = new System.Drawing.Size(422, 64);
            this.pnlsearchspokeosingleaddress.TabIndex = 31;
            this.pnlsearchspokeosingleaddress.Paint += new System.Windows.Forms.PaintEventHandler(this.radPanel2_Paint);
            // 
            // lblspokeosingleaddress
            // 
            this.lblspokeosingleaddress.AutoSize = true;
            this.lblspokeosingleaddress.Location = new System.Drawing.Point(13, 26);
            this.lblspokeosingleaddress.Name = "lblspokeosingleaddress";
            this.lblspokeosingleaddress.Size = new System.Drawing.Size(106, 13);
            this.lblspokeosingleaddress.TabIndex = 30;
            this.lblspokeosingleaddress.Text = "Enter Sigle Address";
            // 
            // txtspokeosingleaddress
            // 
            this.txtspokeosingleaddress.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtspokeosingleaddress.Location = new System.Drawing.Point(190, 25);
            this.txtspokeosingleaddress.Name = "txtspokeosingleaddress";
            // 
            // 
            // 
            this.txtspokeosingleaddress.RootElement.AccessibleDescription = null;
            this.txtspokeosingleaddress.RootElement.AccessibleName = null;
            this.txtspokeosingleaddress.RootElement.ControlBounds = new System.Drawing.Rectangle(190, 25, 100, 20);
            this.txtspokeosingleaddress.RootElement.StretchVertically = true;
            this.txtspokeosingleaddress.Size = new System.Drawing.Size(216, 20);
            this.txtspokeosingleaddress.TabIndex = 29;
            // 
            // object_73c9a747_22cf_4f3a_a660_2813c219558a
            // 
            this.object_73c9a747_22cf_4f3a_a660_2813c219558a.StretchHorizontally = true;
            this.object_73c9a747_22cf_4f3a_a660_2813c219558a.StretchVertically = true;
            // 
            // SelectCategoryMenu
            // 
            this.SelectCategoryMenu.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.SelectCategoryMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.SelectCategoryMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.Home,
            this.radMenuItem1,
            this.radMenuGeneral,
            this.radmenuBroward,
            this.radmenuPalmBeach,
            this.radMenuMiamiDade,
            this.radMenuSpokeoSearch,
            this.radMenuSunbizSearch,
            this.radMenuFSBOSearch,
            this.radmenuSocialmediaSearch});
            this.SelectCategoryMenu.Location = new System.Drawing.Point(0, 0);
            this.SelectCategoryMenu.Name = "SelectCategoryMenu";
            this.SelectCategoryMenu.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.SelectCategoryMenu.Padding = new System.Windows.Forms.Padding(0, 1, 3, 0);
            // 
            // 
            // 
            this.SelectCategoryMenu.RootElement.AccessibleDescription = null;
            this.SelectCategoryMenu.RootElement.AccessibleName = null;
            this.SelectCategoryMenu.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 24);
            this.SelectCategoryMenu.Size = new System.Drawing.Size(111, 625);
            this.SelectCategoryMenu.TabIndex = 11;
            // 
            // radMenuSpokeoSearch
            // 
            this.radMenuSpokeoSearch.AccessibleDescription = "Spokeo Search";
            this.radMenuSpokeoSearch.AccessibleName = "Spokeo Search";
            this.radMenuSpokeoSearch.AngleTransform = 90F;
            this.radMenuSpokeoSearch.Image = ((System.Drawing.Image)(resources.GetObject("radMenuSpokeoSearch.Image")));
            this.radMenuSpokeoSearch.Image = RotateImageIfReq(this.radMenuSpokeoSearch.Image);
            this.radMenuSpokeoSearch.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radMenuSpokeoSearch.Name = "radMenuSpokeoSearch";
            this.radMenuSpokeoSearch.Text = "Spokeo Search";
            this.radMenuSpokeoSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.radMenuSpokeoSearch.TextOrientation = System.Windows.Forms.Orientation.Vertical;
            this.radMenuSpokeoSearch.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radMenuSpokeoSearch.Click += new System.EventHandler(this.radMenuSpokeoSearch_Click);
            // 
            // radMenuSunbizSearch
            // 

            Image RotateImageIfReq(Image img)
            {
                img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                return img;

            }


            this.radMenuSunbizSearch.AccessibleDescription = "Sunbiz Search";
            this.radMenuSunbizSearch.AccessibleName = "Sunbiz Search";
            this.radMenuSunbizSearch.AngleTransform = 90F;
            this.radMenuSunbizSearch.Image = ((System.Drawing.Image)(resources.GetObject("radMenuSunbizSearch.Image")));
            this.radMenuSunbizSearch.Image = RotateImageIfReq(this.radMenuSunbizSearch.Image);
            this.radMenuSunbizSearch.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radMenuSunbizSearch.Name = "radMenuSunbizSearch";
            this.radMenuSunbizSearch.Text = "Sunbiz Search";
            this.radMenuSunbizSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.radMenuSunbizSearch.TextOrientation = System.Windows.Forms.Orientation.Vertical;
            this.radMenuSunbizSearch.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radMenuSunbizSearch.Click += new System.EventHandler(this.radMenuSunbizSearch_Click);
            // 
            // radMenuFSBOSearch
            // 
            this.radMenuFSBOSearch.AccessibleDescription = "RedZillLoop Search";
            this.radMenuFSBOSearch.AccessibleName = "RedZillLoop Search";
            this.radMenuFSBOSearch.AngleTransform = 90F;
            this.radMenuFSBOSearch.Image = ((System.Drawing.Image)(resources.GetObject("radMenuFSBOSearch.Image")));
            this.radMenuFSBOSearch.Image = RotateImageIfReq(this.radMenuFSBOSearch.Image);
            this.radMenuFSBOSearch.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radMenuFSBOSearch.Name = "radMenuFSBOSearch";
            this.radMenuFSBOSearch.Text = "RedZillLoop Search";
            this.radMenuFSBOSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.radMenuFSBOSearch.TextOrientation = System.Windows.Forms.Orientation.Vertical;
            this.radMenuFSBOSearch.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radMenuFSBOSearch.Click += new System.EventHandler(this.radMenuFSBOSearch_Click);
            // 
            // radmenuSocialmediaSearch
            // 
            this.radmenuSocialmediaSearch.AccessibleDescription = "Social Media";
            this.radmenuSocialmediaSearch.AccessibleName = "Social Media";
            this.radmenuSocialmediaSearch.AngleTransform = 90F;
            this.radmenuSocialmediaSearch.Image = ((System.Drawing.Image)(resources.GetObject("radmenuSocialmediaSearch.Image")));
            this.radmenuSocialmediaSearch.Image = RotateImageIfReq(this.radmenuSocialmediaSearch.Image);
            this.radmenuSocialmediaSearch.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radmenuSocialmediaSearch.Name = "radmenuSocialmediaSearch";
            this.radmenuSocialmediaSearch.Text = "Social Media";
            this.radmenuSocialmediaSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.radmenuSocialmediaSearch.TextOrientation = System.Windows.Forms.Orientation.Vertical;
            this.radmenuSocialmediaSearch.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radmenuSocialmediaSearch.Click += new System.EventHandler(this.radmenuSocialmediaSearch_Click);
            // 
            // pnlSpokeoSearch
            // 
            this.pnlSpokeoSearch.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pnlSpokeoSearch.Controls.Add(this.radGroupBox2);
            this.pnlSpokeoSearch.Controls.Add(this.btnBeginSpokeoSearch);
            this.pnlSpokeoSearch.Controls.Add(this.radGrpSearchByPerson);
            this.pnlSpokeoSearch.Controls.Add(this.radGrpSearchByAddress);
            this.pnlSpokeoSearch.Controls.Add(this.lblSpokeoSearch);
            this.pnlSpokeoSearch.Location = new System.Drawing.Point(160, 367);
            this.pnlSpokeoSearch.Name = "pnlSpokeoSearch";
            // 
            // 
            // 
            this.pnlSpokeoSearch.RootElement.AccessibleDescription = null;
            this.pnlSpokeoSearch.RootElement.AccessibleName = null;
            this.pnlSpokeoSearch.RootElement.ControlBounds = new System.Drawing.Rectangle(160, 367, 200, 100);
            this.pnlSpokeoSearch.Size = new System.Drawing.Size(695, 115);
            this.pnlSpokeoSearch.TabIndex = 63;
            this.pnlSpokeoSearch.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.pnlSpokeoSearch.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlSpokeoSearch_Paint);
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.radGroupBox2.Controls.Add(this.cmbSpokeoCountyList);
            this.radGroupBox2.HeaderText = "Select County";
            this.radGroupBox2.Location = new System.Drawing.Point(9, 46);
            this.radGroupBox2.Name = "radGroupBox2";
            // 
            // 
            // 
            this.radGroupBox2.RootElement.AccessibleDescription = null;
            this.radGroupBox2.RootElement.AccessibleName = null;
            this.radGroupBox2.RootElement.ControlBounds = new System.Drawing.Rectangle(9, 46, 200, 100);
            this.radGroupBox2.Size = new System.Drawing.Size(206, 54);
            this.radGroupBox2.TabIndex = 12;
            this.radGroupBox2.Text = "Select County";
            this.radGroupBox2.Click += new System.EventHandler(this.radGroupBox2_Click_1);
            // 
            // cmbSpokeoCountyList
            // 
            this.cmbSpokeoCountyList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cmbSpokeoCountyList.Location = new System.Drawing.Point(13, 27);
            this.cmbSpokeoCountyList.Name = "cmbSpokeoCountyList";
            // 
            // 
            // 
            this.cmbSpokeoCountyList.RootElement.AccessibleDescription = null;
            this.cmbSpokeoCountyList.RootElement.AccessibleName = null;
            this.cmbSpokeoCountyList.RootElement.ControlBounds = new System.Drawing.Rectangle(13, 27, 125, 20);
            this.cmbSpokeoCountyList.RootElement.StretchVertically = true;
            this.cmbSpokeoCountyList.Size = new System.Drawing.Size(179, 20);
            this.cmbSpokeoCountyList.TabIndex = 10;
            // 
            // btnBeginSpokeoSearch
            // 
            this.btnBeginSpokeoSearch.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnBeginSpokeoSearch.Location = new System.Drawing.Point(505, 134);
            this.btnBeginSpokeoSearch.Name = "btnBeginSpokeoSearch";
            // 
            // 
            // 
            this.btnBeginSpokeoSearch.RootElement.AccessibleDescription = null;
            this.btnBeginSpokeoSearch.RootElement.AccessibleName = null;
            this.btnBeginSpokeoSearch.RootElement.ControlBounds = new System.Drawing.Rectangle(505, 134, 110, 24);
            this.btnBeginSpokeoSearch.Size = new System.Drawing.Size(142, 33);
            this.btnBeginSpokeoSearch.TabIndex = 4;
            this.btnBeginSpokeoSearch.Text = "Begin Search";
            this.btnBeginSpokeoSearch.Click += new System.EventHandler(this.btnBeginSpokeoSearch_Click);
            // 
            // radGrpSearchByPerson
            // 
            this.radGrpSearchByPerson.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGrpSearchByPerson.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGrpSearchByPerson.Controls.Add(this.radAllPerson);
            this.radGrpSearchByPerson.Controls.Add(this.radSearchByBulkPerson);
            this.radGrpSearchByPerson.Controls.Add(this.radSearchBySinglePerson);
            this.radGrpSearchByPerson.HeaderText = "Search By Person";
            this.radGrpSearchByPerson.Location = new System.Drawing.Point(464, 46);
            this.radGrpSearchByPerson.Name = "radGrpSearchByPerson";
            // 
            // 
            // 
            this.radGrpSearchByPerson.RootElement.AccessibleDescription = null;
            this.radGrpSearchByPerson.RootElement.AccessibleName = null;
            this.radGrpSearchByPerson.RootElement.ControlBounds = new System.Drawing.Rectangle(464, 46, 200, 100);
            this.radGrpSearchByPerson.Size = new System.Drawing.Size(221, 54);
            this.radGrpSearchByPerson.TabIndex = 3;
            this.radGrpSearchByPerson.Text = "Search By Person";
            this.radGrpSearchByPerson.Click += new System.EventHandler(this.radGroupBox2_Click);
            // 
            // radAllPerson
            // 
            this.radAllPerson.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radAllPerson.Location = new System.Drawing.Point(8, 25);
            this.radAllPerson.Name = "radAllPerson";
            // 
            // 
            // 
            this.radAllPerson.RootElement.AccessibleDescription = null;
            this.radAllPerson.RootElement.AccessibleName = null;
            this.radAllPerson.RootElement.ControlBounds = new System.Drawing.Rectangle(8, 25, 110, 18);
            this.radAllPerson.RootElement.StretchHorizontally = true;
            this.radAllPerson.RootElement.StretchVertically = true;
            this.radAllPerson.Size = new System.Drawing.Size(33, 18);
            this.radAllPerson.TabIndex = 1;
            this.radAllPerson.TabStop = true;
            this.radAllPerson.Text = "All";
            this.radAllPerson.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.radAllPerson.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radAllPerson_ToggleStateChanged);
            // 
            // radSearchByBulkPerson
            // 
            this.radSearchByBulkPerson.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSearchByBulkPerson.Location = new System.Drawing.Point(134, 25);
            this.radSearchByBulkPerson.Name = "radSearchByBulkPerson";
            // 
            // 
            // 
            this.radSearchByBulkPerson.RootElement.AccessibleDescription = null;
            this.radSearchByBulkPerson.RootElement.AccessibleName = null;
            this.radSearchByBulkPerson.RootElement.ControlBounds = new System.Drawing.Rectangle(134, 25, 110, 18);
            this.radSearchByBulkPerson.RootElement.StretchHorizontally = true;
            this.radSearchByBulkPerson.RootElement.StretchVertically = true;
            this.radSearchByBulkPerson.Size = new System.Drawing.Size(79, 18);
            this.radSearchByBulkPerson.TabIndex = 1;
            this.radSearchByBulkPerson.Text = "Bulk Person";
            this.radSearchByBulkPerson.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radRadioButton2_ToggleStateChanged);
            // 
            // radSearchBySinglePerson
            // 
            this.radSearchBySinglePerson.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSearchBySinglePerson.Location = new System.Drawing.Point(43, 25);
            this.radSearchBySinglePerson.Name = "radSearchBySinglePerson";
            // 
            // 
            // 
            this.radSearchBySinglePerson.RootElement.AccessibleDescription = null;
            this.radSearchBySinglePerson.RootElement.AccessibleName = null;
            this.radSearchBySinglePerson.RootElement.ControlBounds = new System.Drawing.Rectangle(43, 25, 110, 18);
            this.radSearchBySinglePerson.RootElement.StretchHorizontally = true;
            this.radSearchBySinglePerson.RootElement.StretchVertically = true;
            this.radSearchBySinglePerson.Size = new System.Drawing.Size(88, 18);
            this.radSearchBySinglePerson.TabIndex = 0;
            this.radSearchBySinglePerson.Text = "Single Person";
            this.radSearchBySinglePerson.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radSearchBySinglePerson_ToggleStateChanged);
            // 
            // radGrpSearchByAddress
            // 
            this.radGrpSearchByAddress.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGrpSearchByAddress.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGrpSearchByAddress.Controls.Add(this.radAllAddress);
            this.radGrpSearchByAddress.Controls.Add(this.radSearchByOneAddress);
            this.radGrpSearchByAddress.Controls.Add(this.radSearchByBulkAddress);
            this.radGrpSearchByAddress.HeaderText = "Search By Address";
            this.radGrpSearchByAddress.Location = new System.Drawing.Point(224, 46);
            this.radGrpSearchByAddress.Name = "radGrpSearchByAddress";
            // 
            // 
            // 
            this.radGrpSearchByAddress.RootElement.AccessibleDescription = null;
            this.radGrpSearchByAddress.RootElement.AccessibleName = null;
            this.radGrpSearchByAddress.RootElement.ControlBounds = new System.Drawing.Rectangle(224, 46, 200, 100);
            this.radGrpSearchByAddress.Size = new System.Drawing.Size(234, 54);
            this.radGrpSearchByAddress.TabIndex = 2;
            this.radGrpSearchByAddress.Text = "Search By Address";
            this.radGrpSearchByAddress.Click += new System.EventHandler(this.radGrpSpokeoSearch_Click);
            // 
            // radAllAddress
            // 
            this.radAllAddress.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radAllAddress.Location = new System.Drawing.Point(10, 26);
            this.radAllAddress.Name = "radAllAddress";
            // 
            // 
            // 
            this.radAllAddress.RootElement.AccessibleDescription = null;
            this.radAllAddress.RootElement.AccessibleName = null;
            this.radAllAddress.RootElement.ControlBounds = new System.Drawing.Rectangle(10, 26, 110, 18);
            this.radAllAddress.RootElement.StretchHorizontally = true;
            this.radAllAddress.RootElement.StretchVertically = true;
            this.radAllAddress.Size = new System.Drawing.Size(33, 18);
            this.radAllAddress.TabIndex = 2;
            this.radAllAddress.Text = "All";
            // 
            // radSearchByOneAddress
            // 
            this.radSearchByOneAddress.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSearchByOneAddress.Location = new System.Drawing.Point(45, 25);
            this.radSearchByOneAddress.Name = "radSearchByOneAddress";
            // 
            // 
            // 
            this.radSearchByOneAddress.RootElement.AccessibleDescription = null;
            this.radSearchByOneAddress.RootElement.AccessibleName = null;
            this.radSearchByOneAddress.RootElement.ControlBounds = new System.Drawing.Rectangle(45, 25, 110, 18);
            this.radSearchByOneAddress.RootElement.StretchHorizontally = true;
            this.radSearchByOneAddress.RootElement.StretchVertically = true;
            this.radSearchByOneAddress.Size = new System.Drawing.Size(94, 18);
            this.radSearchByOneAddress.TabIndex = 1;
            this.radSearchByOneAddress.Text = "Single Address";
            this.radSearchByOneAddress.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radSearchByOneAddress_ToggleStateChanged);
            // 
            // radSearchByBulkAddress
            // 
            this.radSearchByBulkAddress.AutoSize = false;
            this.radSearchByBulkAddress.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSearchByBulkAddress.Location = new System.Drawing.Point(142, 25);
            this.radSearchByBulkAddress.Name = "radSearchByBulkAddress";
            // 
            // 
            // 
            this.radSearchByBulkAddress.RootElement.AccessibleDescription = null;
            this.radSearchByBulkAddress.RootElement.AccessibleName = null;
            this.radSearchByBulkAddress.RootElement.ControlBounds = new System.Drawing.Rectangle(142, 25, 110, 18);
            this.radSearchByBulkAddress.Size = new System.Drawing.Size(85, 18);
            this.radSearchByBulkAddress.TabIndex = 0;
            this.radSearchByBulkAddress.Text = "Bulk Address";
            this.radSearchByBulkAddress.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radSearchByBulkAddress_ToggleStateChanged);
            // 
            // lblSpokeoSearch
            // 
            this.lblSpokeoSearch.AutoSize = true;
            this.lblSpokeoSearch.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpokeoSearch.Location = new System.Drawing.Point(291, 17);
            this.lblSpokeoSearch.Name = "lblSpokeoSearch";
            this.lblSpokeoSearch.Size = new System.Drawing.Size(97, 17);
            this.lblSpokeoSearch.TabIndex = 1;
            this.lblSpokeoSearch.Text = "Spokeo Search";
            // 
            // lblFSBOSearch
            // 
            this.lblFSBOSearch.AutoSize = true;
            this.lblFSBOSearch.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFSBOSearch.Location = new System.Drawing.Point(291, 17);
            this.lblFSBOSearch.Name = "lblFSBOSearch";
            this.lblFSBOSearch.Size = new System.Drawing.Size(126, 17);
            this.lblFSBOSearch.TabIndex = 1;
            this.lblFSBOSearch.Text = "RedZillLoop Search";
            // 
            // lblSocialMedia
            // 
            this.lblSocialMedia.AutoSize = true;
            this.lblSocialMedia.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSocialMedia.Location = new System.Drawing.Point(291, 17);
            this.lblSocialMedia.Name = "lblSocialMedia";
            this.lblSocialMedia.Size = new System.Drawing.Size(130, 17);
            this.lblSocialMedia.TabIndex = 1;
            this.lblSocialMedia.Text = "Social Media Search";
            // 
            // pnlSunbizsearch
            // 
            this.pnlSunbizsearch.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pnlSunbizsearch.Controls.Add(this.radSunbizGroupBox);
            this.pnlSunbizsearch.Controls.Add(this.lblSunbizSearch);
            this.pnlSunbizsearch.Controls.Add(this.radGrpSunbizSearchNew);
            this.pnlSunbizsearch.Location = new System.Drawing.Point(50, 367);
            this.pnlSunbizsearch.Name = "pnlSunbizsearch";
            // 
            // 
            // 
            this.pnlSunbizsearch.RootElement.AccessibleDescription = null;
            this.pnlSunbizsearch.RootElement.AccessibleName = null;
            this.pnlSunbizsearch.RootElement.ControlBounds = new System.Drawing.Rectangle(50, 367, 200, 100);
            this.pnlSunbizsearch.Size = new System.Drawing.Size(695, 115);
            this.pnlSunbizsearch.TabIndex = 63;
            this.pnlSunbizsearch.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radSunbizGroupBox
            // 
            this.radSunbizGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radSunbizGroupBox.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.radSunbizGroupBox.Controls.Add(this.cmbSunbizCountyList);
            this.radSunbizGroupBox.HeaderText = "Select County";
            this.radSunbizGroupBox.Location = new System.Drawing.Point(9, 46);
            this.radSunbizGroupBox.Name = "radSunbizGroupBox";
            // 
            // 
            // 
            this.radSunbizGroupBox.RootElement.AccessibleDescription = null;
            this.radSunbizGroupBox.RootElement.AccessibleName = null;
            this.radSunbizGroupBox.RootElement.ControlBounds = new System.Drawing.Rectangle(9, 46, 200, 100);
            this.radSunbizGroupBox.Size = new System.Drawing.Size(206, 54);
            this.radSunbizGroupBox.TabIndex = 12;
            this.radSunbizGroupBox.Text = "Select County";
            this.radSunbizGroupBox.Click += new System.EventHandler(this.radSunbizGroupBox_Click_1);
            // 
            // cmbSunbizCountyList
            // 
            this.cmbSunbizCountyList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cmbSunbizCountyList.Location = new System.Drawing.Point(13, 27);
            this.cmbSunbizCountyList.Name = "cmbSunbizCountyList";
            // 
            // 
            // 
            this.cmbSunbizCountyList.RootElement.AccessibleDescription = null;
            this.cmbSunbizCountyList.RootElement.AccessibleName = null;
            this.cmbSunbizCountyList.RootElement.ControlBounds = new System.Drawing.Rectangle(13, 27, 125, 20);
            this.cmbSunbizCountyList.RootElement.StretchVertically = true;
            this.cmbSunbizCountyList.Size = new System.Drawing.Size(179, 20);
            this.cmbSunbizCountyList.TabIndex = 10;
            this.cmbSunbizCountyList.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.cmbSunbizCountyList_SelectedIndexChanged);
            // 
            // lblSunbizSearch
            // 
            this.lblSunbizSearch.AutoSize = true;
            this.lblSunbizSearch.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSunbizSearch.Location = new System.Drawing.Point(291, 17);
            this.lblSunbizSearch.Name = "lblSunbizSearch";
            this.lblSunbizSearch.Size = new System.Drawing.Size(93, 17);
            this.lblSunbizSearch.TabIndex = 1;
            this.lblSunbizSearch.Text = "Sunbiz Search";
            // 
            // radGrpSunbizSearchNew
            // 
            this.radGrpSunbizSearchNew.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGrpSunbizSearchNew.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGrpSunbizSearchNew.Controls.Add(this.radSearchByZipcode);
            this.radGrpSunbizSearchNew.Controls.Add(this.radSearchBySunbizCounty);
            this.radGrpSunbizSearchNew.HeaderText = "Search By";
            this.radGrpSunbizSearchNew.Location = new System.Drawing.Point(224, 46);
            this.radGrpSunbizSearchNew.Name = "radGrpSunbizSearchNew";
            // 
            // 
            // 
            this.radGrpSunbizSearchNew.RootElement.AccessibleDescription = null;
            this.radGrpSunbizSearchNew.RootElement.AccessibleName = null;
            this.radGrpSunbizSearchNew.RootElement.ControlBounds = new System.Drawing.Rectangle(224, 46, 200, 100);
            this.radGrpSunbizSearchNew.Size = new System.Drawing.Size(480, 58);
            this.radGrpSunbizSearchNew.TabIndex = 2;
            this.radGrpSunbizSearchNew.Text = "Search By";
            this.radGrpSunbizSearchNew.Click += new System.EventHandler(this.radGrpSunbizSearch_Click);
            // 
            // radSearchByZipcode
            // 
            this.radSearchByZipcode.AutoSize = false;
            this.radSearchByZipcode.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSearchByZipcode.Location = new System.Drawing.Point(8, 26);
            this.radSearchByZipcode.Name = "radSearchByZipcode";
            // 
            // 
            // 
            this.radSearchByZipcode.RootElement.AccessibleDescription = null;
            this.radSearchByZipcode.RootElement.AccessibleName = null;
            this.radSearchByZipcode.RootElement.ControlBounds = new System.Drawing.Rectangle(8, 26, 100, 18);
            this.radSearchByZipcode.TabIndex = 0;
            this.radSearchByZipcode.Text = "Active";
            this.radSearchByZipcode.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radSearchByZipcode_ToggleStateChanged);
            // 
            // radSearchBySunbizCounty
            // 
            this.radSearchBySunbizCounty.AutoSize = false;
            this.radSearchBySunbizCounty.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSearchBySunbizCounty.Location = new System.Drawing.Point(142, 25);
            this.radSearchBySunbizCounty.Name = "radSearchBySunbizCounty";
            // 
            // 
            // 
            this.radSearchBySunbizCounty.RootElement.AccessibleDescription = null;
            this.radSearchBySunbizCounty.RootElement.AccessibleName = null;
            this.radSearchBySunbizCounty.RootElement.ControlBounds = new System.Drawing.Rectangle(142, 25, 100, 18);
            this.radSearchBySunbizCounty.Size = new System.Drawing.Size(80, 18);
            this.radSearchBySunbizCounty.TabIndex = 1;
            this.radSearchBySunbizCounty.Text = "Inactive";
            // 
            // radSocialGroupBox
            // 
            this.radSocialGroupBox.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radSocialGroupBox.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.radSocialGroupBox.Controls.Add(this.cmbSocialmediaCountyList);
            this.radSocialGroupBox.HeaderText = "Select County";
            this.radSocialGroupBox.Location = new System.Drawing.Point(9, 46);
            this.radSocialGroupBox.Name = "radSocialGroupBox";
            // 
            // 
            // 
            this.radSocialGroupBox.RootElement.AccessibleDescription = null;
            this.radSocialGroupBox.RootElement.AccessibleName = null;
            this.radSocialGroupBox.RootElement.ControlBounds = new System.Drawing.Rectangle(9, 46, 200, 100);
            this.radSocialGroupBox.Size = new System.Drawing.Size(206, 54);
            this.radSocialGroupBox.TabIndex = 12;
            this.radSocialGroupBox.Text = "Select County";
            this.radSocialGroupBox.Click += new System.EventHandler(this.radSocialGroupBox_Click_1);
            // 
            // cmbSocialmediaCountyList
            // 
            this.cmbSocialmediaCountyList.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cmbSocialmediaCountyList.Location = new System.Drawing.Point(13, 27);
            this.cmbSocialmediaCountyList.Name = "cmbSocialmediaCountyList";
            // 
            // 
            // 
            this.cmbSocialmediaCountyList.RootElement.AccessibleDescription = null;
            this.cmbSocialmediaCountyList.RootElement.AccessibleName = null;
            this.cmbSocialmediaCountyList.RootElement.ControlBounds = new System.Drawing.Rectangle(13, 27, 125, 20);
            this.cmbSocialmediaCountyList.RootElement.StretchVertically = true;
            this.cmbSocialmediaCountyList.Size = new System.Drawing.Size(179, 20);
            this.cmbSocialmediaCountyList.TabIndex = 10;
            // 
            // radGrpSunbizSearch
            // 
            this.radGrpSunbizSearch.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGrpSunbizSearch.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGrpSunbizSearch.Controls.Add(this.radSunbizName);
            this.radGrpSunbizSearch.Controls.Add(this.radSearchBySunbizAddress);
            this.radGrpSunbizSearch.HeaderText = "Search By";
            this.radGrpSunbizSearch.Location = new System.Drawing.Point(224, 46);
            this.radGrpSunbizSearch.Name = "radGrpSunbizSearch";
            // 
            // 
            // 
            this.radGrpSunbizSearch.RootElement.AccessibleDescription = null;
            this.radGrpSunbizSearch.RootElement.AccessibleName = null;
            this.radGrpSunbizSearch.RootElement.ControlBounds = new System.Drawing.Rectangle(224, 46, 200, 100);
            this.radGrpSunbizSearch.Size = new System.Drawing.Size(480, 58);
            this.radGrpSunbizSearch.TabIndex = 2;
            this.radGrpSunbizSearch.Text = "Search By";
            this.radGrpSunbizSearch.Click += new System.EventHandler(this.radGrpSunbizSearch_Click);
            // 
            // radSunbizName
            // 
            this.radSunbizName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSunbizName.Location = new System.Drawing.Point(8, 26);
            this.radSunbizName.Name = "radSunbizName";
            // 
            // 
            // 
            this.radSunbizName.RootElement.AccessibleDescription = null;
            this.radSunbizName.RootElement.AccessibleName = null;
            this.radSunbizName.RootElement.ControlBounds = new System.Drawing.Rectangle(8, 26, 110, 18);
            this.radSunbizName.RootElement.StretchHorizontally = true;
            this.radSunbizName.RootElement.StretchVertically = true;
            this.radSunbizName.Size = new System.Drawing.Size(100, 18);
            this.radSunbizName.TabIndex = 1;
            this.radSunbizName.Text = "LinkedIn";
            // 
            // radSearchBySunbizAddress
            // 
            this.radSearchBySunbizAddress.AutoSize = false;
            this.radSearchBySunbizAddress.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radSearchBySunbizAddress.Location = new System.Drawing.Point(142, 25);
            this.radSearchBySunbizAddress.Name = "radSearchBySunbizAddress";
            // 
            // 
            // 
            this.radSearchBySunbizAddress.RootElement.AccessibleDescription = null;
            this.radSearchBySunbizAddress.RootElement.AccessibleName = null;
            this.radSearchBySunbizAddress.RootElement.ControlBounds = new System.Drawing.Rectangle(142, 25, 110, 18);
            this.radSearchBySunbizAddress.Size = new System.Drawing.Size(80, 18);
            this.radSearchBySunbizAddress.TabIndex = 1;
            this.radSearchBySunbizAddress.Text = "Twitter";
            this.radSearchBySunbizAddress.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.radSearchBySunbizAddress_ToggleStateChanged);
            // 
            // radGrpFSBOSearchCategory
            // 
            this.radGrpFSBOSearchCategory.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGrpFSBOSearchCategory.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.radGrpFSBOSearchCategory.Controls.Add(this.cmbFSBOSearchCategory);
            this.radGrpFSBOSearchCategory.HeaderText = "Select By";
            this.radGrpFSBOSearchCategory.Location = new System.Drawing.Point(9, 46);
            this.radGrpFSBOSearchCategory.Name = "radGrpFSBOSearchCategory";
            // 
            // 
            // 
            this.radGrpFSBOSearchCategory.RootElement.AccessibleDescription = null;
            this.radGrpFSBOSearchCategory.RootElement.AccessibleName = null;
            this.radGrpFSBOSearchCategory.RootElement.ControlBounds = new System.Drawing.Rectangle(9, 46, 200, 100);
            this.radGrpFSBOSearchCategory.Size = new System.Drawing.Size(350, 54);
            this.radGrpFSBOSearchCategory.TabIndex = 12;
            this.radGrpFSBOSearchCategory.Text = "Select By";
            // 
            // cmbFSBOSearchCategory
            // 
            this.cmbFSBOSearchCategory.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cmbFSBOSearchCategory.Location = new System.Drawing.Point(13, 27);
            this.cmbFSBOSearchCategory.Name = "cmbFSBOSearchCategory";
            // 
            // 
            // 
            this.cmbFSBOSearchCategory.RootElement.AccessibleDescription = null;
            this.cmbFSBOSearchCategory.RootElement.AccessibleName = null;
            this.cmbFSBOSearchCategory.RootElement.ControlBounds = new System.Drawing.Rectangle(13, 27, 125, 20);
            this.cmbFSBOSearchCategory.RootElement.StretchVertically = true;
            this.cmbFSBOSearchCategory.Size = new System.Drawing.Size(330, 20);
            this.cmbFSBOSearchCategory.TabIndex = 10;
            // 
            // pnlFSBOsearch
            // 
            this.pnlFSBOsearch.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pnlFSBOsearch.Controls.Add(this.lblFSBOSearch);
            this.pnlFSBOsearch.Location = new System.Drawing.Point(50, 367);
            this.pnlFSBOsearch.Name = "pnlFSBOsearch";
            // 
            // 
            // 
            this.pnlFSBOsearch.RootElement.AccessibleDescription = null;
            this.pnlFSBOsearch.RootElement.AccessibleName = null;
            this.pnlFSBOsearch.RootElement.ControlBounds = new System.Drawing.Rectangle(50, 367, 200, 100);
            this.pnlFSBOsearch.Size = new System.Drawing.Size(695, 115);
            this.pnlFSBOsearch.TabIndex = 63;
            this.pnlFSBOsearch.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlSocialmedia
            // 
            this.pnlSocialmedia.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.pnlSocialmedia.Controls.Add(this.radSocialGroupBox);
            this.pnlSocialmedia.Controls.Add(this.lblSocialMedia);
            this.pnlSocialmedia.Controls.Add(this.radGrpSunbizSearch);
            this.pnlSocialmedia.Location = new System.Drawing.Point(50, 367);
            this.pnlSocialmedia.Name = "pnlSocialmedia";
            // 
            // 
            // 
            this.pnlSocialmedia.RootElement.AccessibleDescription = null;
            this.pnlSocialmedia.RootElement.AccessibleName = null;
            this.pnlSocialmedia.RootElement.ControlBounds = new System.Drawing.Rectangle(50, 367, 200, 100);
            this.pnlSocialmedia.Size = new System.Drawing.Size(695, 115);
            this.pnlSocialmedia.TabIndex = 63;
            this.pnlSocialmedia.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radGrpFSBOSearch
            // 
            this.radGrpFSBOSearch.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGrpFSBOSearch.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGrpFSBOSearch.HeaderText = "";
            this.radGrpFSBOSearch.Location = new System.Drawing.Point(0, 0);
            this.radGrpFSBOSearch.Name = "radGrpFSBOSearch";
            // 
            // 
            // 
            this.radGrpFSBOSearch.RootElement.AccessibleDescription = null;
            this.radGrpFSBOSearch.RootElement.AccessibleName = null;
            this.radGrpFSBOSearch.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 200, 100);
            this.radGrpFSBOSearch.TabIndex = 0;
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.AccessibleDescription = "Palm Beach Search";
            this.radMenuItem2.AccessibleName = "Palm Beach Search";
            this.radMenuItem2.AngleTransform = 90F;
            this.radMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("radMenuItem2.Image")));
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.Padding = new System.Windows.Forms.Padding(20, 1, 8, 1);
            this.radMenuItem2.RightToLeft = false;
            this.radMenuItem2.Text = "Palm Beach Search";
            this.radMenuItem2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.radMenuItem2.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.radMenuItem2.TextOrientation = System.Windows.Forms.Orientation.Vertical;
            this.radMenuItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CompanyName,
            this.Address,
            this.Officer,
            this.Zipcode});
            this.dataGridView1.Location = new System.Drawing.Point(22, 13);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(530, 192);
            this.dataGridView1.TabIndex = 64;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_CellMouseDown);
            this.dataGridView1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridView1_KeyDown_1);
            this.dataGridView1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataGridView1_KeyPress);
            // 
            // CompanyName
            // 
            this.CompanyName.HeaderText = "Company Name";
            this.CompanyName.Name = "CompanyName";
            this.CompanyName.Width = 120;
            // 
            // Address
            // 
            this.Address.HeaderText = "Address";
            this.Address.Name = "Address";
            this.Address.Width = 180;
            // 
            // Officer
            // 
            this.Officer.HeaderText = "Officer";
            this.Officer.Name = "Officer";
            this.Officer.Width = 120;
            // 
            // Zipcode
            // 
            this.Zipcode.HeaderText = "Zipcode";
            this.Zipcode.Name = "Zipcode";
            this.Zipcode.Width = 120;
            // 
            // groupbox1
            // 
            this.groupbox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupbox1.Controls.Add(this.dataGridView1);
            this.groupbox1.Location = new System.Drawing.Point(238, 285);
            this.groupbox1.Name = "groupbox1";
            this.groupbox1.Size = new System.Drawing.Size(566, 230);
            this.groupbox1.TabIndex = 65;
            this.groupbox1.TabStop = false;
            // 
            // txtboxEmailID
            // 
            this.txtboxEmailID.Location = new System.Drawing.Point(280, 145);
            this.txtboxEmailID.Name = "txtboxEmailID";
            this.txtboxEmailID.Size = new System.Drawing.Size(165, 20);
            this.txtboxEmailID.TabIndex = 66;
            this.txtboxEmailID.TextChanged += new System.EventHandler(this.txtboxEmailID_TextChanged);
            // 
            // txtboxPasswrd
            // 
            this.txtboxPasswrd.Location = new System.Drawing.Point(280, 173);
            this.txtboxPasswrd.Name = "txtboxPasswrd";
            this.txtboxPasswrd.Size = new System.Drawing.Size(165, 20);
            this.txtboxPasswrd.TabIndex = 67;
            this.txtboxPasswrd.TextChanged += new System.EventHandler(this.txtboxPasswrd_TextChanged);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(142, 145);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(76, 13);
            this.lblEmail.TabIndex = 68;
            this.lblEmail.Text = "LinkedIn Email";
            // 
            // lblpassword
            // 
            this.lblpassword.AutoSize = true;
            this.lblpassword.Location = new System.Drawing.Point(144, 179);
            this.lblpassword.Name = "lblpassword";
            this.lblpassword.Size = new System.Drawing.Size(97, 13);
            this.lblpassword.TabIndex = 69;
            this.lblpassword.Text = "LinkedIn Password";
            this.lblpassword.Click += new System.EventHandler(this.lblpassword_Click);
            // 
            // lblTwitterPassword
            // 
            this.lblTwitterPassword.AutoSize = true;
            this.lblTwitterPassword.Location = new System.Drawing.Point(144, 234);
            this.lblTwitterPassword.Name = "lblTwitterPassword";
            this.lblTwitterPassword.Size = new System.Drawing.Size(88, 13);
            this.lblTwitterPassword.TabIndex = 73;
            this.lblTwitterPassword.Text = "Twitter Password";
            // 
            // lblTwitterEmail
            // 
            this.lblTwitterEmail.AutoSize = true;
            this.lblTwitterEmail.Location = new System.Drawing.Point(142, 202);
            this.lblTwitterEmail.Name = "lblTwitterEmail";
            this.lblTwitterEmail.Size = new System.Drawing.Size(67, 13);
            this.lblTwitterEmail.TabIndex = 72;
            this.lblTwitterEmail.Text = "Twitter Email";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(278, 228);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(167, 20);
            this.textBox1.TabIndex = 71;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(278, 202);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(167, 20);
            this.textBox2.TabIndex = 70;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.button1.Location = new System.Drawing.Point(508, 223);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 34);
            this.button1.TabIndex = 74;
            this.button1.Text = "Refresh";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnBeginSunbizSearch
            // 
            this.btnBeginSunbizSearch.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnBeginSunbizSearch.Location = new System.Drawing.Point(632, 223);
            this.btnBeginSunbizSearch.Name = "btnBeginSunbizSearch";
            this.btnBeginSunbizSearch.Size = new System.Drawing.Size(92, 34);
            this.btnBeginSunbizSearch.TabIndex = 75;
            this.btnBeginSunbizSearch.Text = "Begin Search";
            this.btnBeginSunbizSearch.UseVisualStyleBackColor = false;
            this.btnBeginSunbizSearch.Click += new System.EventHandler(this.btnBeginSunbizSearch_Click_1);
            // 
            // btnSocialmediasearch
            // 
            this.btnSocialmediasearch.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnSocialmediasearch.Location = new System.Drawing.Point(632, 223);
            this.btnSocialmediasearch.Name = "btnSocialmediasearch";
            this.btnSocialmediasearch.Size = new System.Drawing.Size(90, 34);
            this.btnSocialmediasearch.TabIndex = 77;
            this.btnSocialmediasearch.Text = "Begin Search";
            this.btnSocialmediasearch.UseVisualStyleBackColor = false;
            this.btnSocialmediasearch.Click += new System.EventHandler(this.btnSocialmediasearch_click);
            // 
            // btnRefreshSocialmediasearch
            // 
            this.btnRefreshSocialmediasearch.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnRefreshSocialmediasearch.Location = new System.Drawing.Point(508, 223);
            this.btnRefreshSocialmediasearch.Name = "btnRefreshSocialmediasearch";
            this.btnRefreshSocialmediasearch.Size = new System.Drawing.Size(118, 34);
            this.btnRefreshSocialmediasearch.TabIndex = 76;
            this.btnRefreshSocialmediasearch.Text = "Refresh";
            this.btnRefreshSocialmediasearch.UseVisualStyleBackColor = false;
            this.btnRefreshSocialmediasearch.Click += new System.EventHandler(this.btnRefreshSocialmediasearch_Click);
            // 
            // btnfsbosearch
            // 
            this.btnfsbosearch.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnfsbosearch.Location = new System.Drawing.Point(633, 223);
            this.btnfsbosearch.Name = "btnfsbosearch";
            this.btnfsbosearch.Size = new System.Drawing.Size(79, 34);
            this.btnfsbosearch.TabIndex = 77;
            this.btnfsbosearch.Text = "Begin Search";
            this.btnfsbosearch.UseVisualStyleBackColor = false;
            this.btnfsbosearch.Click += new System.EventHandler(this.btnfsbosearch_Click);
            // 
            // btnfsboRefresh
            // 
            this.btnfsboRefresh.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnfsboRefresh.Location = new System.Drawing.Point(538, 223);
            this.btnfsboRefresh.Name = "btnfsboRefresh";
            this.btnfsboRefresh.Size = new System.Drawing.Size(92, 34);
            this.btnfsboRefresh.TabIndex = 76;
            this.btnfsboRefresh.Text = "Refresh";
            this.btnfsboRefresh.UseVisualStyleBackColor = false;
            this.btnfsboRefresh.Click += new System.EventHandler(this.btnfsboRefresh_Click);
            // 
            // groupFSBO
            // 
            this.groupFSBO.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupFSBO.Controls.Add(this.cmbFSBOSearchCategopry);
            this.groupFSBO.Location = new System.Drawing.Point(327, 79);
            this.groupFSBO.Name = "groupFSBO";
            this.groupFSBO.Size = new System.Drawing.Size(160, 51);
            this.groupFSBO.TabIndex = 78;
            this.groupFSBO.TabStop = false;
            this.groupFSBO.Text = "Search By";
            // 
            // cmbFSBOSearchCategopry
            // 
            this.cmbFSBOSearchCategopry.FormattingEnabled = true;
            this.cmbFSBOSearchCategopry.Items.AddRange(new object[] {
            "--Select--",
            "Redfin",
            "Zillow",
            "Loopnet"});
            this.cmbFSBOSearchCategopry.Location = new System.Drawing.Point(6, 18);
            this.cmbFSBOSearchCategopry.Name = "cmbFSBOSearchCategopry";
            this.cmbFSBOSearchCategopry.Size = new System.Drawing.Size(148, 21);
            this.cmbFSBOSearchCategopry.TabIndex = 0;
            // 
            // radFSBOSearchGroupBox1
            // 
            this.radFSBOSearchGroupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radFSBOSearchGroupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.radFSBOSearchGroupBox1.Controls.Add(this.cmbFSBOSearchCountyList1);
            this.radFSBOSearchGroupBox1.Location = new System.Drawing.Point(157, 79);
            this.radFSBOSearchGroupBox1.Name = "radFSBOSearchGroupBox1";
            this.radFSBOSearchGroupBox1.Size = new System.Drawing.Size(161, 51);
            this.radFSBOSearchGroupBox1.TabIndex = 79;
            this.radFSBOSearchGroupBox1.TabStop = false;
            this.radFSBOSearchGroupBox1.Text = "Search County";
            // 
            // cmbFSBOSearchCountyList1
            // 
            this.cmbFSBOSearchCountyList1.FormattingEnabled = true;
            this.cmbFSBOSearchCountyList1.Items.AddRange(new object[] {
            "--Select--",
            "Broward",
            "Palm Beach"});
            this.cmbFSBOSearchCountyList1.Location = new System.Drawing.Point(6, 18);
            this.cmbFSBOSearchCountyList1.Name = "cmbFSBOSearchCountyList1";
            this.cmbFSBOSearchCountyList1.Size = new System.Drawing.Size(146, 21);
            this.cmbFSBOSearchCountyList1.TabIndex = 0;
            // 
            // grpbsingleaddress
            // 
            this.grpbsingleaddress.Controls.Add(this.richsingleaddress);
            this.grpbsingleaddress.Controls.Add(this.lblsingleaddress);
            this.grpbsingleaddress.Location = new System.Drawing.Point(163, 161);
            this.grpbsingleaddress.Name = "grpbsingleaddress";
            this.grpbsingleaddress.Size = new System.Drawing.Size(375, 86);
            this.grpbsingleaddress.TabIndex = 80;
            this.grpbsingleaddress.TabStop = false;
            this.grpbsingleaddress.Text = "Single Address";
            // 
            // richsingleaddress
            // 
            this.richsingleaddress.Location = new System.Drawing.Point(115, 18);
            this.richsingleaddress.Name = "richsingleaddress";
            this.richsingleaddress.Size = new System.Drawing.Size(242, 51);
            this.richsingleaddress.TabIndex = 1;
            this.richsingleaddress.Text = "";
            // 
            // lblsingleaddress
            // 
            this.lblsingleaddress.AutoSize = true;
            this.lblsingleaddress.Location = new System.Drawing.Point(12, 28);
            this.lblsingleaddress.Name = "lblsingleaddress";
            this.lblsingleaddress.Size = new System.Drawing.Size(77, 13);
            this.lblsingleaddress.TabIndex = 0;
            this.lblsingleaddress.Text = "Single Address";
            // 
            // dgvwbulkAddress
            // 
            this.dgvwbulkAddress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwbulkAddress.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BulkAddress});
            this.dgvwbulkAddress.Location = new System.Drawing.Point(192, 254);
            this.dgvwbulkAddress.Name = "dgvwbulkAddress";
            this.dgvwbulkAddress.Size = new System.Drawing.Size(582, 184);
            this.dgvwbulkAddress.TabIndex = 81;
            this.dgvwbulkAddress.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwbulkAddress_CellClick);
            // 
            // BulkAddress
            // 
            this.BulkAddress.HeaderText = "Bulk Address";
            this.BulkAddress.Name = "BulkAddress";
            this.BulkAddress.Width = 600;
            // 
            // grpbEntries
            // 
            this.grpbEntries.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.grpbEntries.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.grpbEntries.Controls.Add(this.cmbEntries);
            this.grpbEntries.Location = new System.Drawing.Point(502, 79);
            this.grpbEntries.Name = "grpbEntries";
            this.grpbEntries.Size = new System.Drawing.Size(161, 51);
            this.grpbEntries.TabIndex = 82;
            this.grpbEntries.TabStop = false;
            this.grpbEntries.Text = "Search Entries";
            // 
            // cmbEntries
            // 
            this.cmbEntries.FormattingEnabled = true;
            this.cmbEntries.Items.AddRange(new object[] {
            "--select--",
            "Last 50",
            "Last 100",
            "Last 500",
            "Last 1000",
            "All"});
            this.cmbEntries.Location = new System.Drawing.Point(6, 18);
            this.cmbEntries.Name = "cmbEntries";
            this.cmbEntries.Size = new System.Drawing.Size(143, 21);
            this.cmbEntries.TabIndex = 0;
            // 
            // cmbsearchdays
            // 
            this.cmbsearchdays.FormattingEnabled = true;
            this.cmbsearchdays.Items.AddRange(new object[] {
            "--select--",
            "7 days",
            "Month",
            "6 Month",
            "Year"});
            this.cmbsearchdays.Location = new System.Drawing.Point(6, 18);
            this.cmbsearchdays.Name = "cmbsearchdays";
            this.cmbsearchdays.Size = new System.Drawing.Size(130, 21);
            this.cmbsearchdays.TabIndex = 0;
            this.cmbsearchdays.SelectedIndexChanged += new System.EventHandler(this.cmbsearchdays_SelectedIndexChanged);
            // 
            // grpbSearchdays
            // 
            this.grpbSearchdays.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.grpbSearchdays.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.grpbSearchdays.Controls.Add(this.cmbsearchdays);
            this.grpbSearchdays.Location = new System.Drawing.Point(672, 79);
            this.grpbSearchdays.Name = "grpbSearchdays";
            this.grpbSearchdays.Size = new System.Drawing.Size(146, 51);
            this.grpbSearchdays.TabIndex = 83;
            this.grpbSearchdays.TabStop = false;
            this.grpbSearchdays.Text = "Search Days";
            // 
            // grpbdatefromto
            // 
            this.grpbdatefromto.Controls.Add(this.dtmfsbodateto);
            this.grpbdatefromto.Controls.Add(this.dtmfsbodatefrom);
            this.grpbdatefromto.Controls.Add(this.lblfsbodateto);
            this.grpbdatefromto.Controls.Add(this.lblfsbodatefrom);
            this.grpbdatefromto.Location = new System.Drawing.Point(157, 136);
            this.grpbdatefromto.Name = "grpbdatefromto";
            this.grpbdatefromto.Size = new System.Drawing.Size(661, 50);
            this.grpbdatefromto.TabIndex = 84;
            this.grpbdatefromto.TabStop = false;
            this.grpbdatefromto.Text = "Date Filter";
            // 
            // dtmfsbodateto
            // 
            this.dtmfsbodateto.Enabled = false;
            this.dtmfsbodateto.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtmfsbodateto.Location = new System.Drawing.Point(402, 19);
            this.dtmfsbodateto.Name = "dtmfsbodateto";
            this.dtmfsbodateto.Size = new System.Drawing.Size(249, 20);
            this.dtmfsbodateto.TabIndex = 4;
            // 
            // dtmfsbodatefrom
            // 
            this.dtmfsbodatefrom.Enabled = false;
            this.dtmfsbodatefrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtmfsbodatefrom.Location = new System.Drawing.Point(74, 19);
            this.dtmfsbodatefrom.Name = "dtmfsbodatefrom";
            this.dtmfsbodatefrom.Size = new System.Drawing.Size(250, 20);
            this.dtmfsbodatefrom.TabIndex = 3;
            // 
            // lblfsbodateto
            // 
            this.lblfsbodateto.AutoSize = true;
            this.lblfsbodateto.Location = new System.Drawing.Point(346, 17);
            this.lblfsbodateto.Name = "lblfsbodateto";
            this.lblfsbodateto.Size = new System.Drawing.Size(46, 13);
            this.lblfsbodateto.TabIndex = 2;
            this.lblfsbodateto.Text = "Date To";
            // 
            // lblfsbodatefrom
            // 
            this.lblfsbodatefrom.AutoSize = true;
            this.lblfsbodatefrom.Location = new System.Drawing.Point(11, 20);
            this.lblfsbodatefrom.Name = "lblfsbodatefrom";
            this.lblfsbodatefrom.Size = new System.Drawing.Size(56, 13);
            this.lblfsbodatefrom.TabIndex = 0;
            this.lblfsbodatefrom.Text = "Date From";
            // 
            // btnResum
            // 
            this.btnResum.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.btnResum.Location = new System.Drawing.Point(716, 224);
            this.btnResum.Name = "btnResum";
            this.btnResum.Size = new System.Drawing.Size(102, 33);
            this.btnResum.TabIndex = 85;
            this.btnResum.Text = "Resume Search";
            this.btnResum.UseVisualStyleBackColor = false;
            this.btnResum.Click += new System.EventHandler(this.btnResum_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(865, 625);
            this.Controls.Add(this.btnResum);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.grpbdatefromto);
            this.Controls.Add(this.grpbSearchdays);
            this.Controls.Add(this.btnBeginSunbizSearch);
            this.Controls.Add(this.btnRefreshSocialmediasearch);
            this.Controls.Add(this.btnSocialmediasearch);
            this.Controls.Add(this.grpbEntries);
            this.Controls.Add(this.dgvwbulkAddress);
            this.Controls.Add(this.grpbsingleaddress);
            this.Controls.Add(this.radFSBOSearchGroupBox1);
            this.Controls.Add(this.groupFSBO);
            this.Controls.Add(this.btnfsbosearch);
            this.Controls.Add(this.btnfsboRefresh);
            this.Controls.Add(this.lblTwitterPassword);
            this.Controls.Add(this.lblTwitterEmail);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.lblpassword);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.txtboxPasswrd);
            this.Controls.Add(this.txtboxEmailID);
            this.Controls.Add(this.groupbox1);
            this.Controls.Add(this.pnlSpokeoSearch);
            this.Controls.Add(this.pnlSunbizsearch);
            this.Controls.Add(this.pnlFSBOsearch);
            this.Controls.Add(this.pnlSocialmedia);
            this.Controls.Add(this.lblInternetConnection);
            this.Controls.Add(this.lblselectCategory);
            this.Controls.Add(this.radDropDownList1);
            this.Controls.Add(this.BusinessNamePanel);
            this.Controls.Add(this.PartyPanel);
            this.Controls.Add(this.SelectCategoryMenu);
            this.Controls.Add(this.CitationPanel);
            this.Controls.Add(this.CasePanel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblTab);
            this.Controls.Add(this.PalmBeachPanel);
            this.Controls.Add(this.pnlGeneral);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shark Property Records";
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessNamePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBeginSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBusinessName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertysearchPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwsunbizgrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCompleteAddressPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostDirection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreeCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetDirection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radForBulkCase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radforLPcase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPropertyAddresss)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PropertSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbtnFolioNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbtnCompleteAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbtnIndividualsearchBroward)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartyPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMiddleName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PartySearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPartyDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownParty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dailySearchPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DDSearchDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DefendantSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblError1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DDDocumentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDocType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblEnddate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBeginDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CasePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanelBulk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCaseSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCaseNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioBulk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRadioSingle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CitationPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCitationSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCitationNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblselectCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PalmBeachPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblpalmpending)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPCBPalmDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PalmSelectCorrectAddreess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDDlDisplayRecords)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDDlTotalRecord)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PalmBeachsrch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPalmDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPalmDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGeneral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlBulk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSearchAllCasesAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBulkCases)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLPcases)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlsearchByFolio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFolioNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnScheduledSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gbSelectCounty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCountyList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSearchByAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitNumber1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostDirection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetNumber1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreeCity1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetType1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetDirection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStreetName1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnManualPropSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlsearchspokeosingleaddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtspokeosingleaddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectCategoryMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSpokeoSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSpokeoCountyList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnBeginSpokeoSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGrpSearchByPerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radAllPerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSearchByBulkPerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSearchBySinglePerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGrpSearchByAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radAllAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSearchByOneAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSearchByBulkAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSunbizsearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSunbizGroupBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSunbizCountyList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGrpSunbizSearchNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSearchByZipcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSearchBySunbizCounty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSocialGroupBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSocialmediaCountyList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGrpSunbizSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSunbizName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSearchBySunbizAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGrpFSBOSearchCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFSBOSearchCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlFSBOsearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSocialmedia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGrpFSBOSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupbox1.ResumeLayout(false);
            this.groupFSBO.ResumeLayout(false);
            this.radFSBOSearchGroupBox1.ResumeLayout(false);
            this.grpbsingleaddress.ResumeLayout(false);
            this.grpbsingleaddress.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwbulkAddress)).EndInit();
            this.grpbEntries.ResumeLayout(false);
            this.grpbSearchdays.ResumeLayout(false);
            this.grpbdatefromto.ResumeLayout(false);
            this.grpbdatefromto.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void CitationPanel_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            throw new System.NotImplementedException();
        }
       
        #endregion

        private System.Windows.Forms.Label lblTab;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblCourtYpe;
        private System.Windows.Forms.Label lblBusinessNameTitle;
        private System.Windows.Forms.Label lblBusinessName;
        private System.Windows.Forms.Label lbldatefrom;
        private System.Windows.Forms.Label lblDateTo;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList1;
        private Telerik.WinControls.UI.RadPanel BusinessNamePanel;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList2;
        private Telerik.WinControls.UI.RadDateTimePicker txtDateFrom;
        private Telerik.WinControls.UI.RadTextBox txtBusinessName;
        private Telerik.WinControls.UI.RadDateTimePicker txtdateTo;
        private Telerik.WinControls.UI.RadButton btnBeginSearch;
        private Telerik.WinControls.UI.RadPanel PartyPanel;
        private Telerik.WinControls.UI.RadButton PartySearch;
        private Telerik.WinControls.UI.RadDateTimePicker txtPartyDateTo;
        private Telerik.WinControls.UI.RadDateTimePicker txtPartyDateFrom;
        private Telerik.WinControls.UI.RadTextBox txtFirstName;
        private Telerik.WinControls.UI.RadDropDownList radDropDownParty;
        private System.Windows.Forms.Label lblPartNameTitle;
        private System.Windows.Forms.Label lblPartDateTo;
        private System.Windows.Forms.Label lblPartCourtType;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblPartDateFrom;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.Label lblMiddleName;
        private Telerik.WinControls.UI.RadTextBox txtLastName;
        private Telerik.WinControls.UI.RadTextBox txtMiddleName;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
        private System.Windows.Forms.Label lblcasenumber;
        private System.Windows.Forms.Label lblcaseNumberTitle;
        private Telerik.WinControls.UI.RadPanel CasePanel;
        private Telerik.WinControls.UI.RadTextBox radTextBox3;
        private Telerik.WinControls.UI.RadTextBox txtCaseNumber;
        private Telerik.WinControls.UI.RadButton btnCaseSearch;
        private Telerik.WinControls.UI.RadPanel CitationPanel;
        private Telerik.WinControls.UI.RadButton btnCitationSearch;
        private Telerik.WinControls.UI.RadTextBox txtCitationNumber;
        private System.Windows.Forms.Label lblCitationTitle;
        private System.Windows.Forms.Label lblCitationNumber;
        private Telerik.WinControls.UI.RadPanel dailySearchPanel;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadButton DefendantSearch;
        private Telerik.WinControls.UI.RadPanel PropertysearchPanel;
        private Telerik.WinControls.UI.RadButton PropertSearch;
        private Telerik.WinControls.UI.RadTextBox txtStreetNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
        private Telerik.WinControls.UI.RadMenuItem BussinessName;
        private Telerik.WinControls.UI.RadMenuItem PartyName;
        private Telerik.WinControls.UI.RadMenuItem CaseNumber;
        private Telerik.WinControls.UI.RadMenuItem CitationNumber;
        private Telerik.WinControls.UI.RadMenuItem radMenuGeneral;
        private Telerik.WinControls.UI.RadLabel lblselectCategory;
        private Telerik.WinControls.UI.RadMenuItem Home;
        private Telerik.WinControls.UI.RadTextBox txtStreetType;
        private Telerik.WinControls.UI.RadTextBox txtStreetName;
        private Telerik.WinControls.UI.RadTextBox txtStreetDirection;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private Telerik.WinControls.UI.RadTextBox txtStreeCity;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private Telerik.WinControls.UI.RadRadioButton radbtnFolioNumber;
        private Telerik.WinControls.UI.RadRadioButton radbtnCompleteAddress;
        private Telerik.WinControls.UI.RadRadioButton radbtnIndividualsearchBroward;
        private Telerik.WinControls.UI.RadPanel radCompleteAddressPanel;
        private System.Windows.Forms.Label lblpropertyAddress;
        private Telerik.WinControls.UI.RadTextBox txtPropertyAddresss;
        private Telerik.WinControls.UI.RadLabel lblEnddate;
        private Telerik.WinControls.UI.RadLabel lblBeginDate;
        private Telerik.WinControls.UI.RadDateTimePicker radEndDate;
        private Telerik.WinControls.UI.RadDateTimePicker radBeginDate;
        private Telerik.WinControls.UI.RadDropDownList DDDocumentType;
        private Telerik.WinControls.UI.RadLabel lblError;
        private Telerik.WinControls.UI.RadLabel lblError1;
        private Telerik.WinControls.UI.RadTextBox txtUnitNumber;
        private Telerik.WinControls.UI.RadTextBox txtPostDirection;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel lblDocType;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnsrchByLPAddress;

        private System.Windows.Forms.DataGridView dgvwsunbizgrid;

        private System.Windows.Forms.Button GetLpCaseForBroward;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button GettAddress;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnSaveAddress;
        private System.Windows.Forms.Label label20;
        private Telerik.WinControls.UI.RadRadioButton radRadioBulk;
        private Telerik.WinControls.UI.RadRadioButton radRadioSingle;
        private Telerik.WinControls.UI.RadPanel radPanelBulk;
        private System.Windows.Forms.Button btnMannualBulk;
        
        private System.Windows.Forms.TextBox txtBulkCase;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnAssignAddressForBulk;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private Telerik.WinControls.UI.RadRadioButton radForBulkCase;
        private Telerik.WinControls.UI.RadRadioButton radforLPcase;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button GetDetailsFromBCPA;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button GetDetailsForBulk;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private Telerik.WinControls.UI.RadDropDownList DDSearchDay;
        private Telerik.WinControls.UI.RadMenuItem radmenuBroward;
     
        private Telerik.WinControls.UI.RadPanel PalmBeachPanel;
        private System.Windows.Forms.LinkLabel link90Days;
        private System.Windows.Forms.LinkLabel link30Days;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private System.Windows.Forms.LinkLabel link7Days;
        private System.Windows.Forms.Label label37;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadDropDownList radDDlDisplayRecords;
        private Telerik.WinControls.UI.RadDropDownList radDDlTotalRecord;
        private Telerik.WinControls.UI.RadButton PalmBeachsrch;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private System.Windows.Forms.TextBox txtPalmDocType;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private System.Windows.Forms.Label label35;
        private Telerik.WinControls.UI.RadDateTimePicker radPalmDateFrom;
        private Telerik.WinControls.UI.RadDateTimePicker radPalmDateTo;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadButton PalmSelectCorrectAddreess;
        private System.Windows.Forms.LinkLabel linkRefreshPalmCases;
        private Telerik.WinControls.UI.RadLabel lblpalmpending;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadButton btnPCBPalmDetails;
        private System.Windows.Forms.LinkLabel ClearCacheDefandent;
        private System.Windows.Forms.LinkLabel linkBtnSpokioDetails;
        private System.Windows.Forms.LinkLabel lnkPalmSpokeoDetails;
        private System.Windows.Forms.Label lblInternetConnection;
        private Telerik.WinControls.UI.RadMenuItem radmenuPalmBeach;
        private Telerik.WinControls.UI.RadPanel pnlGeneral;
   
        private Telerik.WinControls.UI.RadDropDownList cmbCountyList;
        private Telerik.WinControls.UI.RadGroupBox gbSelectCounty;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.RootRadElement object_73c9a747_22cf_4f3a_a660_2813c219558a;
        private Telerik.WinControls.UI.RadRadioButton radBtnScheduledSearch;
        private Telerik.WinControls.UI.RadPanel pnlSearchByAddress;
        private Telerik.WinControls.UI.RadTextBox txtUnitNumber1;
        private Telerik.WinControls.UI.RadTextBox txtPostDirection1;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private Telerik.WinControls.UI.RadTextBox txtStreetNumber1;
        private Telerik.WinControls.UI.RadTextBox txtStreeCity1;
        private System.Windows.Forms.Label label46; 
        private System.Windows.Forms.Label lblFolioNumber;
        private System.Windows.Forms.Label lblspokeosingleaddress; 
        private Telerik.WinControls.UI.RadTextBox txtFolioNumber;
        private Telerik.WinControls.UI.RadTextBox txtspokeosingleaddress;
        private Telerik.WinControls.UI.RadTextBox txtStreetType1;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private Telerik.WinControls.UI.RadTextBox txtStreetDirection1;
        private System.Windows.Forms.Label label49;
        private Telerik.WinControls.UI.RadTextBox txtStreetName1; 
        private Telerik.WinControls.UI.RadPanel pnlsearchByFolio;
        private Telerik.WinControls.UI.RadPanel pnlsearchspokeosingleaddress;
      
        private Telerik.WinControls.UI.RadPanel pnlBulk;
        private System.Windows.Forms.Label lblPendingBulkCases;
        private System.Windows.Forms.Label label38;
        private Telerik.WinControls.UI.RadRadioButton radBulkCases;
        private Telerik.WinControls.UI.RadRadioButton radLPcases;
        private System.Windows.Forms.Label lblPendingLPcases;
        private System.Windows.Forms.Label label40;
        private Telerik.WinControls.UI.RadButton btnManualPropSearch;
        private Telerik.WinControls.UI.RadButton btnSearchAllCasesAddress;
        private Telerik.WinControls.UI.RadMenuItem radMenuMiamiDade;
        private Telerik.WinControls.UI.RadMenu SelectCategoryMenu;
        private Telerik.WinControls.UI.RadMenuItem radMenuSpokeoSearch;
        private Telerik.WinControls.UI.RadPanel pnlSpokeoSearch; 
        private Telerik.WinControls.UI.RadPanel pnlSunbizsearch;
        private Telerik.WinControls.UI.RadPanel pnlFSBOsearch;
        private Telerik.WinControls.UI.RadPanel pnlSocialmedia;
        private System.Windows.Forms.Label lblSpokeoSearch;
        private System.Windows.Forms.Label lblSunbizSearch;
        private System.Windows.Forms.Label lblFSBOSearch;
        private System.Windows.Forms.Label lblSocialMedia;
        private Telerik.WinControls.UI.RadGroupBox radGrpSearchByAddress;
        private Telerik.WinControls.UI.RadRadioButton radSearchByOneAddress;
        private Telerik.WinControls.UI.RadRadioButton radSearchByBulkAddress;
        private Telerik.WinControls.UI.RadGroupBox radGrpSearchByPerson;
        private Telerik.WinControls.UI.RadGroupBox radGrpSunbizSearch;
        private Telerik.WinControls.UI.RadGroupBox radGrpSunbizSearchNew;
        private Telerik.WinControls.UI.RadGroupBox radGrpFSBOSearchCategory;
        private Telerik.WinControls.UI.RadGroupBox radGrpFSBOSearch;
        private Telerik.WinControls.UI.RadRadioButton radSearchBySinglePerson;
        private Telerik.WinControls.UI.RadRadioButton radSearchByBulkPerson;
        private Telerik.WinControls.UI.RadButton btnBeginSpokeoSearch;
        
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGroupBox radSunbizGroupBox;
        private Telerik.WinControls.UI.RadGroupBox radSocialGroupBox;
      //  private Telerik.WinControls.UI.RadGroupBox radFSBOSearchGroupBox;

        private Telerik.WinControls.UI.RadDropDownList cmbSpokeoCountyList;
        private Telerik.WinControls.UI.RadDropDownList cmbSunbizCountyList;
        private Telerik.WinControls.UI.RadDropDownList cmbSocialmediaCountyList;
       // private Telerik.WinControls.UI.RadDropDownList cmbFSBOSearchCountyList; 
            private Telerik.WinControls.UI.RadDropDownList cmbFSBOSearchCategory;
        private Telerik.WinControls.UI.RadRadioButton radAllPerson;
        private Telerik.WinControls.UI.RadRadioButton radAllAddress;
        
        private Telerik.WinControls.UI.RadRadioButton radSunbizName;
        
        private Telerik.WinControls.UI.RadRadioButton radSearchBySunbizAddress;
        private Telerik.WinControls.UI.RadCheckBox radSearchByZipcode; 
        private Telerik.WinControls.UI.RadCheckBox radSearchBySunbizCounty;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
        private Telerik.WinControls.UI.RadMenuItem radMenuSunbizSearch; 
        private Telerik.WinControls.UI.RadMenuItem radMenuFSBOSearch;
        private Telerik.WinControls.UI.RadMenuItem radmenuSocialmediaSearch;
        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn CompanyName;
        private DataGridViewTextBoxColumn Address;
        private DataGridViewTextBoxColumn Officer;
        private DataGridViewTextBoxColumn Zipcode;
        private GroupBox groupbox1;
        private TextBox txtboxEmailID;
        private TextBox txtboxPasswrd;
        private Label lblEmail;
        private Label lblpassword;
        private Label lblTwitterPassword;
        private Label lblTwitterEmail;
        private TextBox textBox1;
        private TextBox textBox2;
        private Button button1;
        private Button btnBeginSunbizSearch;
        private Button btnfsbosearch;
        private Button btnSocialmediasearch;
        private Button btnRefreshSocialmediasearch;
        private Button btnfsboRefresh;
        private GroupBox groupFSBO;
        private ComboBox cmbFSBOSearchCategopry;
        private GroupBox radFSBOSearchGroupBox1;
        private ComboBox cmbFSBOSearchCountyList1;
        private GroupBox grpbsingleaddress;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private RichTextBox richsingleaddress;
        private Label lblsingleaddress;
        private DataGridView dgvwbulkAddress;
        private DataGridViewTextBoxColumn BulkAddress;
        private GroupBox grpbEntries;
        private ComboBox cmbEntries;
        private ComboBox cmbsearchdays;
        private GroupBox grpbSearchdays;
        private GroupBox grpbdatefromto;
        private Label lblfsbodateto;
        private Label lblfsbodatefrom;
        private DateTimePicker dtmfsbodateto;
        private DateTimePicker dtmfsbodatefrom;
        private Button btnResum;
    }
}

