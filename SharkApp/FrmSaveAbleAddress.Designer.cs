﻿namespace SharkApp
{
    partial class FrmSaveAbleAddress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GridSaveableAddress = new System.Windows.Forms.DataGridView();
            this.gridDisplayAddress = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.radSelectedAddressPanel = new Telerik.WinControls.UI.RadPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblType = new System.Windows.Forms.Label();
            this.lblHidenCaseNumberID = new System.Windows.Forms.Label();
            this.txtPalmAddress = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.radDdlSTDirection = new Telerik.WinControls.UI.RadDropDownList();
            this.lblHidenSrchDefendentID = new System.Windows.Forms.Label();
            this.SelectedAddressSave = new System.Windows.Forms.Button();
            this.txtUnitNo = new Telerik.WinControls.UI.RadTextBox();
            this.txtPostDir = new Telerik.WinControls.UI.RadTextBox();
            this.lblUnitNo = new System.Windows.Forms.Label();
            this.lblPostDir = new System.Windows.Forms.Label();
            this.lblstretNumber = new System.Windows.Forms.Label();
            this.txtStNum = new Telerik.WinControls.UI.RadTextBox();
            this.txtStCity = new Telerik.WinControls.UI.RadTextBox();
            this.lblStCity = new System.Windows.Forms.Label();
            this.txtStType = new Telerik.WinControls.UI.RadTextBox();
            this.lblstdirction = new System.Windows.Forms.Label();
            this.lblstType = new System.Windows.Forms.Label();
            this.lblstname = new System.Windows.Forms.Label();
            this.txtStName = new Telerik.WinControls.UI.RadTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblSrchType = new System.Windows.Forms.Label();
            this.dataGridOtherProperty = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.GridSaveableAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDisplayAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSelectedAddressPanel)).BeginInit();
            this.radSelectedAddressPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDdlSTDirection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostDir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOtherProperty)).BeginInit();
            this.SuspendLayout();
            // 
            // GridSaveableAddress
            // 
            this.GridSaveableAddress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridSaveableAddress.Location = new System.Drawing.Point(2, 41);
            this.GridSaveableAddress.Name = "GridSaveableAddress";
            this.GridSaveableAddress.Size = new System.Drawing.Size(631, 244);
            this.GridSaveableAddress.TabIndex = 0;
            this.GridSaveableAddress.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridSaveableAddress_CellContentDoubleClick);
            this.GridSaveableAddress.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.GridSaveableAddress_RowHeaderMouseClick);
            // 
            // gridDisplayAddress
            // 
            this.gridDisplayAddress.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.gridDisplayAddress.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridDisplayAddress.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridDisplayAddress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridDisplayAddress.Location = new System.Drawing.Point(648, 60);
            this.gridDisplayAddress.Name = "gridDisplayAddress";
            this.gridDisplayAddress.Size = new System.Drawing.Size(386, 225);
            this.gridDisplayAddress.TabIndex = 1;
            this.gridDisplayAddress.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridDisplayAddress_CellContentDoubleClick);
            this.gridDisplayAddress.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gridDisplayAddress_RowHeaderMouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(648, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(349, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "1. Double click on IndexNumber to select correct address ";
            // 
            // radSelectedAddressPanel
            // 
            this.radSelectedAddressPanel.Controls.Add(this.label9);
            this.radSelectedAddressPanel.Controls.Add(this.label6);
            this.radSelectedAddressPanel.Controls.Add(this.lblType);
            this.radSelectedAddressPanel.Controls.Add(this.lblHidenCaseNumberID);
            this.radSelectedAddressPanel.Controls.Add(this.txtPalmAddress);
            this.radSelectedAddressPanel.Controls.Add(this.label3);
            this.radSelectedAddressPanel.Controls.Add(this.label2);
            this.radSelectedAddressPanel.Controls.Add(this.radDdlSTDirection);
            this.radSelectedAddressPanel.Controls.Add(this.lblHidenSrchDefendentID);
            this.radSelectedAddressPanel.Controls.Add(this.SelectedAddressSave);
            this.radSelectedAddressPanel.Controls.Add(this.txtUnitNo);
            this.radSelectedAddressPanel.Controls.Add(this.txtPostDir);
            this.radSelectedAddressPanel.Controls.Add(this.lblUnitNo);
            this.radSelectedAddressPanel.Controls.Add(this.lblPostDir);
            this.radSelectedAddressPanel.Controls.Add(this.lblstretNumber);
            this.radSelectedAddressPanel.Controls.Add(this.txtStNum);
            this.radSelectedAddressPanel.Controls.Add(this.txtStCity);
            this.radSelectedAddressPanel.Controls.Add(this.lblStCity);
            this.radSelectedAddressPanel.Controls.Add(this.txtStType);
            this.radSelectedAddressPanel.Controls.Add(this.lblstdirction);
            this.radSelectedAddressPanel.Controls.Add(this.lblstType);
            this.radSelectedAddressPanel.Controls.Add(this.lblstname);
            this.radSelectedAddressPanel.Controls.Add(this.txtStName);
            this.radSelectedAddressPanel.Location = new System.Drawing.Point(2, 293);
            this.radSelectedAddressPanel.Name = "radSelectedAddressPanel";
            this.radSelectedAddressPanel.Size = new System.Drawing.Size(631, 330);
            this.radSelectedAddressPanel.TabIndex = 30;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(40, 40);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(127, 13);
            this.label9.TabIndex = 39;
            this.label9.Text = "From BrowardClerk.org";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(396, 187);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(228, 13);
            this.label6.TabIndex = 38;
            this.label6.Text = "Do not use \'Street\' Keyword in Street Name";
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(500, 163);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(0, 13);
            this.lblType.TabIndex = 37;
            this.lblType.Visible = false;
            // 
            // lblHidenCaseNumberID
            // 
            this.lblHidenCaseNumberID.AutoSize = true;
            this.lblHidenCaseNumberID.Location = new System.Drawing.Point(546, 131);
            this.lblHidenCaseNumberID.Name = "lblHidenCaseNumberID";
            this.lblHidenCaseNumberID.Size = new System.Drawing.Size(0, 13);
            this.lblHidenCaseNumberID.TabIndex = 36;
            // 
            // txtPalmAddress
            // 
            this.txtPalmAddress.Location = new System.Drawing.Point(186, 14);
            this.txtPalmAddress.Multiline = true;
            this.txtPalmAddress.Name = "txtPalmAddress";
            this.txtPalmAddress.Size = new System.Drawing.Size(396, 81);
            this.txtPalmAddress.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(184, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(330, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "Copy address components and paste in respective fields below";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Selected address ";
            // 
            // radDdlSTDirection
            // 
            this.radDdlSTDirection.Location = new System.Drawing.Point(186, 157);
            this.radDdlSTDirection.Name = "radDdlSTDirection";
            this.radDdlSTDirection.Size = new System.Drawing.Size(203, 20);
            this.radDdlSTDirection.TabIndex = 31;
            // 
            // lblHidenSrchDefendentID
            // 
            this.lblHidenSrchDefendentID.AutoSize = true;
            this.lblHidenSrchDefendentID.Location = new System.Drawing.Point(433, 26);
            this.lblHidenSrchDefendentID.Name = "lblHidenSrchDefendentID";
            this.lblHidenSrchDefendentID.Size = new System.Drawing.Size(0, 13);
            this.lblHidenSrchDefendentID.TabIndex = 30;
            this.lblHidenSrchDefendentID.Visible = false;
            // 
            // SelectedAddressSave
            // 
            this.SelectedAddressSave.Location = new System.Drawing.Point(427, 275);
            this.SelectedAddressSave.Name = "SelectedAddressSave";
            this.SelectedAddressSave.Size = new System.Drawing.Size(156, 38);
            this.SelectedAddressSave.TabIndex = 29;
            this.SelectedAddressSave.Text = "Save Address";
            this.SelectedAddressSave.UseVisualStyleBackColor = true;
            this.SelectedAddressSave.Click += new System.EventHandler(this.SelectedAddressSave_Click);
            // 
            // txtUnitNo
            // 
            this.txtUnitNo.Location = new System.Drawing.Point(186, 264);
            this.txtUnitNo.Name = "txtUnitNo";
            this.txtUnitNo.Size = new System.Drawing.Size(203, 20);
            this.txtUnitNo.TabIndex = 21;
            // 
            // txtPostDir
            // 
            this.txtPostDir.Location = new System.Drawing.Point(186, 238);
            this.txtPostDir.Name = "txtPostDir";
            this.txtPostDir.Size = new System.Drawing.Size(203, 20);
            this.txtPostDir.TabIndex = 28;
            // 
            // lblUnitNo
            // 
            this.lblUnitNo.AutoSize = true;
            this.lblUnitNo.Location = new System.Drawing.Point(40, 271);
            this.lblUnitNo.Name = "lblUnitNo";
            this.lblUnitNo.Size = new System.Drawing.Size(73, 13);
            this.lblUnitNo.TabIndex = 27;
            this.lblUnitNo.Text = "Unit Number";
            // 
            // lblPostDir
            // 
            this.lblPostDir.AutoSize = true;
            this.lblPostDir.Location = new System.Drawing.Point(40, 245);
            this.lblPostDir.Name = "lblPostDir";
            this.lblPostDir.Size = new System.Drawing.Size(79, 13);
            this.lblPostDir.TabIndex = 26;
            this.lblPostDir.Text = "Post Direction";
            // 
            // lblstretNumber
            // 
            this.lblstretNumber.AutoSize = true;
            this.lblstretNumber.Location = new System.Drawing.Point(40, 131);
            this.lblstretNumber.Name = "lblstretNumber";
            this.lblstretNumber.Size = new System.Drawing.Size(84, 13);
            this.lblstretNumber.TabIndex = 5;
            this.lblstretNumber.Text = "Street Number ";
            // 
            // txtStNum
            // 
            this.txtStNum.Location = new System.Drawing.Point(186, 131);
            this.txtStNum.Name = "txtStNum";
            this.txtStNum.Size = new System.Drawing.Size(203, 20);
            this.txtStNum.TabIndex = 16;
            // 
            // txtStCity
            // 
            this.txtStCity.Location = new System.Drawing.Point(186, 293);
            this.txtStCity.Name = "txtStCity";
            this.txtStCity.Size = new System.Drawing.Size(203, 20);
            this.txtStCity.TabIndex = 21;
            // 
            // lblStCity
            // 
            this.lblStCity.AutoSize = true;
            this.lblStCity.Location = new System.Drawing.Point(40, 300);
            this.lblStCity.Name = "lblStCity";
            this.lblStCity.Size = new System.Drawing.Size(59, 13);
            this.lblStCity.TabIndex = 25;
            this.lblStCity.Text = "Street City";
            // 
            // txtStType
            // 
            this.txtStType.Location = new System.Drawing.Point(186, 211);
            this.txtStType.Name = "txtStType";
            this.txtStType.Size = new System.Drawing.Size(203, 20);
            this.txtStType.TabIndex = 20;
            // 
            // lblstdirction
            // 
            this.lblstdirction.AutoSize = true;
            this.lblstdirction.Location = new System.Drawing.Point(40, 156);
            this.lblstdirction.Name = "lblstdirction";
            this.lblstdirction.Size = new System.Drawing.Size(90, 13);
            this.lblstdirction.TabIndex = 22;
            this.lblstdirction.Text = "Street Direction ";
            // 
            // lblstType
            // 
            this.lblstType.AutoSize = true;
            this.lblstType.Location = new System.Drawing.Point(40, 218);
            this.lblstType.Name = "lblstType";
            this.lblstType.Size = new System.Drawing.Size(66, 13);
            this.lblstType.TabIndex = 24;
            this.lblstType.Text = "Street Type ";
            // 
            // lblstname
            // 
            this.lblstname.AutoSize = true;
            this.lblstname.Location = new System.Drawing.Point(40, 187);
            this.lblstname.Name = "lblstname";
            this.lblstname.Size = new System.Drawing.Size(72, 13);
            this.lblstname.TabIndex = 23;
            this.lblstname.Text = "Street Name ";
            // 
            // txtStName
            // 
            this.txtStName.Location = new System.Drawing.Point(187, 183);
            this.txtStName.Name = "txtStName";
            this.txtStName.Size = new System.Drawing.Size(203, 20);
            this.txtStName.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(295, 16);
            this.label4.TabIndex = 31;
            this.label4.Text = "Double click on CaseNumber for get address list";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(648, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 16);
            this.label5.TabIndex = 32;
            // 
            // lblSrchType
            // 
            this.lblSrchType.AutoSize = true;
            this.lblSrchType.Location = new System.Drawing.Point(405, 15);
            this.lblSrchType.Name = "lblSrchType";
            this.lblSrchType.Size = new System.Drawing.Size(0, 13);
            this.lblSrchType.TabIndex = 33;
            this.lblSrchType.Visible = false;
            // 
            // dataGridOtherProperty
            // 
            this.dataGridOtherProperty.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridOtherProperty.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridOtherProperty.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridOtherProperty.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridOtherProperty.Location = new System.Drawing.Point(651, 379);
            this.dataGridOtherProperty.Name = "dataGridOtherProperty";
            this.dataGridOtherProperty.Size = new System.Drawing.Size(383, 242);
            this.dataGridOtherProperty.TabIndex = 34;
            this.dataGridOtherProperty.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridOtherProperty_CellContentDoubleClick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(651, 306);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(358, 16);
            this.label7.TabIndex = 35;
            this.label7.Text = "2. Property address list searched by Defendent from BCPA ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(651, 331);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(343, 16);
            this.label8.TabIndex = 36;
            this.label8.Text = "Double click on FolioID to save correct property address";
            // 
            // FrmSaveAbleAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 635);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dataGridOtherProperty);
            this.Controls.Add(this.lblSrchType);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.radSelectedAddressPanel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gridDisplayAddress);
            this.Controls.Add(this.GridSaveableAddress);
            this.Name = "FrmSaveAbleAddress";
            this.Text = "FrmSaveAbleAddress";
            ((System.ComponentModel.ISupportInitialize)(this.GridSaveableAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDisplayAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSelectedAddressPanel)).EndInit();
            this.radSelectedAddressPanel.ResumeLayout(false);
            this.radSelectedAddressPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDdlSTDirection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUnitNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPostDir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridOtherProperty)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView GridSaveableAddress;
        private System.Windows.Forms.DataGridView gridDisplayAddress;
        private System.Windows.Forms.Label label1;
        private Telerik.WinControls.UI.RadPanel radSelectedAddressPanel;
        private System.Windows.Forms.Button SelectedAddressSave;
        private Telerik.WinControls.UI.RadTextBox txtUnitNo;
        private Telerik.WinControls.UI.RadTextBox txtPostDir;
        private System.Windows.Forms.Label lblUnitNo;
        private System.Windows.Forms.Label lblPostDir;
        private System.Windows.Forms.Label lblstretNumber;
        private Telerik.WinControls.UI.RadTextBox txtStNum;
        private Telerik.WinControls.UI.RadTextBox txtStCity;
        private System.Windows.Forms.Label lblStCity;
        private Telerik.WinControls.UI.RadTextBox txtStType;
        private System.Windows.Forms.Label lblstdirction;
        private System.Windows.Forms.Label lblstType;
        private System.Windows.Forms.Label lblstname;
        private Telerik.WinControls.UI.RadTextBox txtStName;
        private System.Windows.Forms.Label lblHidenSrchDefendentID;
        private Telerik.WinControls.UI.RadDropDownList radDdlSTDirection;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPalmAddress;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblSrchType;
        private System.Windows.Forms.Label lblHidenCaseNumberID;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dataGridOtherProperty;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}